\documentclass[11pt,a4paper]{article}
\usepackage[top=50pt,bottom=50pt,left=56pt,right=56pt]{geometry}  % to make pages wider
\usepackage{float}
	
\usepackage{graphicx}  % need this for figures etc.
\usepackage{url}  % to handle urls 
\usepackage[comma,authoryear]{natbib}  % for authordate ref style --  allows \citet (textual), and \citep (parenthesized)
\usepackage{tcolorbox}	% Used for boxes around extra notes
\usepackage[none]{hyphenat} % No word breaking on word wrap
\usepackage{mathtools}
\usepackage[toc,page]{appendix}

\usepackage[margin=0.5cm]{caption} % Padding for Captions
\captionsetup{justification=centering}

\renewcommand {\cite} {\citep}  % default for cite is citet in natbib - so change it
%\renewcommand\bibname{References}    % if not using newrucsthesis sty file

\setlength{\parindent}{0px}
\linespread{1.1}
\begin{document}

% Create a title
\title{ Writeup: \\ Quasi Real-Time Forest Fire Simulation }
\author{ Author: Damon Hook \\ Supervisors: Mr. James Connan and Dr. Karen Bradshaw }
\date { May 2017 }
\maketitle
\hrule
\vspace{2em}

%-------------------------------------------------------------------------------------------

\section{Control CA State Transition Function}
\label{sec:Basic CA}
\citet{KT} popularised a fire simulation CA model, called the K-T model, where a cell's state is the ratio (0 - 1) of its burning and non burning regions at a given time, and determined by:
\begin{equation}
  \label{eq:state}
  S_{i,j}^{t} = \frac{A_{b}}{A_{t}}
\end{equation}
Where: $ A_{b} $ is the area of the cell that is burning , and $ A_{t} $ is the total area of the cell. \\

It is possible to determine the time that a given cell will be completely burned out ( $ S_{i,j}^{t} = 1 $ ) by using a rule described by \citet{KT}: Should the cell $ (i,j) $ be completely unburned, and exactly one of its adjacent neighbours is completely burned out, then the cell $ (i,j) $ will be completely burned out after a time calculated using:

\begin{equation}
\label{eq:ta}
t_{a} = \frac{a}{R_{i,j}}
\end{equation}
Where: $a$ is the length of one side of a cell, and $ R_{i,j} $ is the initial rate of spread of cell $(i,j)$ \\

\citet{Wang} calculates the discrete time step for the CA $ \tilde{t} $ as being the $ t_{a} $ value of the cell with the highest rate of spread (see eq. \ref{eq:ta}). This can be described using:
\begin{equation} 
  \label{eq:time step}
  \tilde{t}=\frac{a}{R_{MAX}}, R_{MAX}=MAX\{ R_{i,j},0<i\leq n, 0 < j\leq m\}
\end{equation}
Where $ m $ and $ n $ are the number of rows and columns of the lattice respectively. \\

\citet{Encinas} explains that, given the square nature of the cells, the effects of a cells adjacent neighbours is stronger than that of its diagonal neighbours, and as such, the adjacent and diagonal cell effects need to be calculated separately. This leads to a new rule given by \citet{KT} being that the time of an unburned cell $(i,j)$ to be completely burned out given that exactly one of its diagonal neighbours is completely burned out is equal to:

\begin{equation}
\label{eq:td}
t_{d} = \frac{\sqrt{2}a}{R_{i,j}} = \sqrt{2}t_a
\end{equation}
Where $ \sqrt{2}a $ is the diagonal length of the cell, as calculated using:
\begin{equation}
  \label{eq:pythag}
  \begin{split}
    d^2 & = a^2 + a^2 \\ 
    d   & = \sqrt{2a^2} \\
        & = \sqrt{2}a
  \end{split}
\end{equation}
\vspace{\baselineskip}

Following the same reasoning, it is possible to calculate the impact that diagonal neighbours will have on the state of cell $(i,j)$ during a given time step \citep{KT}:
\begin{equation}
  \label{eq:lambda}
  \begin{split}
    S_{i,j}^{t+1} = \lambda &= \frac{a^2 - [(\sqrt{2} - 1)a]^2}{a^2} \\
                            &= 1 - (\sqrt{2} - 1)^2 \\
                            &\approx 0.83
  \end{split}
\end{equation}

While \citet{Encinas} proposes a new model, explaining that the fire spread from a diagonal neighbour should be circular instead of a straight diagonal line. Thus equation~(\ref{eq:lambda}) is modified as follows:
\begin{equation}
  \label{eq:lambda2}
  \begin{split}
    S_{i,j}^{t+1} = \lambda &= \frac{\pi a^2}{4a^2} \\
                            &= \frac{\pi}{4} \\
                            &\approx 0.785
  \end{split}
\end{equation}
\vspace{\baselineskip}

\citet{Encinas} introduces a set containing the different possible offsets ($ \alpha $ and $ \beta $) for all of a given cells neighbours.
\begin{equation}
\label{eq:Vm}
  V_{M} = \{ (-1,-1),(-1,0),(-1,+1),(0,-1),(0,+1),(+1,-1),(+1,0),(+1,+1) \}
\end{equation}

In order to more easily distinguish between the adjacent cells and diagonal cells, two subsets of $ V_M $ containing the offsets for the two corresponding neighbour types:
\begin{equation}
\label{eq:Vadj}
  V_{M}^{adj} = \{ (-1,0),(0,+1),(0,-1),(+1,0) \}
\end{equation}
\begin{equation}
\label{eq:Vdiag}
  V_{M}^{diag} = \{ (-1,-1),(-1,+1),(+1,-1),(+1,+1) \}
\end{equation}
\vspace{\baselineskip}

As a consequence, the CA state transition function, for the cell $ (i,j) $ and discrete time step $ \tilde{t} $, is given by \citep{Encinas}:
\begin{equation}
\label{eq:baseState}
  S_{i,j}^{t + 1} = S_{i,j}^{t} + \sum_{(\alpha,\beta)\in V_{M}^{adj}} S_{i + \alpha ,j + \beta}^{t} + 0.785 \sum_{(\alpha,\beta)\in V_{M}^{diag}} S_{i + \alpha ,j + \beta}^{t}
\end{equation}

% ------------------------------------------------------------------------------------------

\section{Factoring in Wind and Slope}
\label{sec:windAndSlope}
It is also possible to factor in wind and slope functions into the above equation by introducing a new function:
\begin{equation}
\label{eq:windAndSlopeFactor}
  \mu_{i,j}^{t} = \phi_{w_{i,j}}^t + \phi_{h_{i,j}}^t
\end{equation}
resulting in a new CA state transition function:
\begin{equation}
\label{eq:CA2}
  S_{i,j}^{t + 1} = S_{i,j}^{t} + \sum_{(\alpha,\beta)\in V_{M}^{adj}} \mu_{i + \alpha,j + \beta}^{t} S_{i + \alpha ,j + \beta}^{t} + 0.785 \sum_{(\alpha,\beta)\in V_{M}^{diag}} \mu_{i + \alpha,j + \beta}^{t} S_{i + \alpha ,j + \beta}^{t}
\end{equation}
\vspace{\baselineskip}

\begin{tcolorbox}
	\textbf{TODO:} Complete Wind and Slope Calculations.
\end{tcolorbox}
\vspace{\baselineskip}

% ------------------------------------------------------------------------------

\section{Heterogeneous Environments}
\label{sec:finalCA}
\citet{Encinas} explains that equation~(\ref{eq:CA2}) does not cater for heterogeneous fuel types (thus cells can have differing rates of spread) and as such the equation needs to be modified to take the neighbouring cell's rate of spread. \\

Should $ R_{i,j} \neq R_{MAX} $ then it can be said that $ R_{i,j} < R_{MAX} $, and then that if an unburned cell has one burning adjacent neighbour, then after the given time period (eq:~\ref{eq:time step}), the cell will be burned out by the function:
\begin{equation}
\label{eq:burnOutHet}
  S_{i,j}^{t+1} = \frac{\pi R_{ij}^{2}}{4R_{MAX}^{2}}
\end{equation}
consequently, equation~(\ref{eq:CA2}) can be modified using the above function to produce a new state transition function for heterogeneous environments:
\begin{equation}
  \label{eq:CAFinal}
    S_{i,j}^{t + 1} = \frac{R_{ij}}{R_{MAX}} S_{i,j}^{t} + \sum_{(\alpha,\beta)\in V_{M}^{adj}} \mu_{i + \alpha,j + \beta}^{t} \frac{R_{i+\alpha,j+\beta}}{R_{MAX}} S_{i + \alpha ,j + \beta}^{t} + 0.785 \sum_{(\alpha,\beta)\in V_{M}^{diag}} \mu_{i + \alpha,j + \beta}^{t} \pi\frac{R_{i+\alpha,j+\beta}^2}{4R_{MAX}^2} S_{i + \alpha ,j + \beta}^{t}
  \end{equation}
  \vspace{\baselineskip}

\begin{tcolorbox}
	\textbf{TODO:} Complete Final State Transition Calculations.
\end{tcolorbox}
\vspace{\baselineskip}

%--------------------------------------------------------------------------------

\section{Discrete Cell States}
\label{sec:DiscreteStateTransitions}
While the CA state is explained, by equation~(\ref{eq:state}), as a continuous scale between 0 and 1, it is important to discretise it into 3 possible cell states: Unburned, Burning, and Extinguished. \\

All cells start in the Unburned state and $ S_{ij}^{0} $ is set to 0. When the simulation is started, a single cell is set to Burning and $ S_{ij}^{0} $ set to 1. \\

For a given cell $(i,j)$, that is in an unburned state, will move to a burning state when the following expression is true:
\begin{equation}
\label{eq:burning}
  S_{i,j}^{t} \geq 1
\end{equation}
\vspace{\baselineskip}

\citet{Berjak} explains that it is possible to determine the time at which a given cell $(i,j)$ will be extinguished when the following expression is true: 
\begin{equation}
\label{eq:extinction}
  (t + \Delta t) \geq (t_s + t_d)
\end{equation}
Where $t_s$ represents the time at which the cell started burning. \\

The 3 possible states, as well as the transitions that govern them can be seen in figure~\ref{fig:states}
\begin{figure}[H]
	\centering
  \includegraphics[width=0.75\textwidth]{Figures/StateDiagram.jpg}
  \caption{Possible States for Cell $(i,j)$ and their transitions}
  \label{fig:states}
\end{figure} 

%----------------------------------------------------------------------------------

\section{Future Work and Notes}
\label{sec:Future}
\begin{itemize}
  \item Does not allow for a cell to move from burning to back unburned
  \item Maximum sim speed multiplier is directly linked to the performance
  \item CA scales deterministically, does not run slower the longer the simulation
\end{itemize}

\newpage
\bibliographystyle{ruauthordate}
\bibliography{references}   	% load in the citation info from ref.bib

\newpage
\begin{appendices}

\end{appendices}

\end{document}
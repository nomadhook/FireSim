\section{Fire Simulation}
\label{sec:Fire Simulation}
There are many different methods for the "real time" simulation of fires (especially within the field of GIS), each with their own advantages and disadvantages that make them better suited in different scenarios. In this literature review several different proposed techniques are discussed and comparisons are made. \\

\subsection{Data Representation Comparison}
\label{sec:Fire Simulation Data}
There are two main competing methods for the representation of the data for various fire simulation implementations (Similar to the two main categories of DEM structures discussed in section~\ref{sec:DEM}), these methods are: Raster-based, and vector-based. \\

A raster-based representation is having the terrain being represented as a cellular grid (linearly based rasters/cells in rows and columns to create a two dimensional grid/matrix) \citep{Lo}. \citet{Lo} explains that a raster-based method is similar to cellular automata and is based on the principle of contiguous data (in a predefined sequence). He also explains that using this method (in specific to fire simulations) each cell's ignition probability can be calculated by using its own locally defined parameters (like fuel) as well as the ignition state of its neighbouring cells. Figure~\ref{fig:Raster representation} provides an illustration of a raster grid. \\

\begin{figure}[H]
	\centering
	\includegraphics[width=0.22\textwidth]{Figures/raster_representation.png}
	\caption{Raster-based representation \citep{Lo}}
	\label{fig:Raster representation}
\end{figure} 

\citet{Lo} explains that a limitation of raster-based methods is that there is no way to, at a given time, predict the next/following movement/direction of the fires front. Instead you are required to perform calculations on each cell in the grid (using its neighbours) to determine the rate of spread to the connecting cells (this results in a situation where it is challenging to predict fire spread trends, without running the entire simulation over the full time of interest). Following this \citet{Lo} brings to light two more limitations, these being: Fire spread calculations are non-contiguous (there are well defined simulation steps making the simulation very staccato), and that the raster-based method falls short in providing well defined fire spread shapes (within realistic time and computational constraints). \\

A vector-based representation involves an enclosing line/boundary made up from several vertex's, which are vectors (see figure~\ref{fig:Vector representation}), this means that each vertex has properties for position and velocity (which has direction and magnitude) \citep{Lo}. \citet{Lo} also explains that due to the properties inherent to vectors it is possible to predict the expansion of the fire front (boundary). The vector-based method is built on a combination of Huygens' Principle (see figure~\ref{fig:Huygens principle}) and the assumption of elliptical spread \citep{Lo}. \\

\begin{figure}[H]
\centering
	\begin{minipage}{0.5\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{Figures/vector_representation.png}
		\caption{Vector-based representation \citep{Lo}}
		\label{fig:Vector representation}
	\end{minipage}%
	\begin{minipage}{0.5\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{Figures/huygens_principle.png}
		\caption{Huygens' Principle \citep{Lo}}
		\label{fig:Huygens principle}
	\end{minipage}
\end{figure}

\citet{Lo} explains the assumption of elliptical spread is the assumption that the propagation of wildfire under the wind will take on an elliptical shape. They also describe Huygens' principle as an assumption that the fire's front (the boundary of the wildfire) will propagate independently and equally from the front-most edge vertex. \\

The vertices that make up the wildfire boundary are two dimensional vertices (x and y coordinates) and are used in conjunction with parameters such as: terrain, fuel type, wind, and fuel density between vertices to get a function representing the spread rate over time \citep{Lo}. This function allows the simulation data to be calculated contiguously (not only existing at given time steps, but rather a function over time to which any time step can be substituted to get what the spread will be at that time. A major advantage to this, that is presented by \citet{Lo}, is that the contiguous nature of the data allows for a smaller number of vertex points in comparison to the number of raster cells needed in a raster-based approach. This smaller number of data points allows for the production of comparatively better fire spread shapes while still being less computationally and memory intensive. \\

\citet{Lo} also explains that there are still shortcomings for a vector-based system, that extends past the difficulty of implementation, and to the disadvantage that while this method is very good for calculating fire spread, it does not store any local variables/information. This results in two main problems: The first problem is the inability to replicate the spread of wildfire using multiple types of fuel, and the second is what is called the crossover problem, which is when two fire fronts overlap or a fire front overlaps with itself. Figure~\ref{fig:crossovers} provides several examples of crossover scenarios. \\

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{Figures/crossovers.png}
	\caption{Various types of fire-front crossovers \citep{Lo}}
	\label{fig:crossovers}
\end{figure} 

\subsection{Realistic fire simulation over a mesh}
\label{sec:Mesh Method}
\citet{Beaudoin} uses a mesh as its surface data type and explains that creating an accurate simulation of fire is very challenging as well as often expensive and so suggests splitting the challenge of creating a controllable and realistic fire simulation into three pieces: rendering, animation and propagation. \\

% --- FILLER ---
A technique for rendering fire that is often used is using a particle system. However, a problem with using this method is that not only are large number of particles very computationally intensive, but it also results in a fuzzy, cloud-like appearance to flames which is unlike the crisp outlines of real flames \citep{Beaudoin}. Instead of this \citet{Beaudoin} used a technique involving a smaller set of flames represented by deformable chains of vertices. \\
% --- END FILLER ---

Fire propagation refers to the spread of fire over time on a given surface \citep{Beaudoin}. Furthermore, this uses a vector-based data representation and splits the mathematical modelling of fire propagation into the evolution of several variables surrounding the burning region: temperature, pressure, and air velocity. These variables are then broken down further, while simplifying what is considered to be most relevant for a simulation of fire expansion, into a set of parameters that are locally defined: The density of the fuel, oxygen supply, wind, as well as the orientation of the surface (relative to gravity) \citep{Beaudoin}. Using this relatively small number of parameters as well as tracking the boundary between the burning and non-burning regions of the surface you can create a simplified fire propagation simulation at relatively low cost \citep{Beaudoin}. \\

A boundary is an enclosed line which can give a clear indication between two different areas and the vertices that make up the boundary line have two main properties: position and velocity \citep{Beaudoin} (see figure~\ref{fig:Mesh Boundary}). Furthermore, a broken line is used on the mesh to represent this boundary all the while ensuring that this broken line satisfies the condition: that should two vertices exist on different faces on the mesh, a third point will be spawned on the connected edge of the two faces. \\

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{Figures/boundry.png}
	\caption{Broken-line boundary on a mesh \citep{Beaudoin}}
	\label{fig:Mesh Boundary}
\end{figure}

\citet{Beaudoin} explains that defining the boundary expansion through the velocity of its vertices is very easy given that the vertex remains on the same face. The equation that \citet{Beaudoin} uses to describe this action is as follows: $ P_{i}(t+\Delta t) = P_{i}(t) + v_{i}(t)\Delta t $ where for a given vector i, $ P_{i} $ denotes its position and $ v_{i} $ denotes its velocity. \\

Should a vertex move onto a different face it becomes necessary to recalculate its velocity so that it is relative to the new mesh as well as the displacement caused by the transition over the edge \citep{Beaudoin}. The equation given by \citet{Beaudoin} for this is as follows: $ v'_{i} = \eta (N_{i}v_{i})N' $ where for the displaced vertex i; $ \eta $ is a pre-chosen coefficient that aims to preserve the magnitude of the original velocity, $ N_{i} $ is the linearly interpolated normal at the point of crossing, and N' is the vector normal to the vertex's destination face (see figure~\ref{fig:Mesh Velocity Transform}). \\

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{Figures/velocity_transform.png}
	\caption{Transforming a vertex's velocity \citep{Beaudoin}}
	\label{fig:Mesh Velocity Transform}
\end{figure}

Tracking the fire spread/growth over time is done using a two-step update at every predetermined time step interval \citep{Beaudoin}: Firstly the new position of each vertex is calculated by its current velocity as well as updating its velocity should it cross an edge (using the two equations mentioned earlier), Secondly any new vertices that need to be spawned due to face transitions are then spawned where needed. The final thing to take into account when tracking the fire's propagation is the non-uniform nature of the speeds of propagation \citep{Beaudoin}. This is enforced by using the locally defined parameters to alter each vertex's velocity at each given time step \citep{Beaudoin}. \\ 

\subsection{Level Set Method for heterogeneous environments}
\label{sec:Level Set Method}
\citet{Lo} conducted research into creating a combination of raster and vector based methods (discussed in section~\ref{sec:Fire Simulation Data} in order to best utilise their strengths while trying to minimise or overcome their relative shortcomings. The method they propose is to store the local variables and fire data in the cell (like in a raster-based system), however, instead of using them to calculate the ignition probability and state for every cell, the fire spread is calculated using the principles from a vector-based systems allowing this implementation to be applied to heterogeneous environments. \citet{Lo} explains that two mathematical methods are relevant to their implementation, these being: The narrow band and fast marching methods. Both of these mathematical models use the projection of two dimensional surface evolution data into a higher plane (three dimensions), which can then be calculated and re-projected down to two dimensions again. \\

The fast marching method is used in particular to solve the crossover problem. It achieves this by, given that the contour of the three dimensional object is in a constant state of shrinking or expansion, it will produce a stationary formula result onto a discrete grid representing where the given contour crosses each grid only once \citep{Lo}. It is also explained that this method does not calculate expansion using a level set method but rather calculates travel time using points to the surface in order to track said moving surface. \citet{Lo} explains that, using pre-stored variables stored in each cell, it is possible to calculate the time of travel from the wildfire's front using an Eikonal equation: $ 1 = F||\nabla T|| $. This results in a time matrix that can be transformed into a three dimensional object (represented by $ T(x, y, t) $) and this can then be re-projected onto the two dimensional plane thus simulating a wildfire's spread. \\

In comparison the narrow band method utilises the level set method in order to satisfy their simulation model \citep{Lo}. 
% FIXME - INSERT EXPLANATION OF WHAT A LEVEL SET FUNCTION IS
This was achieved through a generation of a three dimensional level set function from a distance function stored in each raster cell: $ \phi(x(t), Y(t), t) $ \citep{Lo}. Following this, Hamilton-Jacobi's method is used to solve the level set equation as well as to predict the expansion of the the contour on the two dimensional xy plane, the equation for this method is $ \phi_{t} = -F||\nabla \phi || $. \\

In both of these solutions a fire function is used and represented as $ F $. \citet{Lo} explains this fire function as a modified Rothermel's fire model (discussed further in section~\ref{sec:Rothermel}) described as an equation set: $ F = (\varepsilon_{0} + W(U, \theta) +  \varepsilon_{0}\Phi_{s})\xi $ if $ |\theta| \leq \frac{\pi}{2} $; or $ F = (\varepsilon_{0}(\alpha + (1 - \alpha)|\sin \theta |) + \varepsilon_{0} \Phi_{s})\xi $ if $ |\theta| > \frac{\pi}{2} $ \citep{Lo}. $ W(U, \theta) $ represents the wind factor where $ W(U, \theta) = \alpha \sqrt{U \cos^{3} \theta} $; $ \Phi_{s} $ represents the terrain factor where $ \Phi_{s} = 50275\beta^{-0.3}(\tan \theta_{t})^{2} $; $ \xi $ represents whether the heat provided is sufficient to ignite (0 or 1); $ \varepsilon_{0} $ is the fuel ; $ \alpha $ is an already ignited point; and $ \beta $ is the new point to test \citep{Lo}. Figures~\ref{fig:Level Set}, and \ref{fig:Fast Marching} illustrate a comparison between two simulations (with an easterly wind) using these two methods. \\

\begin{figure}[H]
\centering
	\begin{minipage}{0.5\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{Figures/level_set.png}
		\caption{Level Set simulation \citep{Lo}}
		\label{fig:Level Set}
	\end{minipage}%
	\begin{minipage}{0.5\textwidth}
		\centering
		\includegraphics[width=0.7\textwidth]{Figures/fast_marching.png}
		\caption{Fast Marching simulation \citep{Lo}}
		\label{fig:Fast Marching}
	\end{minipage}
\end{figure}

\subsection{Wind Modelling}
\label{sec:Wind}
\citet{Forthofer} explains that in the large majority fire simulations implement a uniform wind (in both direction and magnitude) and they propose two numerical alternatives to this (and compare them) that are used in wind power generation fields and pollutant dispersion. These two models are: Mass-consistent, and computational fluid dynamics (CFD). They further explain that even though most fire models do not take it into account, wildfires (especially in  mountainous areas) are heavily affected by wind patterns that are complex, spatially heterogeneous. \citet{Forthofer} then explains that the main reason for this is because of the heterogeneous nature of these winds, a fire's propagation becomes erratic and thus a lot more challenging to predict. \\

The mass-consistent method uses elliptical partial differential equations in order to minimise the initial wind field changes while still conserving the mass \citep{Forthofer}. They go on further to say that this method uses a variational calculus technique using the the squared difference of the values for the adjusted and the observed. The CFD model is based around a piece of fluid dynamics code, that is multidimensional and general purpose, called FLUENT\textsuperscript{\textregistered} \citep{Forthofer}. They then state that while its original purpose was, in industries, to model flows of interest, this software has gained momentum (due to it being general purpose) in the field of wind modelling and engineering. Using this method, "Air was modeled as an incompressible fluid and the Coriolis effect neglected." \citep{Forthofer}. \\

The first comparison between these two methods that \citet{Forthofer} makes is that while both methods performed well on the top of hills and upwind, the CFD model performed better on the lee side (there were significant errors when the mass-consistent caused). Furthermore, \citet{Forthofer} explains that, on their testing systems, the CFD model took anywhere from half and hour to an hour and a half to run. Another limitation of both techniques is that neither of them take into account the effect that a fire has on the surrounding wind \citep{Forthofer}. Finally \citep{Forthofer} states that a major advantage for mass-consistent models is their relatively low computational cost. \\

\subsection{Ensemble Fire Simulations}
\label{sec:Ensemble}
\citet{Finney} describes an ensemble fire simulation as a system that factors in the uncertain and erratic nature of long-range weather conditions into its longer-term fire simulations. They proposed a system of generating an extra parameter of fuel moisture due to changing weather seasons through gathering a large amount of real weather conditions to use in a fire simulation. A discrimination between their system and other fire simulation systems is their focus on a longer time period (as other systems start to lose a lot of accuracy due to changing weather patterns). \citet{Finney} suggests that their system's method of ensemble forecast is similar to the forecasts presented by meteorological model and is used to improve strategic decisions based on long-burning wildfires. \\

An assumption that this system is built on is that environmental variables can be streamlined to a single daily value \citep{Finney}. This is due to the understanding that, for each day, wildfire's are most active during what is called the "burning period" (during the afternoon hours) in comparison to mornings and evenings (at this time the fuel moisture is higher due to higher humidity levels). Given that the system \citet{Finney} proposed has a simulation time of multiple days and weeks, the inaccuracy within each day due to the mediating of the days effects can be disregarded as insignificant in the broader simulation. \\

For the weather generation, a time series approach was taken and used to analyse and generate weather sequences \citep{Finney}. They explain that this is because of its ability to capture three areas: seasonal trends that happen within a given year, the variation within and among the seasons, and the autocorrelation in fuel moisture percentage values using the proxy ERC index. \citet{Finney} also explains that there are three assumptions for their approach. The first assumption is that there exists an overall trend for seasons from year to year (denoted by $ f(t) $), it is explained that this is estimated using a weighted least squares polynomial model (denoted by $ z(t) $) and the weights that are used are calculated as the inverse of the daily standard deviations. The second assumption is that the daily standard deviations are normally distributed around the daily means (denoted by $ \mu (t) $). The last assumption is that residuals (the difference between the weighted least squares polynomial model and the overall trend for the season ($ z(t) - f(t) $)) follow an autocorrelation function (denoted by $ p(k) $ with k being the lag in days); these must also always be done in time out to a maximum value of $ t* $. \\

\subsection{Rothermel's Fire Model}
\label{sec:Rothermel}
Rothermel's fire model was developed in 1979 as the first quantitative spread prediction model used by the Forest Service and is still the main spread prediction tool in use today \citep{Wells}. Furthermore, \citep{Wells} describes Rothermel's method as simplifying fire behaviour by applying it to a simpler, more controllable "universe" that is covered with small non-described fuel cells on the floor. \citet{Rothermel} explains that a fundamental observation made by prior work (which is still relevant today) is that the first essential process to creating a fire spread model is to heat the fuel ahead of the fire boundary as it progresses. Furthermore, abstracting a fire's spread into a series of separate ignitions (each cell in a grid can represent one ignition) helps to break the problem down into more handleable and explainable chunks. \\

\citet{Rothermel} explains that the rate of fire spread suring what is called the quasi-steady state is equated as the ratio of source heat flux to the heat required for ignition of potential fuel. Furthering this, calculating the heat required for the ignition of a fuel is dependent upon three properties: the fuel moisture, the ignition temperature, and the amount of fuel involved. \\

The propagating flux is defined by \citet{Rothermel} as being comprised of two parts: the horizontal flux, and the gradient of the vertical flux integrated from negative infinity to the fire's front (boundary). Furthermore, a fire has a more significant vertical flux, given that it is wind-driven (the wind is pushing in the same direction as the fire's movement) or travelling up a slope (see figures \ref{fig:Fire No Wind}, \ref{fig:Fire Wind}, \ref{fig:Fire Up}), this is due to the amount of tilt over the potential fuel. \citet{Rothermel} also explains that it is important to take mass loss rate into account when you want a high accuracy fire model. \\

\begin{figure}[H]
\centering
	\begin{minipage}{0.3333\textwidth}
		\centering
		\includegraphics[width=1.15\textwidth]{Figures/fire_nowind.png}
		\caption{Schematic of fire: No wind \citep{Rothermel}}
		\label{fig:Fire No Wind}
	\end{minipage}%
	\begin{minipage}{0.3333\textwidth}
		\centering
		\includegraphics[width=1.15\textwidth]{Figures/fire_wind.png}
		\caption{Schematic of fire: Wind-driven \citep{Rothermel}}
		\label{fig:Fire Wind}
	\end{minipage}%
	\begin{minipage}{0.3333\textwidth}
		\centering
		\includegraphics[width=1.15\textwidth]{Figures/fire_up.png}
		\caption{Schematic of fire: up-slope. \citep{Rothermel}}
		\label{fig:Fire Up}
	\end{minipage}
\end{figure}
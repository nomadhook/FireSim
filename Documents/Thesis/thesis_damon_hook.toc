\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Overview}{1}
\contentsline {section}{\numberline {1.2}Research Goals}{1}
\contentsline {section}{\numberline {1.3}Document Structure}{2}
\contentsline {chapter}{\numberline {2}Literature Review}{3}
\contentsline {section}{\numberline {2.1}Introduction}{3}
\contentsline {section}{\numberline {2.2}Geographical Information Systems}{3}
\contentsline {subsection}{\numberline {2.2.1}Topographic Maps}{4}
\contentsline {subsection}{\numberline {2.2.2}Digital Elevation Models}{4}
\contentsline {section}{\numberline {2.3}Non-Spacial Fire Spread}{5}
\contentsline {subsection}{\numberline {2.3.1}Rothermel's Model for Calculating Rate of Spread}{6}
\contentsline {subsection}{\numberline {2.3.2}Rothermel's Model for Wind and Slope modifiers}{10}
\contentsline {section}{\numberline {2.4}Fire Simulations}{11}
\contentsline {subsection}{\numberline {2.4.1}Raster vs. Vector based Fire Simulation}{11}
\contentsline {subsection}{\numberline {2.4.2}Cellular Automata Approach}{14}
\contentsline {subsubsection}{K-T Model}{14}
\contentsline {subsubsection}{Berjak \& Hearne Model}{16}
\contentsline {subsection}{\numberline {2.4.3}Modelling Wind and Slope in a CA Approach}{19}
\contentsline {subsubsection}{Wang \textit {et al.}'s Method for Wind Factors}{19}
\contentsline {subsubsection}{Berjak \& Hearne's Method for Wind and Slope}{21}
\contentsline {subsection}{\numberline {2.4.4}Mesh Based Fire Simulation}{23}
\contentsline {subsection}{\numberline {2.4.5}Level Set Method for Heterogeneous Environments}{25}
\contentsline {section}{\numberline {2.5}Programming with GPU's and Accelerators}{28}
\contentsline {subsection}{\numberline {2.5.1}When to parallelise a system}{29}
\contentsline {section}{\numberline {2.6}Augmented Reality}{29}
\contentsline {subsection}{\numberline {2.6.1}Augmented Reality Sandbox}{30}
\contentsline {section}{\numberline {2.7}Summary}{32}
\contentsline {chapter}{\numberline {3}Design}{33}
\contentsline {section}{\numberline {3.1}Model Discussion}{33}
\contentsline {section}{\numberline {3.2}System Discussions}{34}
\contentsline {section}{\numberline {3.3}Data Gathering and Representation}{35}
\contentsline {section}{\numberline {3.4}Rejected Optimisation Suggestions}{37}
\contentsline {subsection}{\numberline {3.4.1}Region Of Interest Approach}{37}
\contentsline {subsection}{\numberline {3.4.2}Padding}{38}
\contentsline {chapter}{\numberline {4}Implementation}{40}
\contentsline {section}{\numberline {4.1}Sequential CPU Version}{40}
\contentsline {subsection}{\numberline {4.1.1}Spread Model Without Wind and Slope}{40}
\contentsline {subsection}{\numberline {4.1.2}Including Wind and Slope}{45}
\contentsline {section}{\numberline {4.2}Parallel CPU Version}{47}
\contentsline {section}{\numberline {4.3}GPU Version}{47}
\contentsline {section}{\numberline {4.4}GUI Proof of Concept}{50}
\contentsline {chapter}{\numberline {5}Results and Discussion}{52}
\contentsline {section}{\numberline {5.1}Model Discussion}{52}
\contentsline {subsection}{\numberline {5.1.1}No Wind or Slope}{52}
\contentsline {subsection}{\numberline {5.1.2}Wind and Slope Effects}{53}
\contentsline {subsection}{\numberline {5.1.3}Overshooting}{55}
\contentsline {section}{\numberline {5.2}Performance Discussion}{58}
\contentsline {chapter}{\numberline {6}Conclusions and Future Work}{66}
\contentsline {chapter}{References}{68}
\contentsline {chapter}{Appendices}{71}
\contentsline {chapter}{\numberline {A}Summary of CA Model Equations}{72}
\contentsline {chapter}{\numberline {B}Sequential CPU model code}{75}
\contentsline {chapter}{\numberline {C}Parallel CPU model code}{82}
\contentsline {chapter}{\numberline {D}GPU model code}{89}
\contentsline {chapter}{\numberline {E}User Guide}{97}

\contentsline {lstlisting}{\numberline {4.1}Globals and Method Signature for Sequential with no Wind or Slope}{41}
\contentsline {lstlisting}{\numberline {4.2}Sequential Update Method for No Wind or Slope}{43}
\contentsline {lstlisting}{\numberline {4.3}Inline Conversion of $x-y$ coordinates to array index}{44}
\contentsline {lstlisting}{\numberline {4.4}Calculating a Neighbour's ROS for No Wind or Slope}{44}
\contentsline {lstlisting}{\numberline {4.5}Timing a call to the Update() method using the chrono class}{45}
\contentsline {lstlisting}{\numberline {4.6}Including slope into the RateOfSpreadAt method}{45}
\contentsline {lstlisting}{\numberline {4.7}Calculating the relative angle between the neighbour and wind directions}{45}
\contentsline {lstlisting}{\numberline {4.8}Adding wind into the RateOfSpreadAt method}{46}
\contentsline {lstlisting}{\numberline {4.9}OpenMP pragma to enable multi-threading for a for loop}{47}
\contentsline {lstlisting}{\numberline {4.10}Pre-simulation GPU code}{47}
\contentsline {lstlisting}{\numberline {4.11}Kernel and supporting GPU method signatures}{48}
\contentsline {lstlisting}{\numberline {B.1}Sequential CPU code}{75}
\contentsline {lstlisting}{\numberline {C.1}Parallel CPU code}{82}
\contentsline {lstlisting}{\numberline {D.1}GPU code}{89}

% ----------------------------------------------------------------------------
%                              FIRE SIMULATION
% ----------------------------------------------------------------------------

\section{Fire Simulations}
\label{sec:Lit Fire Sim}
There are many different methods for the simulation of fire fronts (especially within the field of GIS), each with their own advantages and disadvantages that make them better suited in different scenarios. Fire propagation refers to the spread of fire over time on a given surface \citep{Beaudoin}. In this literature review several different proposed techniques are discussed.

% ----------------------------------------------------------------------------
%                         RASTER VS. VECTOR FIRE SIM
% ----------------------------------------------------------------------------

\subsection{Raster vs. Vector based Fire Simulation}
\label{sec:Lit Raster vs Vector Fire}
There are two main competing methods for the representation of the data for various fire simulation implementations (similar to the two main categories of DEM structures discussed in Section~\ref{sec:Lit DEM}). These methods are: raster-based, and vector-based.

In a raster-based representation is having the terrain being represented as a cellular grid (linearly based rasters/cells in rows and columns to create a two dimensional grid/matrix) \citep{Lo}. According to \citet{Lo} a raster-based method is based on the principle of contiguous data, and as such lends well to a cellular automata approach (see Section~\ref{sec: Lit CA}). Figure~\ref{fig:Raster representation} provides an illustration of a raster grid. \\

\begin{figure}[H]
	\centering
	\includegraphics[width=0.22\textwidth]{lit/figures/raster_representation.png}
	\caption{Raster-based representation \citep{Lo}}
	\label{fig:Raster representation}
\end{figure}

A limitation of raster-based methods is that there is no way, at a given time, to predict the following movements of the fire's front \citep{Lo}. Instead it is necessary to perform calculations on each cell in the grid (using its neighbours) to determine the rate of spread to the connecting cells. This results in a situation where it is challenging to predict fire spread trends, without running the entire simulation over the full time of interest. A further two limitations of this method are: Fire spread calculations are non-contiguous (there are well-defined simulation steps making the simulation very staccato), and the raster-based method falls short in providing well-defined fire spread shapes (within realistic time and computational constraints) \citep{Lo}. 

A vector-based representation involves an enclosing line/boundary made up from several vertices (see Figure~\ref{fig:Vector representation}), each vertex is represented as a vector and has properties for position and velocity (which has direction and magnitude) \citep{Lo}. Due to the properties of vectors it is possible to predict the expansion of the fire front. The vector-based method is built on a combination of Huygens' Principle (see Figure~\ref{fig:Huygens principle}) and the assumption of elliptical spread (can be seen in Figure~\ref{fig:Vector representation}) \citep{Lo}. 

\begin{figure}[H]
	\centering
		\begin{minipage}{0.49\textwidth}
			\centering
			\includegraphics[width=0.8\textwidth]{lit/figures/vector_representation.png}
			\caption{Vector-based representation \citep{Lo}}
			\label{fig:Vector representation}
		\end{minipage}
		\begin{minipage}{0.49\textwidth}
			\centering
			\includegraphics[width=0.8\textwidth]{lit/figures/huygens_principle.png}
			\caption{Huygens' Principle \citep{Lo}}
			\label{fig:Huygens principle}
		\end{minipage}
	\end{figure}

\citet{Lo} explains that the assumption of elliptical spread is that the propagation of wildfire, under the effects of wind, will take on an elliptical shape. They also describe Huygens' principle as an assumption that the fire's front will propagate independently and equally from the front-most edge vertex. 

The vertices that make up the wildfire boundary are two-dimensional vectors ($ x $ and $ y $ coordinates) and are used in conjunction with parameters such as: terrain, fuel type, wind, and fuel density between vertices to get a function representing the spread rate over time \citep{Lo}. This allows the simulation data to be calculated contiguously, which means that values do not only exist at discrete time steps, but rather exist as a function over time to which any time step can be substituted to get what the spread will be at that time. A major advantage to this, according \citet{Lo}, is that the contiguous nature of the data allows for a smaller number of vertex points compared to the number of raster cells needed in a raster-based approach. This smaller number of data points allows for the production of comparatively better fire spread shapes while still being less computationally and memory intensive. 

There are still shortcomings for a vector-based system, that extend past the difficulty of implementation, and to the disadvantage that while this method is very good for calculating fire spread in homogenous environments, it does not store any local variables/information \citep{Lo}. This results in two main problems. The first problem is the inability to replicate the spread of wildfire using multiple types of fuel, and the second is what is called the crossover problem, which is when two fire fronts overlap or a fire front overlaps with itself (see Figure~\ref{fig:crossovers}). \citet{Berjak} point out that most fires do not burn under constant conditions, instead many factors (such as: differing fuel types, weather, and topography) often change spatially and sometimes temporally. 

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{lit/figures/crossovers.png}
	\caption{Various examples of fire-front crossovers \citep{Lo}}
	\label{fig:crossovers}
\end{figure} 

% ----------------------------------------------------------------------------
%                          	  CELLULAR AUTOMATA
% ----------------------------------------------------------------------------

\subsection{Cellular Automata Approach}
\label{sec: Lit CA}
Using this method each cell's ignition probability can be calculated by using its own locally defined parameters (like fuel) as well as the ignition state of its neighbouring cells \citep{Lo}.

\subsubsection{K-T Model}
\label{sec:Lit KT}

\citet{KT} popularised a fire simulation CA model, called the K-T model, that uses a Moore's neighbourhood. A Moore's neighbourhood  In this model a given cell's state can be expressed as the ratio of its burning and non burning regions at a given time (see Equation~\ref{eq:State}). 

\begin{equation}
  \label{eq:State}
  S_{i,j}^{t} = \frac{A_{b}}{A_{t}}
\end{equation}

Where $ A_{b} $ is the area of the cell that is burning , and $ A_{t} $ is the total area of the cell and its state is a continuous scale between 0 and 1. 

The function used to determine a cell's state change, over a discrete time, step must account for the effects of all of the neighbouring cells, as well as the particular cell's preceding state \citep{KT}. Furthermore, it is possible to determine the time needed for a cell to be completely burned out ( $ S_{i,j}^{t} = 1 $ ). Should the cell $ (i,j) $ be completely unburned, and exactly one of its adjacent neighbours is completely burned out, then the cell $ (i,j) $ will be completely burned out after a time calculated using Equation~\ref{eq:Ta}.

\begin{equation}
	\label{eq:Ta}
	t_{a} = \frac{a}{R_{i,j}}
\end{equation}

Where $ a $ is the length of one side of a cell, and $ R_{i,j} $ is the initial rate of spread of cell $ (i,j) $ 

\citet{Wang} calculates the discrete time step for the CA ($ \tilde{t} $) as being the $ t_{a} $ value of the cell with the highest rate of spread (see eq. \ref{eq:Ta}). This can be described using Equation~\ref{eq:Time step}.

\begin{equation} 
  \label{eq:Time step}
  \tilde{t}=\frac{a}{R_{MAX}}, R_{MAX}=MAX\{ R_{i,j},0<i\leq n, 0 < j\leq m\}
\end{equation}

Where $ m $ and $ n $ are the number of rows and columns of the lattice respectively. 

\citet{Encinas} explain that, given the square nature of the cells, the effects of a cells adjacent neighbours is stronger than that of its diagonal neighbours, and as such, the adjacent and diagonal cell effects need to be calculated separately. This leads to a new rule given by \citet{KT} being that the time of an unburned cell $(i,j)$ to be completely burned out, given that exactly one of its diagonal neighbours is completely burned out is calculated using Equation~\ref{eq:Td}

\begin{equation}
\label{eq:Td}
t_{d} = \frac{\sqrt{2}a}{R_{i,j}} = \sqrt{2}t_a
\end{equation}

Where $ \sqrt{2}a $ is the diagonal length of the cell, as calculated using Equation~\ref{eq:Pythag}.

\begin{equation}
  \label{eq:Pythag}
  \begin{split}
    d^2 & = a^2 + a^2 \\ 
    d   & = \sqrt{2a^2} \\
        & = \sqrt{2}a
  \end{split}
\end{equation}

Furthermore, it is possible to calculate the impact that diagonal neighbours will have on the state of cell $(i,j)$ during a given time step using Equation~\ref{eq:lambda} \citep{KT}.

\begin{equation}
  \label{eq:lambda}
  \begin{split}
    S_{i,j}^{t+1} = \lambda &= \frac{a^2 - [(\sqrt{2} - 1)a]^2}{a^2} \\
                            &= 1 - (\sqrt{2} - 1)^2 \\
                            &\approx 0.83
  \end{split}
\end{equation}

\citet{Encinas} introduce a set containing the different possible offsets ($ \alpha $ and $ \beta $) for all of a given cell's neighbours. This can be seen in Equation~\ref{eq:Vm}.

\begin{equation}
\label{eq:Vm}
  V_{M} = {(-1,-1),(-1,0),(-1,+1),(0,-1),(0,+1),(+1,-1),(+1,0),(+1,+1)}
\end{equation}

In order to more easily distinguish between the adjacent cells and diagonal cells, two subsets of $ V_M $ containing the offsets for the two corresponding neighbour types are produced and can be seen in Equations~\ref{eq:Vadj}~and~\ref{eq:Vdiag}.

\begin{equation}
\label{eq:Vadj}
  V_{M}^{adj} = \{ (-1,0),(0,+1),(0,-1),(+1,0) \}
\end{equation}
\begin{equation}
\label{eq:Vdiag}
  V_{M}^{diag} = \{ (-1,-1),(-1,+1),(+1,-1),(+1,+1) \}
\end{equation}

Assuming homogenous fuel and discounting the effects of wind and slope, a state transition function over the discrete time step calculated by Equation~\ref{eq:Time step} can be described by Equation~\ref{eq:State Transition .83} \citep{Encinas}.

\begin{equation}
	\label{eq:State Transition .83}
		S_{i,j}^{t + 1} = S_{i,j}^{t} + \sum_{(\alpha,\beta)\in V_{M}^{adj}} S_{i + \alpha ,j + \beta}^{t} + 0.83 \sum_{(\alpha,\beta)\in V_{M}^{diag}} S_{i + \alpha ,j + \beta}^{t}
\end{equation}

\subsubsection{Berjak \& Hearne Model}
\label{sec:Lit Berjak}

\citet{Berjak} explain that a problem with the K-T model is that while it produces satisfactory fire front shapes, the position of the front is inaccurate. This is due to it handling wind and slope as weightings applied to a cell's state. The KT model was also only designed to work in environments where all fuel types are able to ignite all other fuel types. This causes inaccurate results when one fuel is not supposed to be able to produce enough heat to ignite another fuel.

A new model is proposed by \citet{Berjak} in order to overcome some of the shortcomings of the K-T model. This model takes a non-spacial fire spread model (see Section~\ref{sec:Lit Non-Spatial}) and applies it to a spacial model (the Cellular Automata). The non-spacial model used must be implemented without the wind and slope coefficients as these factors are handled differently within this model. The calculation for a fuel's base rate of spread is not done at every step, instead it is done at $ t = 0 $ and is furthermore denoted as $ R_0 $ \citep{Berjak}. Taking this into account it is possible to define a cell's state as a function of its heat dynamics (see Equation~\ref{eq:State Heat})

\begin{equation}
\label{eq:State Heat}
	S_{i,j}^{t} = f \left ( \frac{H_t}{H_0}, t \right )
\end{equation}

Where $ H_t $ represents the amount of heat that the cell has received, and $ H_0 $ represents the the amount of heat required for the cell to ignite (calculated at $ t = 0 $). \\

From Equation~\ref{eq:State Heat} it is possible to deduce three-discrete states that a cell can be in. These states are: unburned, burning, and burned \citep{Berjak}. An unburned cell is one which the amount of heat received by the cell is insufficient for ignition (Equation~\ref{eq:State Heat}~$ < 1 $). A burning cell is one which has received sufficient heat to ignite (Equation~\ref{eq:State Heat}~$ >= 1 $). Finally, a burned cell is one which has been extinguished or one which no longer produces enough heat through combustion to maintain the process. The default starting state for a cell is unburned and a cell that is either unburned or burned will have a rate of spread of zero ($ R_0 = 0 $) \citep{Berjak}.

This model uses the same neighbourhood as the K-T model and as such will share the same neighbourhood subsets described in Equation~\ref{eq:Vadj}~and~\ref{eq:Vdiag}. In order to handle environments with heterogeneous fuels, it is necessary to introduce an expression that represents whether one fuel produces sufficient heat to ignite another fuel \citep{Berjak}. This expression is called the combustibility index and is described in Equation~\ref{eq:CI}.

\begin{equation}
\label{eq:CI}
	CI = \frac{\left ( H_c \right )_\alpha}{\left ( H_0 \right )_\beta}
\end{equation}

Where $ \left ( H_c \right )_\alpha $ represents the amount of heat produced by fuel $ \alpha $ that is transferred to fuel $ \beta $, and $ \left ( H_0 \right )_\beta $ represents the total amount of heat required to ignite fuel $ \beta $. Should this expression evaluate to a value below one then it can be said that the rate of spread of fuel $ \alpha $ to fuel $ \beta $ is zero \citep{Berjak}. This phenomena is represented by a multiplier described in Equation~\ref{eq:Nf}.

\begin{equation}
\label{eq:Nf}
	\left \{ \begin{matrix}
		\eta_f = 0 & ,if~CI < 1  \\ 
		\eta_f = 1 & ,if~CI \geq 1
	\end{matrix} \right.
\end{equation}

Taking this multiplier into account, as well as factoring in wind and slope factors (discussed in Sections~\ref{sec:Lit CA Wind}), it is possible to create a new equation to represent the spacial rate of spread from one cell ($ i,j $) to another cell ($ k,l $) using Equation~\ref{eq:CA ROS}.

\begin{equation}
\label{eq:CA ROS}
	R_{i,j}^{t} = R_0 \times \left ( \phi_w \phi_s \right )_{k,l}^{t} \times \left ( \eta_f \right )_{k,l}^{t}
\end{equation}

Where $ \phi_w $ and $ \phi_s $ are the wind and slope multipliers respectively.

Once again, within a given time step, the heat transferred from a cell's diagonal neighbour is less than that transferred from its adjacent neighbour \citep{Berjak}. It is said that a diagonal neighbour will start transferring heat to its two adjacent neighbours first. This means that in the first $ t_a $ seconds, a cell will only receive heat from its diagonal neighbour. Following this, for the remaining time, the cell will receive heat from the diagonal cell and the two common adjacent neighbours. The rate to which a cell will receive heat from its neighbours can be calculated using Equation~\ref{eq:Lambda Berjak}.

\begin{equation}
	\label{eq:Lambda Berjak}
	\begin{split}
		\frac{\mathrm{d} H_t }{\mathrm{d} t} & = 
		\left \{ \begin{matrix}
	 		\lambda \left ( \frac{H_0}{t_d} \right ) &, if~0 \leq t < t_a \\ \\
	 		\lambda \left ( \frac{H_0}{t_d} \right ) + 2 \left ( \frac{H_0}{t_a} \right ) &, if~t_a \leq t \leq t_d 
		\end{matrix} \right. \\ \\
		\therefore H_t & = \int_{0}^{t_a} \left ( \frac{\lambda H_0}{t_d} \right )\mathrm{d}t + \int_{t_a}^{t_d} \left [ \left ( \frac{\lambda H_0}{t_d} \right ) + 2 \left ( \frac{H_0}{t_a} \right ) \right ]\mathrm{d} t \\
		& = \lambda H_0 + 2 H_0 \left ( \frac{td - ta}{ ta } \right ) \\ \\
		\therefore H_0 & = H_0 \left ( \lambda + 2 \left ( \sqrt{2}-1 \right ) \right ) \\ \\
		\therefore \lambda & \approx 0.17
	\end{split}
\end{equation}

This means that approximately 17\% of the required heat is provided by the diagonal neighbour, whereas the remaining 83\% is provided from the two common adjacent neighbours that started contributing once $ t \geq t_a $ \citep{Berjak}.

\citet{Berjak} explain that in this model, the new state transition function for the CA, over a discrete time step $ \Delta t $, is described by Equation~\ref{eq:State Transition Berjak}

\begin{equation}
\label{eq:State Transition Berjak}
	\begin{split}
		S_{i,j}^{t+1} & = S_{i,j}^{t} + \left \{ \left ( \frac{1}{a} \right )\sum_{\left ( \alpha, \beta \right ) \epsilon V_{M}^{adj}} R_{i + \alpha, j + \beta}^{t} \right \} \Delta t \\
		& +~ 0.17 \left \{ \left ( \frac{1}{\sqrt{2}a} \right )\sum_{\left ( \alpha, \beta \right ) \epsilon V_{M}^{diag}} R_{i + \alpha, j + \beta}^{t} \right \} \Delta t
	\end{split}
\end{equation}

The discrete states for a cell can be determined using the discretisation model shown in Equation~\ref{eq:Berjak Discrete}.

\begin{equation}
\label{eq:Berjak Discrete}
State = \left\{\begin{matrix*}[l]
	Unburned & ,if~S_{i,j}^{t+1} < 1 \\ 
	Burning & ,if~S_{i,j}^{t+1} >= 1~and~ \left (t + \Delta t  \right ) < \left ( t_a + t_d \right )\\ 
	Burned & ,if~\left (t + \Delta t  \right ) \geq \left ( t_a + t_d \right )
	\end{matrix*}\right.
\end{equation}

% ----------------------------------------------------------------------------
%                                  CA WIND
% ----------------------------------------------------------------------------
\subsection{Modelling Wind and Slope in a CA Approach}
\label{sec:Lit CA Wind}
As was discussed in Section~\ref{sec:Lit Rothermel W&S}, The spread rates of the head of the fire front differs from that of the rear. This is due to the topographical effects of wind and slope. The spread rate of a fire travelling down a slope is slower than that of a fire travelling up a slope due to the flame angle being greater relative to the fuel surface. Wind is seen as affecting the heat transfer of heat from a flame to an adjacent fuel by changing the fire's geometry \citep{Berjak}. Applying these non-spacial fire spread concepts to a spacial CA model is done by calculating the relative effects of each of a cells neighbours on that cell. Since both slope and wind are analogous in the fact that they both effect fire spread rates by effecting the flame angle, they are often done together \citep{Berjak}.

\subsubsection{Wang \textit{et al.}'s Method for Wind Factors}
\citet{Wang} propose using vector decomposition in order to calculate the effect that wind will have on a cell's heat transfer to its neighbouring cells. The vector used is the wind power (represented as $ D $) and can be obtained by another means (for example: see Section~\ref{sec:Lit Rothermel W&S}). It must be noted that the process of vector decomposition will result in negative values, however, there cannot be a negative fire spread in real life, so all negative values are said to have no effect on the flame (weighting is set to 1) \citep{Wang}. An example of the decomposition of the vector can be seen in \ref{fig:Wind Vector Decomposition} and a summary of the vector decompositions for all of a cells neighbours can be seen in Table~\ref{table:Wind Vector}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{lit/figures/wind_vector_decomposition.png}
	\caption{Example Decomposition of Wind Vector \citep{Wang}}
	\label{fig:Wind Vector Decomposition}
\end{figure}

Where a wind's direction is that of the wind's origin direction, for example: a westerly wind is one that originates from the west and blows eastward.

\begin{table}[H]
	\centering
	\caption{Summary of Wind Vector Decompositions \citep{Wang}}
	\label{table:Wind Vector}
	\begin{tabular}{| c | c | c | c | c |}
		\hline
		Cell ID & 1 & 2 & 3 & 4\\
		\hline
		Direction & $ 0^\circ $ & $ 45^\circ $ & $ 90^\circ $ & $ 135^\circ $ \\
		\hline
		N & $ 1 $ & $ 1 $ & $ 1 $ & $ 1 $ \\
		S & $ 1 $ & $ Dsin45^\circ $ & $ D $ & $ Dsin45^\circ $ \\
		W & $ D $ & $ Dsin45^\circ $ & $ 1 $ & $ 1 $ \\
		E & $ 1 $ & $ 1 $ & $ 1 $ & $ Dsin45^\circ $ \\
	 	NW & $ Dsin45^\circ $ & $ 1 $ & $ 1 $ & $ 1 $ \\
		NE & $ 1 $ & $ 1 $ & $ 1 $ & $ 1 $ \\
	 	SW & $ Dsin45^\circ $ & $ D $ & $ Dsin45^\circ $ & $ 1 $ \\
	 	SE & $ 1 $ & $ 1 $ & $ Dsin45^\circ $ & $ D $ \\
		\hline 
		\hline
		Cell ID & 5 & 6 & 7 & 8\\
		\hline
		Direction & $ 180^\circ $ & $ 225^\circ $ & $ 270^\circ $ & $ 315^\circ $ \\
		\hline
		N & $ 1 $ & $ Dsin45^\circ $ & $ D $ & $ Dsin45^\circ $ \\
		S & $ 1 $ & $ 1 $ & $ 1 $ & $ 1 $ \\
		W & $ 1 $ & $ 1 $ & $ 1 $ & $ Dsin45^\circ $ \\
		E & $ D $ & $ Dsin45^\circ $ & $ 1 $ & $ 1 $ \\
		NW & $ 1 $ & $ 1 $ & $ Dsin45^\circ $ & $ D $ \\
		NE & $ Dsin45^\circ $ & $ D $ & $ Dsin45^\circ $ & $ 1 $ \\
		SW & $ 1 $ & $ 1 $ & $ 1 $ & $ 1 $ \\
		SE & $ Dsin45^\circ $ & $ 1 $ & $ 1 $ & $ 1 $ \\
		\hline
	\end{tabular}
\end{table}

\subsubsection{Berjak \& Hearne's Method for Wind and Slope}
\citet{Berjak} explain that wind affects heat transfer through changing the angle of the flame relative to the fuel surface. This means that the radiant heat is increased for headfires (the portion of the fire front that is parallel to the direction of the wind) and the radiant heat is decreased for backfires. This is due to the flame being tilted towards the fuel of the headfire. Figure~\ref{fig:Flame Angle} describes the flame geometry of a line fire.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{lit/figures/flame_angle.png}
	\caption{Flame Geometry Description \citep{Weise_Effect}}
	\label{fig:Flame Angle}
\end{figure}

The flame angle $ \theta_f $ is measured from the vertical of the fuel surface and positive values indicate that the flame is tilted in the same direction as the fire spread, and conversely for negative values. \citet{Trollope1984} explains that wind speed does not play a significant effect on a flame's height given that it is a surface grass fire (no crown fire), and that the wind speed did not exceed 5.6 $ m s^{-1} $. \citet{Trollope1978} found that the mean flame height for headfires was 2.8 m with a standard error of 0.4 m and for backfires was 0.8 m with a standard error of 0.1 m. Using these findings, \citet{Berjak} use 2.8 m as the value for flame height for all equations that require it.

\citet{Weise_Effect} provides two alternatives for estimating flame angle through the use of two different calculations of a Froude number. One using a flame's height and the other its length. These can be seen in Equations~\ref{eq:Flame Angle Height}~and~\ref{eq:Flame Angle Length}.

\begin{equation}
\label{eq:Flame Angle Height}
	\theta_f = \tan^{-1} \left ( 2.35 \left ( U^2/gH \right )^{0.57} \right ), R^2 = 0.61
\end{equation}

\begin{equation}
\label{eq:Flame Angle Length}
	\theta_f = \tan^{-1} \left ( 2.67 \left ( U^2/gL_f \right )^{0.57} \right ), R^2 = 0.48
\end{equation}

Where $ U $ represents the wind speed in $ m s^{-1}$, $ g $ is the gravitational force of the area (for earth it is approximated to $9.8~m s^{-1}$), $H$ represents the flame height, and $L_f$ represents the flame length. 

\citet{Weise_Effect} also propose an alternative to using a Froude number, being a convection number (see Equation~\ref{eq:Nc}), giving a new estimation of the flame angle described in Equation~\ref{eq:Flame Angle Convection}.

\begin{equation}
\label{eq:Nc}
	N_c = 2gh_cwRpc_pT(U - R)^3
\end{equation}

\begin{equation}
\label{eq:Flame Angle Convection}
	\theta_f = \tan^{-1} \left ( 3.08 N_c^{0.57} \right ), R^2 = 0.48
\end{equation}

Where $h_c$ represents the heating content, $ w $ represents the fuel loading, $ p $ is the ambient air density, $c_p$ is the specific heat capacity, $T$ is the absolute temperature, and $R$ is the rate of spread.

The multiplier applied to the base rate of spread of a particular cell ($\phi_w$) is described in Equation~\ref{eq:Wind Coeff}. The flame angle is calculated using another means \citep{Berjak}. It was concluded by \citet{Berjak} that the Weise and Briging flame height model described in Equation~\ref{eq:Flame Angle Height} was the most fitting for this CA model.

\begin{equation}
\label{eq:Wind Coeff R}
	R = R_0 exp(\beta \theta_f)
\end{equation}

Where, using the natural logarithm of this equation, the $\beta$ value for each model is estimated through the least squares linear regression method \citep{Berjak}. The $\beta$ value for the Weise and Briging flame height model was estimated as $0.0576$ with a correlation coefficient $r$ of $0.87$. This correlation coefficient is significant at the 5\% level. Taking this $\beta$ value into account Equation~\ref{eq:Wind Coeff R} is adapted to describe the wind coefficient ($\phi_2$) and is described in Equation~\ref{eq:Wind Coeff}.

\begin{equation}
\label{eq:Wind Coeff}
	\phi_w = exp(0.0576 \theta_f)
\end{equation}

The flame angle calculated is the flame angle in the heading direction. For a rate of spread in the opposite direction the flame angle is negated ($ \theta_f = -\theta_f $), and in all other directions the flame angle is set to 0 ($ \theta_f = 0 $) \citep{Berjak}. If the wind has a diagonal direction, then the wind vector is decomposed into its two nearest adjacent directions (for example: a north-east wind will have vector components in the north and east directions). This is calculated using the cosine of the angle between the wind and direction of fire spread. From this each of a cell's neighbours effects can be calculated using the relative flame angles. \\

Slope Effects can be introduced into the model by calculating the slope between the two cells. This slope is calculated by taking the difference in elevation between the two cells and dividing it by the horizontal distance between them \citep{Berjak}. This is described in Equation~\ref{eq:Slope}. This slope can then be incorporated into a modifier to rate of spread ($\phi_s$) described in Equation~\ref{eq:Slope Coeff}.

\begin{equation}
	\label{eq:Slope}
	\theta_s = \left \{\begin{matrix}
		\tan^{-1} \left (\frac{Elev_{k,l}-Elev_{i,j}}{a} \right ) & , if~adjacent \\ 
		\tan^{-1} \left (\frac{Elev_{k,l}-Elev_{i,j}}{\sqrt{2}a} \right ) & , if~diagonal 
	 \end{matrix}\right.
\end{equation}

\begin{equation}
\label{eq:Slope Coeff}
	\phi_s = exp(0.0693 \theta_s)
\end{equation}

Taking both Equations~\ref{eq:Wind Coeff}~and~\ref{eq:Slope Coeff} into one equation and applying it to the cells base rate of spread results in Equation~\ref{eq:Topo ROS}

\begin{equation}
\label{eq:Topo ROS}
	R_{i,j}^t = R_0 exp \left( 0.0693 \left( \theta_s \right )_{k,l} + \left ( 0.0576 \theta_f \right )^t_{k,l} \right ) \left ( \eta_f \right )_{k,l}
\end{equation}

% ----------------------------------------------------------------------------
%                          MESH BASED FIRE SIMULATION
% ----------------------------------------------------------------------------

\subsection{Mesh Based Fire Simulation}
\label{sec:Lit Mesh Method}
\citet{Beaudoin} use a traditional triangular mesh to represent the surface that the fire will spread on, and explain that creating an accurate simulation of fire is very challenging as well as often computationally expensive. They suggest splitting the challenge of creating a controllable and realistic fire simulation into three phases: rendering, animation and propagation.

A technique for rendering fire that is often used is utilising a particle system. However, a problem with this method is that: using a large number of particles is very computationally intensive, and it results in a fuzzy, cloud-like appearance to flames which is unlike the crisp outlines of real fires \citep{Beaudoin}. The method that has been proposed by \citet{Beaudoin} is vector-based and splits the mathematical modelling of fire propagation into the evolution of several variables surrounding the burning region: temperature, pressure, and air velocity. This is achieved by having a smaller set of flames represented by deformable chains of vertices.

\citet{Beaudoin} explain that the locally defined variables used are: the density of the fuel, oxygen supply, wind, as well as the orientation of the surface (relative to gravity). Using this relatively small number of parameters as well as tracking the boundary between the burning and non-burning regions of the surface, a simplified fire propagation simulation can be created at relatively low cost \citep{Beaudoin}.

A boundary is an enclosed line which can give a clear indication between two different areas (see Figure~\ref{fig:Mesh Boundary}), and the vertices that make up the boundary line have two main properties: position and velocity \citep{Beaudoin}. Furthermore, should two vertices exist on different faces on the mesh, a third point will be spawned on the connected edge of the two faces.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{lit/figures/boundry.png}
	\caption{Broken-line boundary on a mesh \citep{Beaudoin}}
	\label{fig:Mesh Boundary}
\end{figure}

\citet{Beaudoin} explain that defining the boundary expansion through the velocity of its vertices is relatively easy, given that the vertex remains on the same face. The equation that they use to describe this vector displacement is given in Equation~\ref{eq:Mesh-Vector Displacement}.

\begin{equation}
\label{eq:Mesh-Vector Displacement}
	P_{i}^{t+\Delta t} = P_{i}^{t} + v_{i}^{t}\Delta t
\end{equation}

Where for a given vector $i$, $ P_{i} $ denotes its position and $ v_{i} $ denotes its velocity.

Should a vertex move onto a different face, it becomes necessary to recalculate its velocity so that it is relative to the new mesh, as well as, to account for the displacement caused by the transition over the edge \citep{Beaudoin}. The equation for this is given in Equation~\ref{eq:Mesh-New Face Transition}.

\begin{equation}
\label{eq:Mesh-New Face Transition}
	v'_{i} = \eta (N_{i} \times v_{i}) \times N'
\end{equation}

Where for the displaced vertex $i$; $ \eta $ is a pre-chosen coefficient that aims to preserve the magnitude of the original velocity, $ N_{i} $ is the linearly interpolated normal at the point of crossing, and N' is the vector normal to the vertex's destination face (see Figure~\ref{fig:Mesh Velocity Transform}).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{lit/figures/velocity_transform.png}
	\caption{Transforming a vertex's velocity \citep{Beaudoin}}
	\label{fig:Mesh Velocity Transform}
\end{figure}

Tracking the fire spread/growth over time is done using a two-step update at every predetermined time step interval \citep{Beaudoin}: Firstly the new position of each vertex is calculated using Equation~\ref{eq:Mesh-Vector Displacement} as well as updating its velocity should it cross an edge (Equation~\ref{eq:Mesh-New Face Transition}). Secondly, any new vertices needed due to face transitions are spawned, and excess vertices caused by multiple vertices coming into close proximity are destroyed. When tracking the fire's propagation it is important to take the non-uniform nature of the speeds of propagation into account \citep{Beaudoin}. This is enforced by using the locally defined parameters to alter each vertex's velocity at each given time step.

\subsection{Level Set Method for Heterogeneous Environments}
\label{sec:Lit Level Set}
\citet{Lo} conducted research into creating a combination of raster and vector-based methods (discussed in Section~\ref{sec:Lit Raster vs Vector Fire} to best utilise their strength's while trying to minimise or overcome their relative shortcomings. The method they propose is to store the local variables and fire data in the cell, however, instead of using them to calculate the ignition probability and state for every cell, the fire spread is calculated using the level set method. 

The base rate of spread was adapted from using Equation~\ref{eq:Rothermel ROS W&S} from Rothermel's method for fire spread calculations. This adapted equation can be seen in Equation~\ref{eq:LS ROS}

\begin{equation}
	\label{eq:LS ROS}
		\begin{cases}
			F = \left ( \varepsilon_0 + W\left ( U,\theta_w \right ) + \varepsilon_0\phi_s \right )\eta_f & \text{ ,if } \left | \theta_w \right | \leq \frac{\pi}{2} \\ 
			F  = \left ( \varepsilon_0\left [ \alpha + \left ( 1 - \alpha \right )\left | \sin \theta_w \right | \right ]+ \varepsilon_0 \phi_s \right )\eta_f & \text{ ,if } \left | \theta_w \right | > \frac{\pi}{2}
		 \end{cases}
\end{equation}
\begin{equation*}
	\begin{split}
		\text{Where:} &  \\
		& W \left ( U, \theta_w \right ) = a \sqrt{U \cos^3 \theta_w} \\
		\text{And:} & \\
		& \phi_s = 5.275 \beta^{-0.3} \left ( \tan \theta_t \right )^2 	 
	\end{split}
\end{equation*}

$\theta_w$ represents the angle between the head of the fire front and the wind direction, $\theta_t$ represents the slope, $\alpha$ is the ratio between the rear and flank velocities, and $\eta_f$ is taken from Equation~\ref{eq:Nf}.

The level set method is used to track moving surfaces or simulating graph evolution. This is done by transferring a two-dimensional problem to a higher dimension, then transferring the problem back to two dimensions \citep{Osher}. This resolves the crossover issue described in Section~\ref{sec:Lit Raster vs Vector Fire} and produces propagating contours. The graph evolution is modelled using Partial Differential Equations (PDE) \citep{Lo}.

Within the level set method, the surface consists of points containing the signed distance between it and the moving graph \citep{Lo}. Points with a negative distance are within the fire front, and points with a positive distance are outside the front. Points with a zero distance represent the moving fire front, also known as its zero level set. A three dimensional view of the two surfaces along with their corresponding signed distance lattices can be seen in Figure~\ref{fig:Level Set Explanation}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{lit/figures/level_set_3d.png}
	\caption{Evolution of the Fire Front \citep{Lo}}
	\label{fig:Level Set Explanation}
\end{figure}

In order to calculate the new level set value for a given point $x$ it is necessary to add the value of the level set equation at a given time to the old value at that point. This is illustrated in Equation~\ref{eq:Level Set} \citep{Lo}. In order to calculate this new value, it is necessary to calculate the new level set equation. The new level set function in Hamilton-Jacobi form can be seen in Equation~\ref{eq:Level Set Update}.

\begin{equation}
\label{eq:Level Set}
	\phi_{new}(x) \approx \phi_{old} + \Delta t \cdot \phi'(t)
\end{equation}

\begin{equation}
\label{eq:Level Set Update}
	\phi'(t) = -F \left | \bigtriangledown \phi \right |
\end{equation}

Where $\bigtriangledown \phi$ is the gradient of $\phi$ with respect to x.

\citet{Lo} explains that while the level set equation allows the fire front to be tracked, it updates every point in the lattice every update. This is not necessary and using the narrow band method can limit the level set to only solve the points in a narrow band around the fire front. This reduces the computational complexity of this method from $O(n^2)$ to $O(n)$. Figure~\ref{fig:Level Set} Provides two examples of simulations run using the narrow band level set method.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \includegraphics[width=\linewidth]{lit/figures/level_set_nowind.png}
    \caption{No wind or slope}
  \end{subfigure}%
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \includegraphics[width=\linewidth]{lit/figures/level_set_wind.png}
    \caption{Westerly wind}
  \end{subfigure}
  \caption{Example simulations run using the narrow band level set method \citep{Lo}}
  \label{fig:Level Set}
\end{figure}
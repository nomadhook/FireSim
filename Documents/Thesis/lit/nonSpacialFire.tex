\section{Non-Spacial Fire Spread}
\label{sec:Lit Non-Spatial}
Non-spacial fire spread is defined as calculating the fire spread at the head of the fire front. It serves as the base for calculating the rate of spread of a particular fuel in a quantitative way \citep{Rothermel}.

% ----------------------------------------------------------------------------
%                           ROTHERMEL FIRE SPREAD
% ----------------------------------------------------------------------------

\subsection{Rothermel's Model for Calculating Rate of Spread}
\label{sec:Lit Rothermel}
Rothermel's fire model was developed in 1979 as the first quantitative spread prediction model used by the Forest Service and is still the main spread prediction tool in use today \citep{Wells}. Furthermore, \citet{Wells} describes Rothermel's method as simplifying fire behaviour by applying it to a simpler, more controllable ``universe" that is covered with small, non-described fuel cells on the floor. \citet{Rothermel} explains that a fundamental observation made by prior work (which is still relevant today) is that the first essential process to creating a fire spread model is to heat the fuel ahead of the fire boundary as it progresses. Furthermore, abstracting a fire's spread into a series of separate ignitions (each cell in a grid can represent one ignition) helps to break the problem down into more manageable and explainable chunks. 

\citet{Frandsen} applies the concepts of energy conservation to a unit of fuel ahead of an advancing fire front in order to calculate its rate of spread in an homogenous fuel bed. This analysis led to Equation~\ref{eq:Frandsen ROS}. 

\begin{equation}
\label{eq:Frandsen ROS}
  R_0 = \frac{I_{xig} \int_{-\infty}^{o}\left ( \frac{\partial I_z}{\partial z} \right )_{z_c} \mathrm{d}x}{p_{be} Q_{ig}}
\end{equation}

Where: $R_0$ is the quazi-steady rate of spread, $I_{xig}$ is the horizontal heat flux that is absorbed by a unit of volume of fuel at the time of ignition, $Q_{ig}$ is the heat of pre-ignition, and $\left ( \frac{\partial I_z }{\partial z} \right )_{z_c}$ represents the gradient of the vertical intensity evaluated at a plane at a constant depth \citep{Rothermel}. The combination of the horizontal flux, and the vertical flux that has been integrated from negative infinity to the fire front, results what is called the propagating flux.  

The rate of fire spread during what is called the quasi-steady state is equated as the ratio of source heat flux to the heat required for ignition of potential fuel \citep{Rothermel}. Furthering this, calculating the heat required for the ignition of a fuel is dependent upon three properties: the fuel moisture, the ignition temperature, and the amount of fuel involved.

\citet{Rothermel} explains that Equation~\ref{eq:Frandsen ROS} contains heat flux terms that have unknown heat transfer mechanisms. Due to this, it is not possible to solve this equation analytically. A new equation was presented by \citet{Rothermel} in order to provide an approximate solution to Equation~\ref{eq:Frandsen ROS}. This new definition for quasi-steady rate of spread in the absence of wind or slope can be seen in Equation~\ref{eq:Rothermel ROS} and is measured in $ft.min.^{-1}$.

\begin{equation}
\label{eq:Rothermel ROS}
  R_0 = \frac{I_R \xi}{p_b \varepsilon Q_{ig}}
\end{equation}

Where $I_R$ represents the reaction intensity, $\xi$ represents the propagating flux ratio, $p_b$ represents the ovendry bulk density, $\varepsilon$  represents the effective heating number, and $Q_{ig}$ represents the heat of pre-ignition \citep{Rothermel}. 

The reaction intensity of a fuel is that fuel's energy release rate, and is produced by the burning gasses emitted from the fuels organic matter \citep{Rothermel}. Subsequently, the heat release rate per unit area is a function of the fuel's particle size, bulk density, moisture, and chemical composition. The reaction intensity is the source of the propagating flux in the absence of wind. Equations~\ref{eq:Rothermel All Start}~to~\ref{eq:Rothermel All End} contain all of the equations needed to calculate the reaction intensity, and subsequently, the rate of spread.

\newcommand \wtext[1]{\begin{tabular}{@{}l@{}}#1\end{tabular}}
\begingroup
\addtolength{\jot}{.8em}
\begin{flalign}
  \label{eq:Rothermel All Start}
  &I_R = \Gamma' w_n h \eta_M \eta_s & \wtext{Reation intensity}& \\
%
  \label{eq:Gamma}
  &\Gamma' = \Gamma'_{max} \left ( \frac{\beta}{\beta_{op}} \right )^A exp \left [ A \left ( 1 - \frac{\beta}{\beta_{op}} \right ) \right ] & \wtext{Optimum reaction velocity} &\\
%
\label{eq:Gamma Max}
  &\Gamma'_{max} = \sigma^{1.5} \left (495 + 0.0594\sigma^{1.5} \right )^{-1} & \wtext{Max reaction velocity} &\\
%
  \label{eq:beta_op}
  &\beta_{op} = 3.348 \sigma^{0.8189} & \wtext{Optimum packing ratio} & \\
%
\label{eq:A}
  &A = 1 / \left (  4.774\sigma^{0.1} - 7.27 \right ) & \wtext{--} & \\
%
\label{eq:Eta M}
  &\begin{aligned}
  \eta_M &= 1 - 2.59 \frac{M_f}{M_x} + 5.11 \left ( \frac{M_f}{M_x} \right )^2 \\
  & \qquad - 3.52 \left ( \frac{M_f}{M_x} \right )^3
  \end{aligned}& \wtext{Moisture damping coefficient} & \\
%
\label{eq:Eta S}
  &\eta_s = 0.174 S_e^{-0.19} & \wtext{Mineral damping coefficient} & \\
%
\label{eq:Xi}
&\begin{aligned}
    \xi =& (192 + 0.2595 \sigma)^{-1} \\
        &\qquad exp \left [ \left ( 0.792 + 0.681 \sigma^{0.5} \right ) \left ( \beta + 0.1 \right ) \right ]
  \end{aligned}& \wtext{Propagating flux ratio} & \\
%  
\label{eq:Wn}
  &w_n = \frac{w_o}{1 + S_t} & \wtext{Net fuel loading} & \\
% 
\label{eq:pb}
  &p_b = \frac{w_0}{\delta} & \wtext{ovendry bulk density} & \\
%
\label{eq:epsilon}
  &\varepsilon = exp \left ( -138 / \sigma \right ) & \wtext{Effective heating number}& \\
%
\label{eq:Qig}
  &Q_{ig} = 250 + 1116 M_f & \wtext{Heat of pre-ignition}& \\
%
  \label{eq:Rothermel All End}
  &\beta = \frac{p_b}{p_p} & \wtext{Packing ratio} & 
\end{flalign}
\endgroup

\citet{Albini} groups the thirteen fuel models into four generalising groups: Grass and Grass-Dominated Fuel Complexes, Chaparral and Shrubfields, Timber Litter, and Logging Slash. The fuel model data are based on typical fuel models, trading off complete accuracy for providing easy to compare, rough estimates to be used in hazard assessment and planning. Table~\ref{table:Fuel Models} provides the fuel model descriptions and parameters for the thirteen fuel models provided by \citet{Albini}, as well as, certain parameters that are constant across all fuel models provided by \citet{Rothermel}. A fuel can be either living or dead, and dead fuels are further split into three categroies: 1-Hour (fine), 10-Hour (medium), and 100-Hour (large) \citep{Anderson}. These categories represent the fuel's timelag measured in hours and are based on the fuel's diameter. A fuel is considered a fine fuel if it has a diameter of less than \nicefrac{1}{4} inches, a medium fuel if it has a diameter between \nicefrac{1}{4} and 3 inches, and a large fuel if it has a diameter between 1 and 3 inches \citep{Anderson}. 

\begin{table}[H]
  \centering
  \caption{Fuel Model Descriptions and Parameters}
  \label{table:Fuel Models}
  \resizebox{\textwidth}{!}{\begin{tabular}{c l c c c c c c c c c c}
  \hline
   & & \multicolumn{6}{c}{Dead Fuel} & & & & \\
  \cline{3-8}
   & & \multicolumn{2}{c}{1-H} & \multicolumn{2}{c}{10-H} & \multicolumn{2}{c}{100-H} & \multicolumn{2}{c}{Live} & & $M_x$ \\
  \cline{3-8} \cline{9-10}
  No. & Typical Fuel & $\sigma$ & $w_o$ & $\sigma$ & $w_o$ & $\sigma$ & $w_o$ & $\sigma$ & $w_o$ & $\delta$ & Dead \\
  \hline
  & & $ft.^{-1}$ & $lb./ft.^2$ & $ft.^{-1}$ & $lb./ft.^2$ & $ft.^{-1}$ & $lb./ft.^2$ & $ft.^{-1}$ & $lb./ft.^2$ & $ft.$ & \% \\
  \hline 

  \multicolumn{12}{c}{\underline{GRASS AND GRASS DOMINATED}} \\
  1 & Short Grass (1 ft.) & 3500 & 0.034 & -- & -- & -- & -- & -- & -- & 1.0 & 12 \\
  2 & Timber (Grass and Understory) & 3000 & 0.092 & 109 & 0.046 & 30 & 0.023 & 1500 & 0.023 & 1.0 & 15 \\
  3 & Tall Grass (2.5 ft.) & 1500 & 0.138 & -- & -- & -- & -- & -- & -- & 2.5 & 25 \\
  
  \multicolumn{12}{c}{\underline{CHAPARRAL AND SHRUBFIELDS}} \\
  4 & Chaparral (6 ft.) & 2000 & 0.230 & 109 & 0.184 & 30 & 0.092 & 1500 & 0.230 & 6.0 & 20 \\
  5 & Brush (2 ft.) & 2000 & 0.046 & 109 & 0.023 & -- & 1500 & 0.092 & 2.0 & 20 \\
  6 & Dormant Brush, Hardwood Slash & 1750 & 0.069 & 109 & 0.115 & 30 & 0.092 & -- & -- & 2.5 & 25 \\
  7 & Southern Rough & 1750 & 0.052 & 109 & 0.086 & 30 & 0.069 & 1550 & 0.017 & 2.5 & 40 \\

  \multicolumn{12}{c}{\underline{TIMBER LITTER}} \\
  8 & Closed Timber Litter & 2000 & 0.069 & 109 & 0.046 & 30 & 0.115 & -- & -- & 0.2 & 30 \\
  9 & Hardwood Litter & 2500 & 0.134 & 109 & 0.019 & 30 & 0.007 & -- & -- & 0.2 & 25 \\
  10 & Timber (Litter and Understory) & 2000 & 0.138 & 109 & 0.092 & 30 & 0.230 & 1500 & 0.092 & 1.0 & 25 \\

  \multicolumn{12}{c}{\underline{LOGGING SLASH}} \\
  11 & Light Logging Slash & 1500 & 0.096 & 109 & 0.207 & 30 & 0.253 & -- & -- & 1.0 & 15 \\
  12 & Medium Logging Slash & 1500 & 0.184 & 109 & 0.644 & 30 & 0.759 & -- & -- & 2.3 & 20 \\
  13 & Heavy Logging Slash & 1500 & 0.322 & 109 & .059 & 30 & 1.288 & -- & -- & 3.0 & 25 \\
  \hline
  \multicolumn{12}{l}{For all models: $S_t = 0.0555$, $S_e=0.010$, $h = 8000~B.t.u./lb.$, $p_p=32~lb./ft.^3$} \\
  \hline
  \end{tabular}}
\end{table}

Where $ \sigma $ represents the fuel particle density, $w_o$ represents the ovendry fuel loading, $\delta$ represents the fuel depth, $S_t$ us the fuel particle total mineral content, $S_e$ is the fuel particle effective mineral content, $h$ is the fuel particle low heat content, and $p_p$ represents the ovendry particle density. Calculating the moisture content of extinction ($M_x$) for live fuels is based on the ratio of living to dead fuels, as well as, the moisture content of the fine (1-H) dead fuel \citep{Rothermel}. This equation can be seen in Equation~\ref{eq:Mx Living}.

\begin{equation}
\label{eq:Mx Living}
  \left ( M_x \right )_{living} = max \left ( 0.30,~ 2.9 \left ( \frac{1 - a}{a} \right ) \left [ 1 - \frac{10}{3} \left ( M_f \right )_{dead} \right ] - 0.226 \right )
\end{equation}

Where $a$ represents the ratio of the mass of fine living fuel to the mass of total fine fuel. Incorporated into Rothermel's model is the inclusion of fuel mass loss to the calculations for reaction velocity. This is because as the fire burns up the fuel, the fuel starts to lose mass \citep{Rothermel}. Figure~\ref{fig:Mass Loss} illustrates the fuels mass loss over time, starting from when the fire front reaches the fuel.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{lit/figures/mass_loss.png}
  \caption{Mass loss, and its derivative, over time \citep{Rothermel}}
  \label{fig:Mass Loss}
\end{figure}

% ----------------------------------------------------------------------------
%                           ROTHERMEL WIND AND SLOPE
% ----------------------------------------------------------------------------
\subsection{Rothermel's Model for Wind and Slope modifiers}
\label{sec:Lit Rothermel W&S}
Wind and slope effect a fire's rate of spread by changing the propagating heat flux \citep{Rothermel}. These changes are done by exposing the fuel to increased radiant and convective heat. Figure~\ref{fig:Rothermel W&S} provides an illustration for these three possible schematics of fire behaviour.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[width=\linewidth]{lit/figures/fire_nowind.png}
    \caption{No wind or slope}
  \end{subfigure}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[width=\linewidth]{lit/figures/fire_wind.png}
    \caption{Wind-driven}
  \end{subfigure}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[width=\linewidth]{lit/figures/fire_up.png}
    \caption{Up-slope}
  \end{subfigure}
  \caption{Schematics of fire \citep{Rothermel}}
  \label{fig:Rothermel W&S}
\end{figure}

When a fire is wind-driven or travelling up-slope, the flame angle relative to the floor is decreased which increases the amount of convection and radiant heat. The wind and slope effects are included into the rate of spread calculation from Equation~\ref{eq:Rothermel ROS} as modifiers to the fuel's reaction intensity. This new Equation can be seen in Equation~\ref{eq:Rothermel ROS W&S}.

\begin{equation}
\label{eq:Rothermel ROS W&S}
  R = \frac{I_R \xi \left ( 1 + \phi_w + \phi_s \right )}{p_b \varepsilon Q_{ig}}
\end{equation}

Where $\phi_w$ and $\phi_s$ represent the dimensionless modifiers applied by wind and slope effects respectively. The slope modifier is calculated using Equation~\ref{eq:Rothermel Wind}, and the wind modifier is calculated using Equation~\ref{eq:Rothermel Slope}.

\begin{equation}
\label{eq:Rothermel Slope}
  \phi_s = 5.27 \beta^{-0.3} \left ( \tan \theta_s \right )
\end{equation}
Where $\beta$ is calculated using Equation~\ref{eq:Rothermel All End}, and $\theta_s$ represents the slope angle.

\begin{equation}
  \label{eq:Rothermel Wind}
  \begin{split}
    \phi_w &= CU^B \left ( \frac{\beta}{\beta_{op}} \right )^{-E} \\
    Where: \\
    C      &= 7.47 exp\left ( -0.133 \sigma^{0.55} \right ) \\
    B      &= 0.02526\sigma^{0.55} \\
    E      &= 0.715 exp \left ( -3.59 \times 10^{-4}\sigma \right)
  \end{split}
\end{equation}

$U$ represents the wind speed in $ft. min.^{-1}$, $\beta_op$ is calculated using Equation~\ref{eq:beta_op}, and $\sigma$ is gathered from the relevant fuel on Table~\ref{table:Fuel Models}.
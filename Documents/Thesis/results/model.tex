\chapter{Results and Discussion}
\label{chap:Results}
This chapter is split into two major sections: Model, and Performance discussions. 

\section{Model Discussion}
This section will be focussed on the model itself and the output it gives. The data was gathered using the GUI system discussed in Section~\ref{sec:GUI PoC}. In order to keep the data consistent and easily predictable. Unless otherwise stated, the fuel will always be combustible and the rate of spread was set to $0.5~m/sec$, the scale was set to $10 m$ and the grid was set to 100x100. 

\subsection{No Wind or Slope}
Figure~\ref{fig:Results No Wind or Slope} provides the front evolution for a fire propagating over flat terrain under no wind conditions. As can be seen, the fire front is not a perfect circle or ellipse. This has to do with how the model treats diagonal spread, and that the data itself has the region split into squares. The spread rate does however spread uniformly as expected, in that the spaces between the evolution contours are uniform in size.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{results/figures/0_0.png}
  \caption{Fire front evolution with no wind or slope}
  \label{fig:Results No Wind or Slope}
\end{figure}

\subsection{Wind and Slope Effects}
Figure~\ref{fig:Wind and Slope} provides a various simulation results for different wind and slope settings (keeping all else the same). As with Figure~\ref{fig:Results No Wind or Slope}, the fire front often has distinct differences in the shape of the adjacent and diagonal portions, unlike a circular or elliptical shape. While this model does not produce perfectly accurate fire fronts, it still illustrates the various fire spread concepts in a predictable and easy-to-see manner. It is clearly visible that a fire will spread quicker when travelling up-slope, with the higher the slope, the faster the rate of spread. This is conversely true for travelling down-slope. When wind is introduced, the fire spread in the same direction of the wind is increased, the fire spread in the opposite direction to the wind is decreased, and all other directions are unaffected. Figure~\ref{fig:Wind and Slope Mayhem} provides a more complex example of fire spread under the effects of wind and complex terrain.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.8\textwidth]{results/figures/matrix.png}
  \caption{Fire front evolution with variable wind and slope}
  \label{fig:Wind and Slope}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{results/figures/full.png}
  \caption{Illustration of fire spread in generated terrain}
  \label{fig:Wind and Slope Mayhem}
\end{figure}

\subsection{Overshooting}
A major issue with the use of any of the proposed CA models for real-time is a phenomenon that will be further termed overshooting. Overshooting is when in one time step, a cell's state transitions from being less than one to more than one. This means that the cell did not actually take the full time step to become fully burned out, but rather it took a portion of the time step. The reason why this issue is severe is because for the portion of the time step that the cell was greater than one, that cell is supposed to have an impact on its neighbours, however because of how these methods are done, it does not.

To better explain this issue, an example is given. Say that for a given time step of 1 sec, a cells state transitions from 0.9 to 1.3. This means that for 25\% of the time step (250 ms) the cell remains unburned, however, for 75\% of the time step (750ms) the cell should be in the burning state. Thus for 750ms, the cell's neighbours should take this cell as burning, however, currently they do not. 

This issue causes further problems as it causes a ripple effect. What if the cell described earlier had a neighbour that, in this extra 750 ms update, will overshoot. This creates a ripple effect that means that doing just a single extra update would not solve. However, it could be argued that this ripple effect will have diminishing effects as if this previously described situation would occur, this second overshoot would only occur for a portion of a portion of the time step. 

Lastly, this phenomenon causes some level of inaccuracy with regards to tracking of when a cell would be extinguished. This is because the time captured for when a cell becomes fully burned out is initially taken as $ t + \Delta t $. This issue is easy, and computationally light, to solve by adjusting the captured burning time as described in Equation~\ref{eq:New Time Burning}.

\begin{equation}
\label{eq:New Time Burning}
t_s = t + \left ( \frac{1 - S_{i,j}^{t+1}}{S_{i,j}^{t+1} - S_{i,j}^{t}} \right ) \Delta t
\end{equation}

It may be beneficial to perform a second pass on the update method in order to remove the first order overshoots. While this does not completely remove the overshooting problem, it will remove the biggest offenders. It could be argued that the update can be continuously stepped through until there are no more overshoots. While this may work ideally for a non-real-time system, each extra update that is performed will add to the execution time. This will even have other adverse effects on the number of overshoots, which is discussed later. 

Table~\ref{table:Overshoot Sim Speed} contains the data captured for overshooting cells over a given number of time steps, set DEM scale, and constant rate of spread. Where: Model represents the model in use as well as whether it is single or double update, ``$\Delta t$'' represents that set delta time for each update, ``Total \#'' represents the total number of overshoots, ``Average \#'' represents the average number of overshoots per time step, and ``Average Amt.'' represents the average overshot amount. Table~\ref{table:Overshoot Scale} provides similar data however instead of changing the simulation speed, the DEM scale is changed. Table~\ref{table:Overshoot ROS} provides data on controlled simulation speed and DEM scale, but variable rate of spread. The overshooting was set with a tolerance of 0.1. This means that only cells that reach a state of 1.1 or higher are considered overshot.

\begin{table}[H]
  \centering
  \caption{Overshoots over 100 steps, DEM Scale of 10 m, and ROS of 0.5}
  \label{table:Overshoot Sim Speed}
  \begin{tabular}{| c | c | c | c |}
    \hline
    \textbf{$\Delta t$} & \textbf{Total \#} & \textbf{Average \#} & \textbf{Average Amt.} \\
    \hline
    200 ms & 0 & 0 & 0 \\
    1000 ms & 0 & 0 & 0 \\
    2000 ms & 244 & 2 & 0.19 \\
    5000 ms & 2012 & 20 & 0.43 \\
    \hline
  \end{tabular}
\end{table}


\begin{table}[H]
  \centering
  \caption{Overshoots over 100 steps, $\Delta t$ of 1000 ms, and ROS of 0.5}
  \label{table:Overshoot Scale}
  \begin{tabular}{| c | c | c | c |}
    \hline
    \textbf{DEM Scale} & \textbf{Total \#} & \textbf{Average \#} & \textbf{Average Amt.} \\
    \hline
    1 m & 6726 & 67 & 0.68 \\
    5 m & 244 & 2 & 0.19 \\
    10 m & 0 & 0 & 0 \\
    20 m & 0 & 0 & 0\\
    \hline
  \end{tabular}
\end{table}

\begin{table}[H]
  \centering
  \caption{Overshoots over 100 steps, DEM Scale of 10 m, and $\Delta t$ of 1000 ms}
  \label{table:Overshoot ROS}
  \begin{tabular}{| c | c | c | c |}
    \hline
    \textbf{ROS} & \textbf{Total \#} & \textbf{Average \#} & \textbf{Average Amt.} \\
    \hline
    0.5 $ m \cdot s^{-1} $ & 0 & 0 & 0 \\
    1.0 $ m \cdot s^{-1} $ & 0 & 0 & 0 \\
    5.0 $ m \cdot s^{-1} $ & 6726 & 67 & 0.28 \\
    10.0 $ m \cdot s^{-1} $ & 9801 & 98 & 1.17 \\
    \hline
  \end{tabular}
\end{table}

This means that the number of overshoots is not only dependant on $ \Delta t $ but also on the DEM scale ($ a $) and the cell's neighbour's rate of spread. Combining this observation with the variables that influence $ \Delta t $ The probability of a cell overshooting is some function of: the execution time of the previous step, the simulation speed, the DEM scale, That cell's state at the start of the time step, and the rate of spread of all of its burning neighbours. Due to many of the variables being specific to a given time step (and therefore are only known once that time step occurs), it is impossible to determine whether a following time step will have overshooting cells. Figure~\ref{fig:Overshoot} provides an illustration on how the fire front shape can change depending on the severity of the overshooting.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/no_overshoot.png}
    \caption{No Overshooting}
  \end{subfigure}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/minor_overshoot.png}
    \caption{Mild Overshooting}
  \end{subfigure}%
  \begin{subfigure}[t]{0.33\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/severe_overshoot.png}
    \caption{Severe overshooting}
  \end{subfigure}%
\caption{Changes in the fire front with depending on the severity of overshooting}
\label{fig:Overshoot}
\end{figure}

%\begin{table}[H]
%  \centering
%  \caption{Overshoots over X steps, DEM Scale of Y, and Sim Speed of Z}
%  \label{table:Overshoot ROS}
%  \begin{tabu}{|[2pt] c |[2pt] c |[2pt] c |[2pt] c |[2pt] c |[2pt]}
%    \tabucline[2pt]{-}
%    \textbf{Model} & \textbf{ROS} & \textbf{Total \#} & \textbf{Average \#} & \textbf{Average Amt.} \\
%    \tabucline[2pt]{-}
%    \multirow{4}{*}{\rotatebox[origin=c]{90}{\parbox{4em}{\centering Beaudoin Single Update}}} 
%    & 0.5 $ m \cdot s^{-1} $ &  &  &  \\\tabucline[.5pt]{2-}
%    & 1.0 $ m \cdot s^{-1} $ &  &  &  \\\tabucline[.5pt]{2-}
%    & 5.0 $ m \cdot s^{-1} $ &  &  &  \\\tabucline[.5pt]{2-}
%    & 10.0 $ m \cdot s^{-1} $ &  &  &  \\
%    \tabucline[2pt]{-}
%    \multirow{4}{*}{\rotatebox[origin=c]{90}{\parbox{4em}{\centering Beaudoin Double Update}}} 
%    & 0.5 $ m \cdot s^{-1} $ &  &  &  \\\tabucline[.5pt]{2-}
%    & 1.0 $ m \cdot s^{-1} $ &  &  &  \\\tabucline[.5pt]{2-}
%    & 5.0 $ m \cdot s^{-1} $ &  &  &  \\\tabucline[.5pt]{2-}
%    & 10.0 $ m \cdot s^{-1} $ &  &  &  \\
%    \tabucline[2pt]{-}
%  \end{tabu}
%\end{table}
\section{Performance Discussion}
\label{sec:Performance}
As was discussed in Chapter~\ref{chap:Introduction}, a real-time visual system should keep the update time of its frames (update steps) less than what is perceivable for the human eye. For an interactive system like AR however, the frame update should be even less than that. As was discussed in Chapter~\ref{chap:Design}, the timings were gathered using the bare-bones console version of the system. This provides a best case scenario for the algorithm without any integration specific performance loss.

Each version of the console program was run with equal settings. Data was gathered for 100 iterations, and for width/height values of: 100x100, 1000x1000, 2500x2500, 5000x5000, and 7500x7500. Table~\ref{table:Compare Timings} contains the average and cumulative execution times, as well as the standard deviation for the three types and five sizes, while having wind and slope effects enabled. Figure~\ref{fig:Avg Time Bad} provides a graph plotting the gathered average times, as well as presenting a predicted trend.

\begin{table}[H]
  \centering
  \caption{Average and Cumulative Timings for Each Processing Method}
  \label{table:Compare Timings}
  \begin{tabular}{| l | c | c | c | c | c |}
    \hline
    \textbf{Width x Height} & \textbf{100x100} & \textbf{1000x1000} & \textbf{2500x2500} & \textbf{5000x5000} & \textbf{7500x7500} \\
    \hline
    \textbf{Total Cells} & \textbf{10000} & \textbf{1000000} & \textbf{6250000} & \textbf{25000000} & \textbf{56250000}  \\
    \hline
    \multicolumn{6}{|c|}{\textbf{Average Time (ms)}} \\
    \hline
    CPU Sequential & 0.6 & 60.8 & 377.9 & 1510.7 & 3401.9 \\
    CPU Parallel & 0.2 & 22.2 & 124.2 & 463.6 & 1071.7 \\
    GPU Parallel & 0.5 & 4.9 & 19.2 & 74.1 & 161.3  \\
    \hline
    \multicolumn{6}{|c|}{\textbf{Cumulative Time (ms)}} \\
    \hline
    CPU Sequential & 62.6 & 6076.5 & 37787.4 & 151068.0 & 340188.0 \\
    CPU Parallel & 23.2 & 2221.7 & 12419.0 & 46360.5 & 107169.0 \\
    GPU Parallel & 49.8 & 389.7 & 1923.9 & 7411.4 & 16129.4 \\
    \hline
    \multicolumn{6}{|c|}{\textbf{Standard Deviation (ms)}} \\
    \hline
    CPU Sequential & 0.12 & 0.12 & 0.59 & 0.96 & 6.06 \\
    CPU Parallel & 0.17 & 3.42 & 3.54 & 2.19 & 39.95 \\
    GPU Parallel & 0.25 & 10.29 & 13.76 & 25.42 & 6.48 \\
    \hline
  \end{tabular}
\end{table}

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.75\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/avg_exec_bad.png}
    \caption{Linear Y-Axis Scaling}
  \end{subfigure}\\ %
  \begin{subfigure}[t]{0.75\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/avg_exec_bad_log.png}
    \caption{Logarithmic Y-Axis Scaling}
  \end{subfigure}%
\caption{Average Execution Times Based on Width/Height Values}
\label{fig:Avg Time Bad}
\end{figure}

From Figure~\ref{fig:Avg Time Bad} we can see that for the CPU Sequential, the trend is that the execution time will increase exponentially worse than both the parallel methods. The CPU parallel scales much better, while not completely linear it is very close and the GPU parallel scales extremely well. While these graphs shows how the average execution time changes based on changing in relation to the width and height values. Looking at Figure~\ref{fig:Avg Time Bad} or Table~\ref{table:Compare Timings}, for relatively low input values ($\text{width x height}\lessapprox 400\text{x}400~\text{or total cells}\lessapprox160000$), the parallel CPU version is likely to perform better for similarly configured PC's. 

There is an argument to be made that in reality, the changes in width and height properties causes an exponential increase in the total number of cells. if the x-axis is taken as being the total number of cells instead of the width and height values the result is Figure~\ref{fig:Avg Time}.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.75\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/avg_exec.png}
    \caption{Linear Y-Axis Scaling}
  \end{subfigure}\\%
  \begin{subfigure}[t]{0.75\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/avg_exec_log.png}
    \caption{Logarithmic Y-Axis Scaling}
  \end{subfigure}
\caption{Average Execution Times Based on Total Number of Cells}
\label{fig:Avg Time}
\end{figure}

At first glance it would be easy to assume that the sequential version scales $O(n^2)$, where $n$ is the width and height values, as for a single increase in value for $n$, both the inner and outer for loops increase by an extra iteration. However, this observation does not take into account that there are three code paths with unequal amounts of work (computationally and memory-wise). For the update simulation the bulk of the work only applies to unburned cells while burning cells have much less, and extinguished cells have none at all. Even with the unburned cells there is unequal work when calculating the rate of spread of a cell's neighbours. Burning neighbours perform more work than unburned or extinguished neighbours, as these will just return zero. In short, the majority of the work is performed when updating the cells adjacent/diagonal to the fire front, making the system scale much better. 

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{results/figures/stddev.png}
  \caption{Standard Deviations For Each Method}
  \label{fig:Stddev}
\end{figure}

Figure~\ref{fig:Stddev} is a graph representing the different standard deviations. It would seem that while the sequential CPU version is the worst performer, it is the most consistent with regards to both: increasing input amount, and within the 100 iterations. The parallel CPU was not much worse than the sequential CPU in all cases except the 7500x7500 test where it had a standard deviation of 40 ms. Figure~\ref{fig:Stddev Compare} provides a graph for each processing method representing the difference from the mean for each iteration.

\begin{figure}[H]
  \centering
   \begin{subfigure}[t]{0.8\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/dev_seq.png}
    \caption{CPU Sequential}
  \end{subfigure} \\
  \begin{subfigure}[t]{0.8\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/dev_par.png}
    \caption{CPU Parallel}
  \end{subfigure} \\
  \end{figure}
  \begin{figure}[H]\ContinuedFloat
  \centering
  \begin{subfigure}[t]{0.8\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/dev_gpu.png}
    \caption{GPU Parallel}
  \end{subfigure}
  \caption{Deviation from the Mean for Each Iteration}
  \label{fig:Stddev Compare}
\end{figure}

For the CPU sequential and CPU parallel methods there is a clear outlier in that the 7500x7500 run is drastically less consistent. For the GPU, the first iteration performs exponentially worse than any other iteration, making it the outlier for the GPU method. Figure~\ref{fig:Stddev Compare2} provides the same data, reformatted to ensure that the outliers identified do not obscure data. For the CPU sequential and CPU parallel graphs (Figures~\ref{fig:Stddev Seq}~and~\ref{fig:Stddev Par}), the 7500x7500 run is moved to a secondary axis (with its values on the right), and for the GPU graph (Figure~\ref{fig:Stddev GPU}) the first iteration is excluded as to allow visibility for the other iterations.

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.8\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/dev_seq2.png}
    \caption{CPU Sequential, with 7500x7500 on secondary axis }
    \label{fig:Stddev Seq}
  \end{subfigure} \\
  \begin{subfigure}[t]{0.8\textwidth}
    \centering
    \includegraphics[width=\linewidth]{results/figures/dev_par2.png}
    \caption{CPU Parallel, with 7500x7500 on secondary axis}
    \label{fig:Stddev Par}
  \end{subfigure} \\
\end{figure}
\begin{figure}[H]\ContinuedFloat
\centering
  \begin{subfigure}[t]{0.8\textwidth}\ContinuedFloat
    \centering
    \includegraphics[width=\linewidth]{results/figures/dev_gpu2.png}
    \caption{GPU Parallel, excluding the first iteration}
    \label{fig:Stddev GPU}
  \end{subfigure}
\caption{Deviation from the Mean for Each Iteration with Modified Graphs}
\label{fig:Stddev Compare2}
\end{figure}

For the CPU sequential and GPU methods, the large majority of non-outlier iterations remain within $\approx$1 ms and $\approx$3 ms, respectively. While the CPU parallel is much less consistent, with many spikes and with the majority being $\approx$4 ms and spiking up to $\approx$14 ms. Once the input size is pushed to 7500x7500, both CPU methods perform very inconsistently, while the GPU still performs with a majority of $\approx$1 ms. 

Even with some of the criticisms listed in this section, all three methods perform very well under realistic inputs. If the upper limit for an update is considered to be 100 ms, giving enough time for any other extra work to be performed (like generating an image to display). The sequential version falls within this constraint for an input size of less than approximately 1200x1200, the CPU parallel for approximately 2200x2200, and the GPU for approximately 6000x6000. 

In conclusion, all three methods perform adequately for real-time use, however, using either the parallel CPU or GPU methods will result in much better and more consistent timings. These timings only take into account the update method timing in the best possible scenario. Once the data structures, visual output, or other extra features (like contour lines) are added, the total update time for the system increases. Choosing a method that gives more headroom for these slowdowns will provide a better experience for real-time use.
\chapter{Implementation}
\label{chap:Implementation}
This chapter is split into four sections, each one expanding onto the previous. These sections correspond to various aspects for discussion: Sequential CPU, Parallel CPU, Parallel GPU, and GUI Proof of Concept versions. Concepts from the former section apply to the latter. Within each section, the model is tested in three parts. For the purposes of this implementation: the Rothermel ROS was converted to $m/sec$. Appendices~\ref{app:seq}~,~\ref{app:par}~,and~\ref{app:gpu} provide full code listings for the Sequential CPU, Parallel CPU, and Parallel GPU console programs respectively.

\section{Sequential CPU Version}
\label{sec:Sequential CPU}
This first version focuses on implementing the model in a simple sequential fashion. This will serve as the basis for the two parallel versions. 

\subsection{Spread Model Without Wind and Slope}
Although this implementation is built on the principles of 2D-CA, the data sets are represented as single dimensional arrays/vectors. This was done for both an increase in performance and to better represent the data structures of a GPU, making the GPU conversion less complicated. The translation of a two-dimensional coordinate $ (i,j) $ to a single dimensional array index is trivial given that the width of the lattice is known (denoted by $n$). This is described by Equation~\ref{eq:Translate 2D to Index}.

\begin{equation}
\label{eq:Translate 2D to Index}
  index = j \times n + i
\end{equation}

The first step in implementing the spread model was to identify portions of the model that do not need to be updated each time step. The base rate of spread of the fuel (calculated using Equation~\ref{eq:Rothermel ROS}) does not change through the process of the simulation, thus it can be calculated before the first time step is performed. For this reason the calculation of each cell's base rate of spread ($R_0$) does not have an impact on the timings of the update process and is omitted from the console version of the program entirely. 

Following this, data structures and variables to hold all of the information that is kept between update steps were built. Ignoring wind and slope means that the elevation data does not come into effect for this phase. The width and height (number of rows and columns in the lattice) is kept in simple integers. A float value containing $ \Delta t $ in seconds is passed into the method. Arrays of an appropriate size are created to hold the discrete states, float states ($ S_{i,j}^t $), start times for burning, and base rate of spread ($ R_0 $) for each cell in the lattice. The reason for having a separate array to store the discrete states is because, while it is possible to determin whether a cell is burning or not from $ S_{i,j}^t $, there is no value for $ S_{i,j}^t $ that indicates whether the cell is extinguished. It can be argued that it is possible to remove the need for the array of discrete states by assigning a value for $ S_{i,j}^t $ that is not possible under any circumstance to represent this state (for example -1). While this is true and it will lead to a more memory efficient system, it would also decrease code visibility when accessing this data from elsewhere in a larger system. Thus this is often considered bad practice and the original proposal is used. 

Since each update will require a copy of the float state values at the start of the update to be kept, it is more efficient to define an array or appropriate size for this outside the update function. This is to avoid the creation and destruction of a new array every update. For the purposes of keeping the parameter list short, the aforementioned variables are kept as globals. Listing~\ref{lst:Seq A Signature} provides an example code listing including the declaration of globals and the method signature for the first update method.

\begin{lstlisting}[caption=Globals and Method Signature for Sequential with no Wind or Slope, label=lst:Seq A Signature]
  const int UNBURNED = 0, BURNING = 1, EXTINGUISHED = 2;
  
  float elevations[WIDTH * HEIGHT];
  int discrete_states[WIDTH * HEIGHT];
  float states[WIDTH * HEIGHT];
  float new_states[WIDTH * HEIGHT];
  float rate_of_spreads[WIDTH * HEIGHT];
  float heat_transferred[WIDTH * HEIGHT];
  float ignition_heat[WIDTH * HEIGHT];
  unsigned long time_started[WIDTH * HEIGHT];
  unsigned long elapsed_sim_time;

  void Update(float delta_time_ms);
\end{lstlisting} 
\vspace{1em}

For this update method, it is necessary to loop through all the cell in the lattice and for each cell there are three paths that the code can take, one for each possible discrete state. If the cell is unburned then the rate of spread for that cell is calculated using Equation~\ref{eq:CA ROS} without the wind and slope coefficients (see Equation~\ref{eq:CA ROS No Wind or Slope}). Then using this rate of spread the new float state value for that cell can be calculated using Equation~\ref{eq:State Transition Berjak}. Furthermore, the discrete state for the cell must be calculated using Equation~\ref{eq:Berjak Discrete}. Should a cell transition from the unburned state to the burning state, then the current elapsed time will need to be stored in that cells corresponding index in the time\_started array.

\begin{equation}
\label{eq:CA ROS No Wind or Slope}
  R_{i,j}^{t} = R_0 \times \left ( \eta_f \right )_{k,l}^{t}
\end{equation}

If the cell is burning then a check needs to be performed on whether the elapsed time is greater than or equal to its expected time of extinction. This is calculated using the Burned path of Equation~\ref{eq:Berjak Discrete}. If the cell is extinguished, then no code needs to be performed and that cell can be skipped. Figure~\ref{fig:Code Paths} provides an illustration of the possible code paths.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{methodology/figures/update_state_diagram.png}
  \caption{Possible Code Paths for Update Method}
  \label{fig:Code Paths}
\end{figure}

Certain portions of the formula described in Equation~\ref{eq:State Transition Berjak} do not change on a cell-by-cell basis and can be calculated at the beginning of the update step. This is done to reduce the number of operations performed for each cell. The split equation can be seen in Equation~\ref{eq:Split Transition}, and the code listing for this update method can be seen in Listing~\ref{lst:Seq Update Full}.

\begin{equation}
  \label{eq:Split Transition}
  \begin{split}
    c_{adj}   & = \left ( \frac{1}{a} \right ) \Delta t \\ 
    c_{diag}  & = 0.17 \left ( \frac{1}{\sqrt{2}a} \right ) \Delta t \\ 
    S_{i,j}^{t+1} & = S_{i,j}^{t} + \left ( c_{adj} \right ) \left ( \sum_{\left ( \alpha, \beta \right ) \epsilon V_{M}^{adj}} R_{i + \alpha, j + \beta}^{t} \right ) + \left ( c_{diag} \right ) \left ( \sum_{\left ( \alpha, \beta \right ) \epsilon V_{M}^{diag}} R_{i + \alpha, j + \beta}^{t} \right )
  \end{split}
\end{equation}

Where $ c_{adj} $ and $ c_{diag} $ are calculated at the start of the update method, and $ S_{i,j}^{t+1} $ is calculated for each cell.

\begin{lstlisting}[caption=Sequential Update Method for No Wind or Slope, label=lst:Seq Update Full]
  float delta_time = delta_time_ms / 1000.0;
  elapsed_sim_time += delta_time;
  float adj_coeff = (1.0f / float(SCALE)) * delta_time;
  float diag_coeff = 0.17 * ((1.0 / (sqrt(2) * float(SCALE))) * delta_time);

  for (int j = 0; j < HEIGHT; j++) {
    for (int i = 0; i < WIDTH; i++) {
      // === BURNING CELLS ===
      if (discrete_states[Coord(i, j)] == BURNING) {
        float td = sqrt(2) * (float(SCALE) / rate_of_spreads[Coord(i, j)]);
        if (elapsed_sim_time >= (time_started[Coord(i, j)] + (td)))
          discrete_states[Coord(i, j)] = EXTINGUISHED;
      } 
      // === UNBURNED CELLS ===
      else if (discrete_states[Coord(i, j)] == UNBURNED) {
        float sum_adj = 0;
        sum_adj += RateOfSpreadAt(i, j, i - 1, j);
        sum_adj += RateOfSpreadAt(i, j, i, j - 1);
        sum_adj += RateOfSpreadAt(i, j, i + 1, j);
        sum_adj += RateOfSpreadAt(i, j, i, j + 1);

        float sum_diag = 0;
        sum_diag += RateOfSpreadAt(i, j, i - 1, j - 1);
        sum_diag += RateOfSpreadAt(i, j, i - 1, j + 1);
        sum_diag += RateOfSpreadAt(i, j, i + 1, j - 1);
        sum_diag += RateOfSpreadAt(i, j, i + 1, j + 1);

        new_states[Coord(i, j)] = states[Coord(i, j)] + adj_coeff * sum_adj + diag_coeff * sum_diag;
      }
    }
  }
  // === CHANGE DISCRETE STATES ===
  for (int i = 0; i < WIDTH * HEIGHT; i++) {
    if (discrete_states[i] == UNBURNED) {
      if (new_states[i] >= 1.0) {
        states[i] = 1.0;
        discrete_states[i] = BURNING;
        time_started[i] = elapsed_sim_time;
      } else
        states[i] = new_states[i];
    }
  }
\end{lstlisting}

The \textit{Coord()} method is a simple inline method that converts an $x-y$ coordinate to an array index. This is done using Equation~\ref{eq:Translate 2D to Index} and the code for this method can be seen in Listing~\ref{lst:Coord}. While the g++ compiler should automatically inline this method, the method is specified as inline just in case. An inlined method is one that gets substituted at compile time, rather than it being called at runtime. The Update method calls a \textit{RateOfSpreadAt()} method for each of its neighbours. This method is what calculates the rate of spread for the neighbouring cell using Equation~\ref{eq:CA ROS No Wind or Slope}. It is also checked if that neighbour's index is out of bounds. Listing~\ref{lst:ROS1} contains the code needed to implement the aforementioned equation.

\begin{lstlisting}[caption=Inline Conversion of $x-y$ coordinates to array index, label=lst:Coord]
  inline int Coord(int i, int j) { return (j * WIDTH) + i; }
\end{lstlisting}

\begin{lstlisting}[caption=Calculating a Neighbour's ROS for No Wind or Slope, label=lst:ROS1]
  float RateOfSpreadAt(int i, int j, int k, int l) {
    if (k < 0 || l < 0 || k >= WIDTH || l >= HEIGHT || discrete_states[Coord(k, l)] != BURNING || heat_transferred[Coord(k, l)] < ignition_heat[Coord(i, j)])
      return 0;
      float ros = rate_of_spreads[Coord(k, l - 1)];
    return ros;
\end{lstlisting}

In order to time the updates, the chrono class is used. Traditional method timing, through the use of clock cycles, cannot be used for this system as it will produce unusable timing data for both the CPU parallel and GPU methods. Tracking clock cycles to time methods is usually seen as advantageous due to it being less effected by delays from outside the system. From the parallel point of view, if the system had eight threads all executing two clock cycles simultaneously, the clock cycle timing would report back that sixteen clock cycles worth of time had passed, when that is not true. From the GPU point of view, since the CPU is executing nothing, the clock cycle timing would report back that no time had passed. In order to get acurate timings for these two cases the chrono class is used to track the elapsed real time. Listing~\ref{lst:Chrono} illustrates how the Update method was called and timed.

\begin{lstlisting}[caption=Timing a call to the Update() method using the chrono class, label=lst:Chrono]
  start = std::chrono::system_clock::now();
  Update((FIXED_DELTA_TIME == 1) ? CONST_TIME : (delta_time * SIM_SPEED));
  end = std::chrono::system_clock::now();
  elapsed_seconds = end - start;
  delta_time = elapsed_seconds.count() * 1000.0;
\end{lstlisting}

% ----------------------------------------------------------------------------
%                                 WIND AND SLOPE
% ----------------------------------------------------------------------------
\subsection{Including Wind and Slope}
Introducing wind and slope effects into the simulation involves adjusting the RateOfSpreadAt method. Starting with slope effects, the slope is determined using Equation~\ref{eq:Slope}. The slope is then calculated using Equation~\ref{eq:Slope Coeff}. The return statement is modified and Listing~\ref{lst:Slope} provides the relevant code necessary for including slope into the system.

\begin{lstlisting}[caption=Including slope into the RateOfSpreadAt method, label=lst:Slope]
  if (!SLOPE_ACTIVE && !WIND_ACTIVE) return ros;
  float modifier = 0;
  if (SLOPE_ACTIVE) {
    float slope = GetSlope(i, j, k, l);
    modifier += exp(SLOPE_ALPHA * slope);
  }
  return ros * modifier;
\end{lstlisting}

Including wind into the system is a more difficult. Firstly, it is necessary to get the relative angle between the rate of spread direction from neighbour to cell, and the wind direction. For the purposes of this system, the wind direction is limited to the eight major compass directions (North (N), North East (NE), East (E), South East (SE), South (S), South West (SW), West (W), and  North West (NW)). Listing~\ref{lst:Angle} contains the code necessary to get the relative angle, represented as degrees between 0 and 360.

\begin{lstlisting}[caption=Calculating the relative angle between the neighbour and wind directions, label=lst:Angle]
  inline int GetRelativeAngle(int wind_direction, int neighbour_direction) {
    float angle = (wind_direction - neighbour_direction) % 360;
    return (angle < 0) ? (angle + 360) : angle;
  }
\end{lstlisting}

If the relative angle is zero degrees, then the direction of spread is in the same direction as the wind. The base flame angle is calculated using Equation~\ref{eq:Flame Angle Height}. Should the wind direction be a multiple of ninety degrees (N, E, S, and W) then the flame angle is adjusted according to the following criteria: if the relative angle is zero degrees then the flame angle is left unchanged, if the relative angle is one hundred and eighty degrees then the flame angle is negated, and if the relative angle is anything else then the flame angle is set to zero degrees. Listing~\ref{lst:Wind} contains the remaining code needed to add wind to the system. Should the wind direction not be a multiple of 90, but is a multiple of 45 (NE, SE, SW, and NW) then the flame angle is broken down into its two components vector components. Following this, the flame angle is adjusted according to both of these flame angle components.

\begin{lstlisting}[caption=Adding wind into the RateOfSpreadAt method, label=lst:Wind]
  if (WIND_ACTIVE) {
    float flame_angle =
        atan(2.35 * pow(pow(WIND_SPEED, 2) / (9.8 * EST_FLAME_LENGTH), 0.57));
    flame_angle = flame_angle * (180.0 / M_PI);
    float relative_angle = GetRelativeAngle(WIND_DIR, neighbour_direction);
    if (WIND_DIR % 90 == 0)
      flame_angle = (relative_angle == 0)
                        ? flame_angle
                        : (relative_angle == 180) ? -flame_angle : 0;
     else if (WIND_DIR % 45 == 0) {
       float speed_component = cos(45.0 * M_PI / 180.0) * WIND_SPEED;
       float flame_angle_component = atan(
           2.35 *
           pow(pow(flame_angle_component, 2) / (9.8 * EST_FLAME_LENGTH, 0.57));
       flame_angle_component = flame_angle_component * (180.0 / M_PI);
       flame_angle =
           (relative_angle == 0)
               ? flame_angle
               : (relative_angle == 180)
                     ? -flame_angle
                     : (relative_angle == 45 || relative_angle == 315)
                           ? flame_angle_component
                           : (relative_angle == 135 || relative_angle== 225)
                                 ? -flame_angle_component
                                 : 0;
                        }
    modifier += exp(WIND_BETA * flame_angle);
  }
\end{lstlisting}

% ----------------------------------------------------------------------------
%                                 PARALLEL CPU
% ----------------------------------------------------------------------------
\section{Parallel CPU Version}
\label{sec:Parallel CPU Version}
For the CPU parallel section it was a simple change. OpenMP uses pragmas to the code is multi-threaded. OpenMP is also included in the g++ compiler thus, in order to get access to it, \textit{-fopenmp} must be added to the makefile. Multiple options were tried when it comes to the number of threads in use however they all performed equal or worse than the number of threads that OpenMP decides on if that option is left unset. Listing~\ref{lst:Pragma} contains the necessary pragma to enable multi-threading for the update method.

\begin{lstlisting}[caption=OpenMP pragma to enable multi-threading for a for loop, label=lst:Pragma]
  #pragma omp parallel for
  for (int j = 0; j < HEIGHT; j++) {
    ...
\end{lstlisting}

% ----------------------------------------------------------------------------
%                                     GPU
% ----------------------------------------------------------------------------
\section{GPU Version}
\label{sec:GPU Version}
Since the code path taken for a burning cell includes very little work and involves modifying the discrete states, it is not worth porting this code to the GPU at all. If the GPU does the calculations for the burning cells, the discrete states array would need to be memory copied after every update. Ensuring that the GPU only performs the calculations for unburned cells means that the only array that the GPU kernel will modify is the \textit{d\_new\_states}. Along with this the only arrays that the host (CPU) modifies during the update process are the \textit{discrete\_states} and \textit{states} arrays. All of the other arrays stay constant. Taking this into account the remaining arrays are loaded into the kernel before the first update call.

It is necessary to declare new arrays for the GPU to use. These arrays are then allocated space before the simulation starts, and all of the arrays that are constant throughout the simulation are copied across. Once all of the iterations are complete (the simulation has ended), then the GPU arrays are destroyed and the GPU device is reset. Listing~\ref{lst:Before Update} provides the code for all of the pre-simulation work for the GPU. Listing~\ref{lst:GPU Sig} provides the method signature for the kernel as well as any supporting GPU methods.

\begin{lstlisting}[caption=Pre-simulation GPU code, label=lst:Before Update]
  float *d_elevations, *d_states, *d_new_states, *d_rate_of_spreads,
  *d_heat_transferred, *d_ignition_heat;
  int *d_discrete_states;
  ...

  int main(int argc, char *argv[]) {
    ...
    cudaMalloc((float **)&d_elevations, WIDTH * HEIGHT * sizeof(float));
    cudaMalloc((int **)&d_discrete_states, WIDTH * HEIGHT * sizeof(int));
    cudaMalloc((float **)&d_states, WIDTH * HEIGHT * sizeof(float));
    cudaMalloc((float **)&d_new_states, WIDTH * HEIGHT * sizeof(float));
    cudaMalloc((float **)&d_rate_of_spreads, WIDTH * HEIGHT * sizeof(float));
    cudaMalloc((float **)&d_heat_transferred, WIDTH * HEIGHT * sizeof(float));
    cudaMalloc((float **)&d_ignition_heat, WIDTH * HEIGHT * sizeof(float));
  
    cudaMemcpy(d_elevations, elevations, WIDTH * HEIGHT * sizeof(float),
               cudaMemcpyHostToDevice);
    cudaMemcpy(d_new_states, new_states, WIDTH * HEIGHT * sizeof(float),
               cudaMemcpyHostToDevice);
    cudaMemcpy(d_rate_of_spreads, rate_of_spreads, WIDTH * HEIGHT * sizeof(float),
               cudaMemcpyHostToDevice);
    cudaMemcpy(d_heat_transferred, heat_transferred,
               WIDTH * HEIGHT * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ignition_heat, ignition_heat, WIDTH * HEIGHT * sizeof(float),
               cudaMemcpyHostToDevice);

    for (int i = 0; i < ITERATIONS; i++) {
      // Update Calls
      ...
    }

    cudaFree(d_elevations);
    cudaFree(d_discrete_states);
    cudaFree(d_states);
    cudaFree(d_new_states);
    cudaFree(d_rate_of_spreads);
    cudaFree(d_heat_transferred);
    cudaFree(d_ignition_heat);
    cudaDeviceReset();
\end{lstlisting}

\begin{lstlisting}[caption=Kernel and supporting GPU method signatures, label=lst:GPU Sig]
  __device__ inline int DCoord(int i, int j) { return (j * WIDTH) + i; }
  __device__ float DRateOfSpreadAt(
      int i, int j, int k, int l, int neighbour_direction, float *d_elevations,
      int *d_discrete_states, float *d_rate_of_spreads, float *d_heat_transferred,
      float *d_ignition_heat, int d_wind_dir, float d_wind_speed);
  __global__ void DUpdate(float *d_elevations, int *d_discrete_states,
      float *d_states, float *d_new_states,
      float *d_rate_of_spreads, float *d_heat_transferred,
      float *d_ignition_heat, int d_wind_dir,
      float d_wind_speed, float d_adj_coeff,
      float d_diag_coeff);
\end{lstlisting}

The Kernel was set up as a two-dimensional grid of two-dimensional blocks, and each kernel thread would handle sixteen elements in a strided pattern. The reason for a strided access pattern was to avoid wasting parts of the read buffer. Figure~\ref{fig:Strided vs Contiguous Data}. When a read is performed, the read cache will read a section of memory, not a single element. Having the threads do strided iterations will mean that when each of the threads perform their first iteration, the data needed for each thread is contiguous. 

Strided versus contiguous data access also applies to how the data is structured in the first place. There are two major possible approaches: one would be to have a cell data struct/class that has all of the data relevant for a single cell in the lattice, the other is to have multiple arrays with each array handling a particular property for the entire lattice. These comparative approaches are often referred to as an array of structures versus a structure of arrays. While the first option may increase visibility and suitably abstracts the problem, it will cause wasted GPU resources as the data that needs to be accessed at a particular time is no longer contiguous. The effects of these two methods on the GPU is most easily explained through an example: if one wanted to represent a list of $x-y$ coordinates in a lattice. One could either have a coordinate struct that contains $x$ and $y$ properties, and have an array of coordinates. The other approach is to have two arrays that each store the relevant properties for the entire grid. Figure~\ref{fig:Strided Data} provides an illustration of this example. It can also be noted that for the second approach one could append the second array to the first, resulted one large array, for a small performance increase. 

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \includegraphics[width=\linewidth]{methodology/figures/bad1.png}
    \caption{Contiguous}
  \end{subfigure}%
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \includegraphics[width=\linewidth]{methodology/figures/good1.png}
    \caption{Strided}
  \end{subfigure}%
\caption{Strided vs. Contiguous elements per thread}
\label{fig:Strided vs Contiguous Data}
\end{figure}

\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \includegraphics[width=\linewidth]{methodology/figures/bad2.png}
    \caption{Contiguous}
  \end{subfigure}%
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \includegraphics[width=\linewidth]{methodology/figures/good2.png}
    \caption{Strided}
  \end{subfigure}%
\caption{Strided vs. Contiguous data structures}
\label{fig:Strided Data}
\end{figure}

Lastly, all of the array accesses to data structures, that are constant within the update process, are accessed through the read-only cache using \textit{\_\_ldg(\&array)}. This does however come with the disadvantage of only working on NVIDIA GPU's with a compute capability of 3.5 or higher. If compatibility is a concern, this can be removed to let the nvcc compiler choose which cache it uses to read the data.

\section{GUI Proof of Concept}
\label{sec:GUI PoC}
The GUI system serves as a proof of concept integration of the 2DCA fire simulation model into a real-time system. This will serve as the data-gathering tool for discussions on the model.

The GUI system can take the elevation data for the DEM in three ways: by creating a blank map with no elevation which gives a completely flat terrain, by generating elevation information based on a height-map image and an entered minimum and maximum elevations, or by loading a DEM file which is a CSV file. The CSV file must be have its first two values representing the width and height of the DEM and newlines are treated like a delimiter.

The system can be in various states: initial, ready, running, paused, and stopped. The GUI controls are grouped into control sections, the various controls and overall layout of the system can be seen in the user manual (Appendix~\ref{app:Guide}). The GUI will enable and disable various control sections based on the system state, in order to allow or prevent certain actions that are considered inappropriate for the given state.

The simulation can be played, paused, stopped, or reset at any time, as well as export the simulation image. There are also various view options, giving the user control over how the simulation is presented. This can be useful for data gathering as well as formatting the data to best fit the medium it is being exported to. The system will also track the fire front at given intervals which provides a better overview of the front evolution when using a still, exported image.

The simulation can be run using either the real-time configuration, where the delta time used is based off of the time since the last update and the simulation speed; or the fixed delta time method where the delta time used is uniform and pre-defined. The latter provides a more controlled environment for the evaluation of the model over multiple runs, where the real-time method provides better insight into how the system would perform in a real-time simulation.

The system can also take the fuel data in two ways: by entering all the information required by Rothermel's fuel model, or by entering the base rate of spread, pre-ignition heat, and heat content manually. The data needed in order to satisfy the Rothermel fuel model can be seen in Table~\ref{table:Fuel Models}.

This system has its GUI and fire simulation running on different threads. This means that long updates of the simulation will not cause the GUI to hang up or become unresponsive. This was done by utilising the \textit{QThread} and \textit{QObject} classes from the Qt library. There is a widget class called \textit{SimWidget} that handles the simulation system and the \textit{QThread} is used to perform the update method and image generation. The widget and thread communicate through the use of signals and slots. This is an event queuing system provided by the Qt library to allow for safe communication between threads and classes. When the thread completes an update step it will emit an update complete signal that contains the newly created image and update timings. The widget will then perform its slot method (for updating the widget pixmap) when it receives the signal from the thread.

\textbf{Class Description:} This system was built with five classes, which can be seen in Figure~\ref{fig:GUI System Overview}. The \textit{Mainwindow} class contains the QML code for the GUI objects. \textit{SimWidget} is the worker class inherited from \textit{QLabel}. This class manages the \textit{SimThread} class as a \textit{QThread}. The \textit{SimThread} class is inherited from \textit{QThread} and is the thread running the simulation. The \textit{DEM} class contains the elevation data for the terrain, as well as any other properties needed to describe the terrain data. This class also contains the list of fuels for each cell. Lastly, the \textit{Fuel} class contains the fuel model information needed to calculate the Rothermel ROS.
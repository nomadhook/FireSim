\chapter{Design}
\label{chap:Design}
This chapter is split into 4 sections. Section~\ref{sec:Design Model} discusses the different models highlighted in Section~\ref{sec:Lit Fire Sim} from the literature review. Section~\ref{sec:Design Program} provides a broad overview on how the programs/systems are structured and why. Section~\ref{sec:Design Methods} provides information on how data was generated and gathered by the systems. Lately, Section~\ref{sec:Design Rejected} outlines two rejected optimisation suggestions and why they were rejected.

\section{Model Discussion}
\label{sec:Design Model}
The model chosen was the CA model (see Section~\ref{sec: Lit CA}), and more specifically using the implementation proposed by \citet{Berjak}. The non-spacial fire spread model is the Rothermel fire spread model (see Section~\ref{sec:Lit Rothermel}), as this provides a method for the easy inserting of new fuel models based on gathered information, the Rothermel model also works independent of the geographic location. The equation used to describe a fuel's base  rate of spread (in the absence of wind and slope) is described by Equation~\ref{eq:Rothermel ROS}. 

The reason that the CA model was chosen was because CA systems lend themselves well to parallel programs. This is due to a CA model having its data represented by a discrete lattice and each cell's update function being calculated based on the previous state, allowing these updates to happen simultaneously as no cell's update function relies on another cell's updated value. Furthermore, this method is relatively simple to understand, teach, and implement especially in comparison to its vector-based counterparts. Since one of the goals of a real-time system as opposed to a baked simulation is to provide a easier teaching platform, the simplicity of CA methods is a boon.

It is possible to build a mesh from a DEM and thus the mesh based model proposed in Section~\ref{sec:Lit Mesh Method} could be employed. However, if a vector-based method was to be employed, the Level-Set method proposed in Section~\ref{sec:Lit Level Set} combined with the Narrow Band optimisation seems like a more promising solution. 

Appendix~\ref{app:Summary} provides a summary of all of the equations needed to implement the model.

\section{System Discussions}
\label{sec:Design Program}
For the purpose of evaluating the performance of the CA method, three versions of the update system were created: a sequential CPU version, a parallel CPU version, and a parallel GPU version. This covers situations where a device may only have a CPU, and where a device may have a GPU to utilise.

The programming language used was C++ as this gives better control over the performance of the system by having lower level control while still having the convenience of an object-oriented paradigm, it was also chosen as it is the language used for the ARSandbox allowing for easy future integration. The library used to implement the parallel CPU version was OpenMP as this library provides an easy-to-use method for parallelising code through the use of pre-processor pragmas providing considerable power as well as easy-to-read code. The library used for the GPU version was CUDAC as this is the base implementation of NVIDIA's CUDA framework, providing first party support for the running of C or C++ code on supporting NVIDIA GPUs. 

The systems used in this thesis are split into two separate programs. The first system has a fully functional Graphical User Interface (GUI) that is used to demonstrate how the chosen model(s) work in real time, as well as providing various extra controls and functionality. The second is a bare-bones system containing a single source file and produces no output, the purpose of this system is to provide a more controlled environment for the more accurate timing of the fire spread model(s). This is due to the GUI system having the simulation running on a separate thread to avoid the locking up of the GUI. Another reason for this split was to remove extra implementation specific factors from polluting the timings (for example: the data structures). A general outline for the GUI system can be seen in Figure~\ref{fig:GUI System Overview}. In summation, the first (console) version provides the timing data in the best case scenario by being in a more controlled environment, while the second (GUI) version provides an example proof of concept system that implements the discussed method, as well as providing visual feedback and data for the model outputs.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{methodology/figures/gui_overview.png}
  \caption{An overview of the GUI based system}
  \label{fig:GUI System Overview}
\end{figure}

This is a broad overview of the sub-systems providing a brief description to their purpose as well as the interaction between the sub-systems. A more extensive look into the system is discussed in Section~\ref{sec:GUI PoC}. 

\section{Data Gathering and Representation}
\label{sec:Design Methods}

a middle-range computer was used to provide timing data for what is considered an average user. the specifications for this computer can be seen in Table~\ref{table:PC Specs}. Timings where obtained using the chrono class and were timed over the update method providing data specific to a single update step, allowing for the gathering of the update methods timings over the lifetime of the simulation.

\begin{table}[H]
  \centering
  \caption{PC Specifications}
  \label{table:PC Specs}
  \begin{tabular}{| r | c |}
    \hline
    \textbf{Motherboard} & Asus H170 PRO GAMING \\
    \textbf{Processor} & Intel(R) Core(TM) i5-6400 \\
    \textbf{CPU Base Clock} & 2.70 GHz \\
    \textbf{CPU Turbo Clock} & 3.30 GHz  \\
    \textbf{CPU Cores} & 4 \\
    \textbf{CPU Threads} & 4 \\
    \textbf{Graphics Card} & NVIDIA GeForce GTX 750Ti 2GB \\
    \textbf{RAM} & 8GB DDR4 @ 2133MHz \\
    \hline
  \end{tabular}
\end{table}

Timings were gathered for differing grid sizes to give insight into the scalability of the model. The gathered timings are compared and discussed in Section~\ref{sec:Performance}. An average timing value gives an easy-to-see graph on general performance differences, however every iteration time was stored and exported to a Comma Separated Values (CSV) file in order to evaluate the consistency between iterations.

The GUI System's generated data was displayed as seen in Figure~\ref{fig:GUI Output}. Various parts of the generated image can be toggled in order to highlight certain aspects of the data. It was decided that the elevation data for the generated image would be displayed in height-map form (the lighter the colour, the higher the elevation data) instead of iso-lines and iso-colours. Having the elevation data displayed in contour map form would make distinguishing elevation contours from fire front contours unnecessarily difficult, and iso-colours would draw attention away from the fire front.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.75\textwidth]{methodology/figures/output.png}
  \caption{Labeled output of the model using the GUI system}
  \label{fig:GUI Output}
\end{figure}

\section{Rejected Optimisation Suggestions}
\label{sec:Design Rejected}
Below are two optimisation possibilities that are described and discussed but were ultimately rejected.

\subsection{Region Of Interest Approach}
\label{sec:ROI}
Due to the strict neighbourhood for the cell transition function it is possible to limit the number of cells to be processed through creating a Region of Interest (ROI). Only the unburned cells that have at least one burning neighbour are needed for the update. This means that for the start of the simulation, when there is a single cell in the burning state, the region of interest would be the eight surrounding cells. This already reduces the number of transition calculations for the first time step from all of the cells in the raster to only eight. After the update function, for every cell that transitioned from an unburned state to a burning state, that cell would be removed from the ROI, and its unburned neighbours would be added to the ROI. Figure~\ref{fig:ROI} provides two examples of a calculated ROI. 

\begin{figure}[H]
  \centering
  \includegraphics[width=0.7\textwidth]{methodology/figures/roi.png}
  \caption{Example ROI based on burning cells}
  \label{fig:ROI}
\end{figure}

A C++ vector would used to contain the indices of all of the cells within the ROI. While this may improve overall performance, it comes with the trade-off of adding more memory requirements on the system. This is because, while the calculations are only performed on the ROI, the update method still needs access to the states of all of the cells.

This also provides an interesting problem with the GPU version of the code. CUDA does not have access to the STL libraries (which contain the vector class) and as such, the ROI adjustment code either has to be modified to match the data structures available to CUDA kernels, or the adjustment must be performed on CPU side, after the update method is completed.

\textbf{Why it was rejected:}
This optimisation suggestion was rejected because, as is discussed in Section~\ref{sec:Performance}, this model is memory, not computationally bottlenecked. While this proposed method may increase performance, especially for the GPU where branch divergence is an issue, it would introduce more memory requirements and as such was rejected. 

\subsection{Padding}
Another GPU optimisation possibility for reducing branch divergence would be to pad the data so that out of range checks do not need to be performed. Figure~\ref{fig:Padding} shows how a two-dimensional data structure would be padded. The for loops within the update would have to be adjusted to not start at the start of the data structure, but rather the start of the relevant data. This addition would not introduce extra computation to be performed, however, care would need to be taken to adapt each method that uses the affected data structures as to not include the padded data. 

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{methodology/figures/padding.png}
  \caption{Illustration on how two-dimensional data would be padded}
  \label{fig:Padding}
\end{figure}

\textbf{Why it was rejected:}
This suggestion was rejected for the same reason as Section~\ref{sec:ROI}. This method would introduce a lot of extra data as for each cell of padding, empty data would be produced for all of the relevant arrays.
#ifndef SIM_WIDGET_H
#define SIM_WIDGET_H

#include "dem.h"
//#include <QThread>
#include "sim_thread.h"
#include <QtWidgets>

enum SystemState { INIT, READY, RUNNING, PAUSED, STOPPED };

class SimWidget : public QLabel {
  Q_OBJECT
public:
  Dem *dem;
  SystemState sys_state;
  float last_model_time;
  float last_full_time;
  bool fixed_time = true;

private:
  QPixmap pixmap;
  SimThread *sim;

public:
  SimWidget();
  ~SimWidget();

public:
  void DemLoaded();
  void PlayPauseSimualation();
  void StopSimulation();
  void SetShowTerrain(bool show);
  void SetShowElevation(bool show);
  void SetShowContours(bool show);
  void SetShowExtinguished(bool show);
  QPixmap GetImage();
  void SetWindEnabled(bool enabled);
  void SetSlopeEnabled(bool enabled);
  void SetConstnat(bool enabled);
  void SetConstantTime(int time);
  void SetSimSpeed(int speed);
  void ContourChanged(int time);
  void WindDirChanged(int dir);
  void WindSpeedChanged(float speed);

private:
  void resizeEvent(QResizeEvent *event) override;
  void StartNewSimulation();

private slots:
  void UpdatePixmap(QImage img, float model_time, float full_time);
};

#endif // SIM_WIDGET_H

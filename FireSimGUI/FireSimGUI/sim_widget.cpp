#include "sim_widget.h"

SimWidget::SimWidget() {
  sys_state = INIT;
  dem = new Dem();
  pixmap = QPixmap();
  this->setMinimumWidth(300);
  this->setAlignment(Qt::AlignCenter);
  // this->setStyleSheet("border: 1px solid;");
  sim = new SimThread(dem);
  this->setPixmap(pixmap);
  connect(sim, SIGNAL(UpdateComplete(QImage, float, float)), this,
          SLOT(UpdatePixmap(QImage, float, float)));
}

SimWidget::~SimWidget() {
  delete sim;
  // delete dem;
}

// === Public Methods ===
void SimWidget::DemLoaded() {
  sim->start();
  sim->ResizeImage(this->width(), this->height());
  update();
}

void SimWidget::PlayPauseSimualation() {
  if (sys_state == READY || sys_state == STOPPED)
    StartNewSimulation();
  else if (sys_state == RUNNING) {
    sim->PlayPauseSimulation(false);
    sys_state = PAUSED;
  } else if (sys_state == PAUSED) {
    sim->PlayPauseSimulation(true);
    sys_state = RUNNING;
  }
}

void SimWidget::StopSimulation() {
  if (sys_state == RUNNING || sys_state == PAUSED) {
    sys_state = STOPPED;
    sim->PlayPauseSimulation(false);
  } else if (sys_state == STOPPED) {
    sim->Reset();
    sys_state = READY;
  }
}

QPixmap SimWidget::GetImage() { return pixmap; }
void SimWidget::SetShowTerrain(bool show) { sim->show_terrain = show; }

void SimWidget::SetShowElevation(bool show) { sim->show_elevation = show; }

void SimWidget::SetShowContours(bool show) { sim->show_contours = show; }

void SimWidget::SetShowExtinguished(bool show) { sim->show_extinguished = show; }

void SimWidget::SetSlopeEnabled(bool enabled) { sim->slope_enabled = enabled; }

void SimWidget::SetWindEnabled(bool enabled) { sim->wind_enabled = enabled; }

void SimWidget::SetConstnat(bool enabled) {
  fixed_time = enabled;
  sim->constant = enabled;
}

void SimWidget::SetConstantTime(int time) { sim->constant_time = time; }

void SimWidget::SetSimSpeed(int speed) { sim->sim_speed = speed; }

void SimWidget::ContourChanged(int time) { sim->contour_interval = time; }

void SimWidget::WindDirChanged(int dir) { sim->wind_direction = dir; }

void SimWidget::WindSpeedChanged(float speed) { sim->wind_speed = speed; }

// === Private Methods ===
void SimWidget::resizeEvent(QResizeEvent *event) {
  QLabel::resizeEvent(event);
  if (sys_state != INIT) {
    pixmap = pixmap.scaled(this->width(), this->width(), Qt::KeepAspectRatio);
    update();
    this->setPixmap(pixmap);
    sim->ResizeImage(this->width(), this->width());
  }
  update();
}

void SimWidget::StartNewSimulation() {
  pixmap = QPixmap();
  sim->Reset();
  sys_state = RUNNING;
  sim->PlayPauseSimulation(true);
}

// === Private Slots ===
void SimWidget::UpdatePixmap(QImage new_img, float model_time, float full_time) {
  last_model_time = model_time;
  last_full_time = full_time;
  pixmap = QPixmap::fromImage(new_img);
  this->setPixmap(pixmap);
  update();
}

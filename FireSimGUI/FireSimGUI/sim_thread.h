#ifndef SIM_THREAD_H
#define SIM_THREAD_H

#include "dem.h"
#include "math.h"
#include <QImage>
#include <QMutex>
#include <QObject>
#include <QThread>
#include <QWaitCondition>
#include <chrono>

#define PARALLEL_ENABLED 1

// clang-format off
// === CONSTANTS ===
static const float SLOPE_ALPHA = 0.0693;
static const float WIND_BETA = 0.0501;
static const float EST_FLAME_LENGTH = 2.8;
// clang-format on

static const int NORTH = 0, NORTH_EAST = 45, EAST = 90, SOUTH_EAST = 135, SOUTH = 180,
                 SOUTH_WEST = 225, WEST = 270, NORTH_WEST = 315;

class SimThread : public QThread {
  Q_OBJECT
private:
  bool running;
  bool abort;
  bool dem_loaded;
  QMutex mutex;
  QWaitCondition condition;
  Dem *dem;
  std::vector<float> states;
  std::vector<float> new_states;
  std::vector<unsigned long> time_started;
  unsigned long elapsed_sim_time;
  QImage sim_img;
  unsigned long last_contour_update;
  unsigned long model_time_ms = 100;
  unsigned long full_time_ms = 100;
  int widgetWidth = 0;
  int widgetHeight = 0;

public:
  bool slope_enabled = true;
  bool wind_enabled = true;
  bool show_terrain = true;
  bool show_elevation = true;
  bool show_contours = true;
  bool show_extinguished = true;
  bool constant = true;
  int constant_time = 120;
  int sim_speed = 1;
  float contour_interval = 120.0;
  float wind_speed = 0.5;
  int wind_direction = 0;

public:
  SimThread(Dem *_dem);
  ~SimThread();

public:
  void Reset();
  void PlayPauseSimulation(bool _running);
  void ResizeImage(int new_width, int new_height);

private:
  void run() override;
  void UpdateStates(float delta_time_ms);
  float RateOfSpreadAt(int i, int j, int k, int l, int neighbour_direction);
  bool HasUnburnedNeighbours(int i, int j);
  inline float GetSlope(int i, int j, int k, int l, int neighbour_direction);
  inline int GetRelativeAngle(int wind_direction, int neighbour_direction);
  QRgb GetRgbFromHsv(int h, float s, float v);
  QImage RenderImage();

signals:
  void UpdateComplete(QImage img, float model_time, float full_time);
};

#endif // SIM_THREAD_H

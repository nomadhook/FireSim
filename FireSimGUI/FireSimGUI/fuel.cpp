#include "fuel.h"
#include <math.h>

// === Constructor ===
Fuel::Fuel(float _load, float _area_vol, float _oven_load, float _depth, int _size) {
  load = _load;
  size = _size;
  area_to_vol_ratio = _area_vol;
  ovendry_load = _oven_load;
  depth = _depth;
  UpdateROS();
}

Fuel::Fuel(float ros) { rate_of_spread = ros; }

// === Member Access Variables ===
float Fuel::FuelLoad() { return load; }
float Fuel::SurfaceAreaToVolRatio() { return area_to_vol_ratio; }
float Fuel::OvendryLoad() { return ovendry_load; }
float Fuel::Depth() { return depth; }
float Fuel::HeatOfPreignition() { return preignition_heat; }
float Fuel::RateOfSpread() { return rate_of_spread; }
float Fuel::PackingRatio() { return packing_ratio; }
float Fuel::OptimumPackingRatio() { return optimum_packing_ratio; }
float Fuel::C() { return c; }
float Fuel::B() { return b; }
float Fuel::E() { return e; }

// === Public Methods ===
void Fuel::UpdateROS() {
  float particle_moisture_content = FuelMoisture();
  float extinct_moisture_content = CalcMx();

  // GammaMax = sigma^1.5(495 + 0.0594 * sigma^1.5)^-1
  float maximum_reaction_velocity =
      pow(area_to_vol_ratio, 1.5) * pow(495 + (0.0594 * pow(area_to_vol_ratio, 1.5)), -1);

  // Pb = W0 / delta
  float ovendry_bulk_density = ovendry_load / depth;

  // beta = Pb / Pp
  packing_ratio = ovendry_bulk_density / PARTICLE_DENSITY;

  // beta_op = 3.348 * sigma ^ -0.08189
  optimum_packing_ratio = 3.348 * pow(area_to_vol_ratio, -.08189);

  // A = 1/(4.774 * sigma ^ 0.1 - 7.27)
  float A = 1 / (4.774 * pow(area_to_vol_ratio, .1) - 7.27);

  // Gamma' = GammaMax(beta/beta_op)^A * exp[A(1 - beta/beta_op)]
  float optimum_reaction_velocity = maximum_reaction_velocity *
                                    pow(packing_ratio / optimum_packing_ratio, A) *
                                    exp(A * (1 - packing_ratio / optimum_packing_ratio));

  // Wn = Wo / (1 + St)
  float net_fuel_loading = ovendry_load / (1 + MINERAL_CONTENT);

  // Nm = 1 - 2.59 * (Mf / Mx) + 5.11 * (Mf / Mx)^2 - 3.52 * (Mf / Mx)^3
  float moisture_dampening_coeff =
      1 - 2.59 * (particle_moisture_content / extinct_moisture_content) +
      5.11 * pow(particle_moisture_content / extinct_moisture_content, 2) -
      3.52 * pow(particle_moisture_content / extinct_moisture_content, 3);

  // Ns = 0.174 * Se^-0.19
  float mineral_dampening_coeff = 0.174 * pow(EFFECTIVE_MINERAL, -0.19);

  // Ir = Gamma' * Wn * h * Nm * Ns
  float reaction_intensity = optimum_reaction_velocity * net_fuel_loading * LOW_HEAT_CONTENT *
                             moisture_dampening_coeff * mineral_dampening_coeff;

  // xi = (192 + 0.2595sigma)^-1 exp[(0.792 + 0.681sigma^0.5) * (beta + 0.1)]
  float propagating_flux_ratio =
      pow(192 + 0.2595 * area_to_vol_ratio, -1) *
      exp((0.792 + 0.681 * pow(area_to_vol_ratio, 0.5)) * (packing_ratio + 0.1));

  // epsilon = exp(-138 / sigma)
  float effective_heating_num = exp(-138 / area_to_vol_ratio);

  // Qig = 250 + 1.116 * Mf
  preignition_heat = 250 + 1.116 * particle_moisture_content;

  // R0 = (Ir * xi) / (Pb * epsilon * Qig)
  rate_of_spread = (reaction_intensity * propagating_flux_ratio) /
                   (ovendry_bulk_density * effective_heating_num * preignition_heat);
  // UpdateWindVariables();

  // convert ft per min to m per sec
  rate_of_spread = rate_of_spread * 0.3048 * 60;
}

void Fuel::UpdateWindVariables() {
  c = 7.47 * exp(-0.133 * pow(area_to_vol_ratio, .55));
  b = 0.02526 * pow(area_to_vol_ratio, .54);
  e = 0.715 * exp(-3.59 * (pow(10, -4) * area_to_vol_ratio));
}

// === Private Methods ===
float Fuel::CalcMx() { return 0.3; }

float Fuel::FuelMoisture() { return 0.04; }

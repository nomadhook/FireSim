#ifndef FUEL_H
#define FUEL_H
#include <string>

const int SIZE_1H = 0, SIZE_10H = 1, SIZE_100H = 2, SIZE_LIVING = 3;

class Fuel {
  // === Constant Member Variables ===
public:
  // Se = 0.010, (Mx)dead = 0.3,
  // (Mx)living = equation 88

  // Fuel particle low heat content (h), B.t.u./lb. (default: 8000)
  const float LOW_HEAT_CONTENT = 8000.0;
  // Ovendry particle density (Pp), lb./ft.^3 (default: 32.0) // Come back
  const float PARTICLE_DENSITY = 32.0;
  // Fuel particle total mineral content (St), lb.(minerals)/lb.(ovendry wood)
  // (default: 0.0555)
  const float MINERAL_CONTENT = 0.0555;
  // Fuel particle effective mineral content (Se), lb.(Silica-free
  // minerals)/lb.(ovendry wood) (default: 0.010)
  const float EFFECTIVE_MINERAL = 0.010;

private:
  // === Member Variables ===
  // Total Fuel Loading, Tons/acre
  float load;
  // fuel particle surface-area-to-volume ratio (sigma), l/ft (ft.^-1).
  float area_to_vol_ratio;
  // Ovendry Fuel Loading (w0), lb./ft.^2 (lb./(ft. * ft.))
  float ovendry_load;
  // Fuel Depth (delta), ft.
  float depth;

  // --- Base ROS variables ---
  // Heat of preignition (Qig)
  float preignition_heat;
  // Rate of Spread (R0)
  float rate_of_spread;
  // Packing Ratio (beta)
  float packing_ratio;
  // Optimum Packing Ratio (beta op)
  float optimum_packing_ratio;
  // Size
  int size;

  // --- Wind Variables ---
  float c;
  float b;
  float e;

  // === Member Access Variables ===
public:
  float FuelLoad();
  float SurfaceAreaToVolRatio();
  float OvendryLoad();
  float Depth();
  float HeatOfPreignition();
  float RateOfSpread();
  float PackingRatio();
  float OptimumPackingRatio();
  float C();
  float B();
  float E();

  // === Constructor ===
public:
  Fuel(float _load, float _area_vol, float _oven_load, float _depth, int _size);
  Fuel(float ros);

  // === Public Methods ===
  void UpdateROS();
  void UpdateWindVariables();

  // === Private Methods ===
private:
  float CalcMx();
  float FuelMoisture();
};

#endif // FUEL_H

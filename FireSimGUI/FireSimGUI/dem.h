#ifndef DEM_H
#define DEM_H
#include "fuel.h"
#include <QString>
#include <fstream>
#include <vector>

class Dem {
  // === CONSTANTS/ENUMS ===
public:
  const int UNBURNED = 0, BURNING = 1, EXTINGUISHED = 2;

  // === MEMBER VARIABLES ===
private:
  int width;
  int height;
  int scale;
  float min_elevation = 0;
  float max_elevation = 0;
  std::vector<int> states;
  std::vector<float> elevation;
  std::vector<bool> contour_map;
  std::vector<Fuel *> fuels;

  // === CONSTRUCTOR ===
public:
  Dem();

  // === GET METHODS ===
public:
  int Width();
  int Height();
  int Scale();
  int State(int index);
  float MaxElevation();
  float MinElevation();
  float Elevation(int index);
  bool HasContourAt(int index);

  // === SET METHODS ===
  void SetState(int index, int state);
  void SetScale(int _scale);
  void SetContourAt(int index, bool has_contour);
  void ChangeFuels(Fuel *_fuel);

  // === PUBLIC METHODS ===
public:
  void CreateBlankDEM(int _width, int _height, int _scale = 10);
  void LoadFromHeightMap(QString filename, float min, float max, int _scale = 10);
  void LoadFromDem(QString filename);
  int Coord(int x, int y);
  float RateOfSpread(int index);
};
#endif // DEM_H

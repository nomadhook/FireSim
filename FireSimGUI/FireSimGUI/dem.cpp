#include "dem.h"
#include <QPixmap>

// === CONSTRUCTOR ===
Dem::Dem() {}

// === GET METHODS ===
int Dem::Width() { return width; }
int Dem::Height() { return height; }
int Dem::Scale() { return scale; }
int Dem::State(int index) { return states[index]; }
float Dem::MinElevation() { return min_elevation; }
float Dem::MaxElevation() { return max_elevation; }
float Dem::Elevation(int index) { return elevation[index]; }
bool Dem::HasContourAt(int index) { return contour_map[index]; }

// === SET METHODS ===
void Dem::SetState(int index, int state) { states[index] = state; }

void Dem::SetScale(int _scale) { scale = _scale; }

void Dem::SetContourAt(int index, bool has_contour) { contour_map[index] = has_contour; }

void Dem::ChangeFuels(Fuel *_fuel) {
  for (int i = 0; i < width * height; i++)
    fuels[i] = _fuel;
}

// === PUBLIC METHODS ===

void Dem::CreateBlankDEM(int _width, int _height, int _scale) {
  width = _width;
  height = _height;
  scale = _scale;
  min_elevation = 0;
  max_elevation = 0;
  states.clear();
  elevation.clear();
  contour_map.clear();
  fuels.clear();
  for (int i = 0; i < width * height; i++) {
    fuels.push_back(new Fuel(0.5));
    states.push_back(UNBURNED);
    elevation.push_back(0);
    contour_map.push_back(0);
  }
  states[Coord(width / 2, height / 2)] = BURNING;
}

void Dem::LoadFromHeightMap(QString filename, float min, float max, int _scale) {
  // QImage img = QPixmap(filename).toImage();
  QImage img = QImage(filename);
  width = img.width();
  height = img.height();
  scale = _scale;
  min_elevation = min;
  max_elevation = max;
  states.clear();
  fuels.clear();
  elevation.clear();
  contour_map.clear();
  for (int i = 0; i < width; i++) {
    for (int j = 0; j < height; j++) {
      fuels.push_back(new Fuel(0.5));
      states.push_back(UNBURNED);
      int val = qGray(img.pixel(i, j));
      float weighted_val =
          min_elevation + (float)((float)val / 255 * (max_elevation - min_elevation));
      elevation.push_back(weighted_val);
      contour_map.push_back(0);
    }
  }
  states[Coord(width / 2, height / 2)] = BURNING;
}

void Dem::LoadFromDem(QString filename) {
  std::ifstream file(filename.toStdString());
  std::string line;
  while (file >> line) {
    dem.push_back();
  }
}

inline int Dem::Coord(int x, int y) { return y * width + x; }

float Dem::RateOfSpread(int index) { return fuels[index]->RateOfSpread(); }

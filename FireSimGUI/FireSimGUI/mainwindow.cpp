#include "mainwindow.h"
#include "ui_mainwindow.h"

// === Constructor ===
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  // Center the Window
  QRect position = frameGeometry();
  position.moveCenter(QDesktopWidget().availableGeometry().center());
  move(position.topLeft());

  this->ui->horizontalLayout->insertWidget(4, sim_widget, 1);
  this->ui->horizontalLayout->setStretch(5, 0);
  UpdateUIPermissions();
}

// === Deconstructor ===
MainWindow::~MainWindow() {}

// === Private Methods ===
// Handles the enabling and disabling of UI elements based on the system state
void MainWindow::UpdateUIPermissions() {
  switch (sim_widget->sys_state) {
  case INIT:
    this->ui->grpFile->setEnabled(true);
    this->ui->grpInit->setEnabled(false);
    this->ui->grpMoisture->setEnabled(false);
    this->ui->grpDEMControls->setEnabled(false);
    this->ui->grpSpeed->setEnabled(false);
    this->ui->grpEffects->setEnabled(false);
    this->ui->grpView->setEnabled(false);

    break;
  case STOPPED:
  case READY:
    this->ui->grpFile->setEnabled(true);
    this->ui->grpInit->setEnabled(true);
    UpdateSpeedControls();
    this->ui->grpMoisture->setEnabled(true);
    this->ui->grpDEMControls->setEnabled(true);
    if (sim_widget->fixed_time)
      this->ui->grpSpeed->setEnabled(false);
    else
      this->ui->grpSpeed->setEnabled(true);
    this->ui->grpEffects->setEnabled(true);
    this->ui->grpView->setEnabled(true);
    break;
  case RUNNING:
    this->ui->grpFile->setEnabled(false);
    this->ui->grpInit->setEnabled(false);
    this->ui->grpMoisture->setEnabled(true);
    this->ui->grpDEMControls->setEnabled(true);
    if (sim_widget->fixed_time)
      this->ui->grpSpeed->setEnabled(false);
    else
      this->ui->grpSpeed->setEnabled(true);
    this->ui->grpEffects->setEnabled(true);
    this->ui->grpView->setEnabled(true);
    break;
  case PAUSED:
    this->ui->grpFile->setEnabled(false);
    this->ui->grpInit->setEnabled(false);
    this->ui->grpMoisture->setEnabled(true);
    this->ui->grpDEMControls->setEnabled(true);
    if (sim_widget->fixed_time)
      this->ui->grpSpeed->setEnabled(false);
    else
      this->ui->grpSpeed->setEnabled(true);
    this->ui->grpEffects->setEnabled(true);
    this->ui->grpView->setEnabled(true);
    break;
  /*case STOPPED:
    this->ui->grpFile->setEnabled(true);
    this->ui->grpInit->setEnabled(true);
    this->ui->grpMoisture->setEnabled(false);
    this->ui->grpDEMControls->setEnabled(true);
    this->ui->grpSpeed->setEnabled(false);
    this->ui->grpEffects->setEnabled(false);
    this->ui->grpView->setEnabled(true);
    break;*/
  default:
    break;
  }
  // UpdateStatistics();
}

void MainWindow::UpdateSpeedControls() {
  if (!this->ui->grpInit->isEnabled())
    return;
  if (sim_widget->fixed_time) {
    this->ui->grpSpeed->setEnabled(false);
    this->ui->lblTime->setEnabled(true);
    this->ui->lblTimeSI->setEnabled(true);
    this->ui->sliderTime->setEnabled(true);
    this->ui->txtTime->setEnabled(true);
  } else {
    this->ui->grpSpeed->setEnabled(true);
    this->ui->lblTime->setEnabled(false);
    this->ui->lblTimeSI->setEnabled(false);
    this->ui->sliderTime->setEnabled(false);
    this->ui->txtTime->setEnabled(false);
  }
}

// === Slots ===
void MainWindow::NewBlankDem() {
  // Create Form
  QDialog *d = new QDialog();
  QFormLayout *form = new QFormLayout();
  QLabel *lblWidth = new QLabel();
  lblWidth->setText("Width:");
  QSpinBox *txtWidth = new QSpinBox();
  txtWidth->setMinimum(10);
  txtWidth->setMaximum(10000);
  txtWidth->setValue(100);

  QLabel *lblHeight = new QLabel();
  lblHeight->setText("Height:");
  QSpinBox *txtHeight = new QSpinBox();
  txtHeight->setMinimum(10);
  txtHeight->setMaximum(10000);
  txtHeight->setValue(100);

  QDialogButtonBox *buttonBox =
      new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
  QObject::connect(buttonBox, SIGNAL(accepted()), d, SLOT(accept()));
  QObject::connect(buttonBox, SIGNAL(rejected()), d, SLOT(reject()));

  form->insertRow(-1, lblWidth, txtWidth);
  form->insertRow(-1, lblHeight, txtHeight);
  form->insertRow(-1, buttonBox);
  d->setLayout(form);
  int result = d->exec();
  if (result == QDialog::Accepted) {
    sim_widget->dem->CreateBlankDEM(txtWidth->value(), txtHeight->value());
    sim_widget->sys_state = READY;
    UpdateUIPermissions();
    sim_widget->DemLoaded();
  }
}

void MainWindow::LoadFromHeightmap() {
  QString file = QFileDialog::getOpenFileName(
      this, tr("Open PPM Image"), QDir::currentPath(),
      tr("Image Files (*.gif *.jpg *.jpeg *.png *.pbm *.ppm *.xbm *.xpm)"));
  if (file.isEmpty() || file.isNull())
    return;
  QDialog *d = new QDialog();
  QFormLayout *form = new QFormLayout();
  QLabel *lblMin = new QLabel();
  lblMin->setText("Minimum Elevation:");
  QSpinBox *txtMin = new QSpinBox();
  txtMin->setMinimum(-10000);
  txtMin->setMaximum(10000);
  txtMin->setValue(0);

  QLabel *lblMax = new QLabel();
  lblMax->setText("Maximum Elevation:");
  QSpinBox *txtMax = new QSpinBox();
  txtMax->setMinimum(-10000);
  txtMax->setMaximum(10000);
  txtMax->setValue(100);

  QDialogButtonBox *buttonBox =
      new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
  QObject::connect(buttonBox, SIGNAL(accepted()), d, SLOT(accept()));
  QObject::connect(buttonBox, SIGNAL(rejected()), d, SLOT(reject()));

  form->insertRow(-1, lblMin, txtMin);
  form->insertRow(-1, lblMax, txtMax);
  form->insertRow(-1, buttonBox);
  d->setLayout(form);
  int result = d->exec();

  if (result == QDialog::Accepted) {
    bool valid = txtMin->value() < txtMax->value();
    while (!valid) {
      int result = d->exec();
      if (result == QDialog::Accepted)
        valid = txtMin->value() < txtMax->value();
      else
        return;
    }
    sim_widget->dem->LoadFromHeightMap(file, txtMin->value(), txtMax->value());
    sim_widget->sys_state = READY;
    UpdateUIPermissions();
    sim_widget->DemLoaded();
  }
}

void MainWindow::LoadFromDem() {
  QString file = QFileDialog::getOpenFileName(
      this, tr("Open PPM Image"), QDir::currentPath(),
      tr("Image Files (*.gif *.jpg *.jpeg *.png *.pbm *.ppm *.xbm *.xpm)"));
  if (file.isEmpty() || file.isNull())
    return;
  sim_widget->dem->LoadFromDem(file);
  sim_widget->sys_state = READY;
  UpdateUIPermissions();
  sim_widget->DemLoaded();
}

void MainWindow::ScaleChanged(int value) {
  this->ui->sliderScale->setValue(value);
  this->ui->txtScale->setValue(value);
  sim_widget->dem->SetScale(value);
}

void MainWindow::IntervalChanged(int value) {
  this->ui->sliderInterval->setValue(value);
  this->ui->txtInterval->setValue(value);
  sim_widget->ContourChanged(value);
}

void MainWindow::TimeChecked(bool checked) {
  sim_widget->SetConstnat(checked);
  UpdateSpeedControls();
}

void MainWindow::TimeChanged(int value) {
  this->ui->sliderTime->setValue(value);
  this->ui->txtTime->setValue(value);
  sim_widget->SetConstantTime(value);
}

void MainWindow::PlayPauseSim() {
  sim_widget->PlayPauseSimualation();
  UpdateUIPermissions();
}

void MainWindow::StopResetSim() {
  sim_widget->StopSimulation();
  UpdateUIPermissions();
}

void MainWindow::ShowTerrainChecked(bool checked) { sim_widget->SetShowTerrain(checked); }

void MainWindow::ShowElevationChecked(bool checked) { sim_widget->SetShowElevation(checked); }

void MainWindow::ShowContoursChecked(bool checked) { sim_widget->SetShowContours(checked); }

void MainWindow::ShowExtinguishedChecked(bool checked) { sim_widget->SetShowExtinguished(checked); }

void MainWindow::SlopeChecked(bool checked) { sim_widget->SetSlopeEnabled(checked); }

void MainWindow::WindChecked(bool checked) { sim_widget->SetWindEnabled(checked); }

void MainWindow::WindDirChanged(int item) { sim_widget->WindDirChanged(item * 45); }

void MainWindow::SaveImage() {
  QString file = QFileDialog::getSaveFileName(
      this, tr("Save Image"), QDir::currentPath(),
      tr("Image Files (*.gif *.jpg *.jpeg *.png *.pbm *.ppm *.xbm *.xpm)"));
  if (file.isEmpty() || file.isNull())
    return;
  sim_widget->GetImage().save(file, "PNG");
}

void MainWindow::WindSpeedChanged(double val) { sim_widget->WindSpeedChanged(val); }

void MainWindow::IncSimSpeed() {
  if (speed >= 50)
    return;
  speed++;
  int speed = this->ui->lblSpeed->setText(speed);
  sim_widget->SetSimSpeed(speed);
}

void MainWindow::DecSimSpeed() {
  if (speed <= 0)
    return;
  speed--;
  int speed = this->ui->lblSpeed->setText(speed);
  sim_widget->SetSimSpeed(speed);
}

void MainWindow::ResetSimSpeed() {
  speed = 1;
  int speed = this->ui->lblSpeed->setText(speed);
  sim_widget->SetSimSpeed(speed);
}

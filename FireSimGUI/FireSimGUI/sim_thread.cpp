#include "sim_thread.h"

SimThread::SimThread(Dem *_dem) {
  dem = _dem;
  running = false;
  abort = false;
  dem_loaded = false;
  sim_img = QImage(dem->Width(), dem->Height(), QImage::Format_RGB888);
}

SimThread::~SimThread() {
  mutex.lock();
  abort = true;
  condition.wakeOne();
  mutex.unlock();
  quit();
  wait();
  delete dem;
}

// === Public Methods ===
void SimThread::Reset() {
  sim_img = QImage(dem->Width(), dem->Height(), QImage::Format_RGB888);
  states.clear();
  new_states.clear();
  time_started.clear();
  elapsed_sim_time = 0;
  last_contour_update = 0;
  for (int i = 0; i < dem->Width() * dem->Height(); i++) {
    states.push_back(0);
    new_states.push_back(0);
    time_started.push_back(0);
    dem->SetContourAt(i, false);
    dem->SetState(i, dem->UNBURNED);
  }
  dem->SetState(dem->Coord(dem->Width() / 2, dem->Height() / 2), dem->BURNING);
  states[dem->Coord(dem->Width() / 2, dem->Height() / 2)] = 1.0;
  new_states[dem->Coord(dem->Width() / 2, dem->Height() / 2)] = 1.0;
}

void SimThread::PlayPauseSimulation(bool _running) { running = _running; }

void SimThread::ResizeImage(int new_width, int new_height) {
  // QMutexLocker locker(&mutex);
  widgetWidth = new_width;
  widgetHeight = new_height;
  if (!isRunning()) {
    start(LowPriority);
  }
}

// === Private Methods ===
void SimThread::run() {
  forever {
    mutex.lock();
    auto start = std::chrono::system_clock::now();
    if (running)
      UpdateStates((constant) ? constant_time
                              : (model_time_ms != 0) ? (model_time_ms * sim_speed) : sim_speed);
    auto model_end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = model_end - start;
    model_time_ms = elapsed_seconds.count() * 1000.0;
    mutex.unlock();
    if (abort)
      return;
    mutex.lock();
    sim_img = RenderImage();
    auto full_end = std::chrono::system_clock::now();
    elapsed_seconds = full_end - start;
    full_time_ms = elapsed_seconds.count() * 1000.0;
    mutex.unlock();
    emit UpdateComplete(sim_img, model_time_ms, full_time_ms);
  }
}

void SimThread::UpdateStates(float delta_time_ms) {
  float delta_time = delta_time_ms / 1000.0;
  elapsed_sim_time += delta_time_ms;
  float adj_coeff = (1.0f / float(dem->Scale())) * delta_time;
  float diag_coeff = 0.17 * ((1.0 / (sqrt(2) * float(dem->Scale()))) * delta_time);

#pragma omp parallel for if (PARALLEL_ENABLED)
  for (int j = 0; j < dem->Height(); j++) {
    for (int i = 0; i < dem->Width(); i++) {
      if (dem->State(dem->Coord(i, j)) == dem->BURNING) {
        float td = sqrt(2) * (float(dem->Scale()) / dem->RateOfSpread(dem->Coord(i, j)));
        if (elapsed_sim_time >= (time_started[dem->Coord(i, j)] + (td * 1000.0)))
          dem->SetState(dem->Coord(i, j), dem->EXTINGUISHED);
      } else if (dem->State(dem->Coord(i, j)) == dem->UNBURNED) {
        float sum_adj = 0;
        sum_adj += RateOfSpreadAt(i, j, i - 1, j, WEST);
        sum_adj += RateOfSpreadAt(i, j, i, j - 1, NORTH);
        sum_adj += RateOfSpreadAt(i, j, i + 1, j, EAST);
        sum_adj += RateOfSpreadAt(i, j, i, j + 1, SOUTH);

        float sum_diag = 0;
        sum_diag += RateOfSpreadAt(i, j, i - 1, j - 1, NORTH_WEST);
        sum_diag += RateOfSpreadAt(i, j, i - 1, j + 1, SOUTH_WEST);
        sum_diag += RateOfSpreadAt(i, j, i + 1, j - 1, NORTH_EAST);
        sum_diag += RateOfSpreadAt(i, j, i + 1, j + 1, SOUTH_EAST);

        new_states[dem->Coord(i, j)] =
            states[dem->Coord(i, j)] + adj_coeff * sum_adj + diag_coeff * sum_diag;
      }
    }
  }
  for (int j = 0; j < dem->Height(); j++) {
    for (int i = 0; i < dem->Width(); i++) {
      if (dem->State(dem->Coord(i, j)) == dem->UNBURNED) {
        if (new_states[dem->Coord(i, j)] >= 1.0) {
          states[dem->Coord(i, j)] = 1.0;
          dem->SetState(dem->Coord(i, j), dem->BURNING);
          time_started[dem->Coord(i, j)] = elapsed_sim_time;
        } else
          states[dem->Coord(i, j)] = new_states[dem->Coord(i, j)];
      }
    }
  }
  bool update_contours = false;
  if (contour_interval != 0 &&
      (elapsed_sim_time > last_contour_update + (contour_interval * 1000.0))) {
    update_contours = true;
    last_contour_update = elapsed_sim_time;
  }
  if (update_contours) {
    for (int j = 0; j < dem->Height(); j++) {
      for (int i = 0; i < dem->Width(); i++) {
        if (dem->State(dem->Coord(i, j)) == dem->BURNING && HasUnburnedNeighbours(i, j))
          dem->SetContourAt(dem->Coord(i, j), true);
      }
    }
  }
}

float SimThread::RateOfSpreadAt(int i, int j, int k, int l, int neighbour_direction) {
  if (k < 0 || l < 0 || k >= dem->Width() || l >= dem->Height() ||
      dem->State(dem->Coord(k, l)) != dem->BURNING /*||
      heat_transferred[dem->Coord(k, l)] < ignition_heat[dem->Coord(i, j)]*/)
    return 0;
  float ros = dem->RateOfSpread(dem->Coord(k, l - 1));
  float slope = 0;
  if (slope_enabled) {
    slope = GetSlope(i, j, k, l, neighbour_direction);
  }
  float flame_angle = 0;
  if (wind_enabled) {
    flame_angle = atan(2.35 * pow(pow(wind_speed, 2) / (9.8 * EST_FLAME_LENGTH), 0.57));
    flame_angle *= (180 / M_PI);
    int relative_angle = GetRelativeAngle(wind_direction, neighbour_direction);
    if (wind_direction % 90 == 0)
      flame_angle =
          (relative_angle == 0) ? flame_angle : (relative_angle == 180) ? -flame_angle : 0;
  }
  return ros * exp(SLOPE_ALPHA * slope + WIND_BETA * flame_angle);
  // return ros * exp(SLOPE_ALPHA * slope);
  // return ros;
}

bool SimThread::HasUnburnedNeighbours(int i, int j) {
  for (int k = i - 1; k <= i + 1; k++) {
    for (int l = j - 1; l <= j + 1; k++) {
      if (k < 0 || l < 0 || k >= dem->Width() || l >= dem->Height() || (i == k && j == l) ||
          dem->State(dem->Coord(k, l)) != dem->UNBURNED)
        continue;
      else
        return true;
    }
  }
  return false;
}

float SimThread::GetSlope(int i, int j, int k, int l, int neighbour_direction) {
  /*float angle = 0;
  float target_elev = tan(angle * (M_PI / 180)) * dem->Scale();
  if (neighbour_direction == NORTH || neighbour_direction == SOUTH)
    target_elev = 0;
  else if (neighbour_direction == NORTH_EAST || neighbour_direction == SOUTH_EAST ||
           neighbour_direction == EAST)
    target_elev = -target_elev;*/
  if (neighbour_direction % 90)
    return atan((dem->Elevation(dem->Coord(i, j)) - dem->Elevation(dem->Coord(k, l))) /
                dem->Scale()) *
           (180 / M_PI);
  else
    return atan((dem->Elevation(dem->Coord(i, j)) - dem->Elevation(dem->Coord(k, l))) /
                (sqrt(2) * dem->Scale())) *
           (180 / M_PI);
  // return atan((dem->Elevation(dem->Coord(i, j)) - dem->Elevation(dem->Coord(k, l))) /
  // dem->Scale());
}

int SimThread::GetRelativeAngle(int wind_direction, int neighbour_direction) {
  int angle = (wind_direction - neighbour_direction) % 360;
  return (angle < 0) ? (angle + 360) : angle;
}

QRgb SimThread::GetRgbFromHsv(int h, float s, float v) {
  float rPrime, gPrime, bPrime;
  if (h >= 360)
    h %= 360;
  s = (s > 1) ? 1 : (s < 0) ? 0 : s;
  v = (v > 1) ? 1 : (v < 0) ? 0 : v;
  float c = v * s;
  float x = c * (1 - abs((h / 60) % 2 - 1));
  float m = v - c;
  if (h < 60) {
    rPrime = c;
    gPrime = x;
    bPrime = 0;
  } else if (h < 120) {
    rPrime = x;
    gPrime = c;
    bPrime = 0;
  } else if (h < 180) {
    rPrime = 0;
    gPrime = c;
    bPrime = x;
  } else if (h < 240) {
    rPrime = 0;
    gPrime = x;
    bPrime = c;
  } else if (h < 300) {
    rPrime = x;
    gPrime = 0;
    bPrime = c;
  } else if (h < 360) {
    rPrime = c;
    gPrime = 0;
    bPrime = x;
  }
  return qRgb((rPrime + m) * 255, (gPrime + m) * 255, (bPrime + m) * 255);
}

QImage SimThread::RenderImage() {
  QImage img = QImage(dem->Width(), dem->Height(), QImage::Format_RGB888);
  const float MIN_VARIANCE = 0.4;
#pragma omp parallel for if (PARALLEL_ENABLED)
  for (int i = 0; i < dem->Width(); i++) {
    for (int j = 0; j < dem->Height(); j++) {
      if (show_contours && dem->HasContourAt(dem->Coord(i, j)))
        img.setPixel(i, j, qRgb(0, 0, 0));
      else {
        float val = 0.5;
        if (show_elevation && dem->MinElevation() < dem->MaxElevation())
          val = (dem->Elevation(dem->Coord(i, j)) - dem->MinElevation()) /
                (dem->MaxElevation() - dem->MinElevation());

        int state = dem->State(dem->Coord(i, j));
        int hue = (state == dem->UNBURNED) ? 80 : 0;
        float saturation = (show_extinguished && state == dem->EXTINGUISHED) ? 0 : 1;
        float variance = val * (1 - MIN_VARIANCE) + MIN_VARIANCE;

        if (!show_terrain && dem->State(dem->Coord(i, j)) == dem->UNBURNED)
          img.setPixel(i, j, qRgb(255, 255, 255));
        else
          img.setPixel(i, j, GetRgbFromHsv(hue, saturation, variance));
      }
    }
  }
  return img.scaled(widgetWidth, widgetHeight, Qt::KeepAspectRatio);
}

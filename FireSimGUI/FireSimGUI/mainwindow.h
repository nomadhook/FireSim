#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "sim_widget.h"
#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>
#include <QtWidgets>
#include <sstream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT
  // === Member Variables ===
  SimWidget *sim_widget = new SimWidget();
  int speed = 1;

  // === Deconstructor ===
public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

private:
  Ui::MainWindow *ui;

  // Methods
private:
  void UpdateUIPermissions();
  void UpdateSpeedControls();

  // Events
private slots:
  void NewBlankDem();
  void LoadFromHeightmap();
  void LoadFromDem();
  void ScaleChanged(int value);
  void IntervalChanged(int value);
  void TimeChecked(bool checked);
  void TimeChanged(int value);
  void PlayPauseSim();
  void StopResetSim();
  void SlopeChecked(bool checked);
  void WindChecked(bool checked);
  void ShowTerrainChecked(bool checked);
  void ShowElevationChecked(bool checked);
  void ShowContoursChecked(bool checked);
  void ShowExtinguishedChecked(bool checked);
  void SaveImage();
  void WindDirChanged(int item);
  void WindSpeedChanged(double val);
  void IncSimSpeed();
  void DecSimSpeed();
  void ResetSimSpeed();
};

#endif // MAINWINDOW_H

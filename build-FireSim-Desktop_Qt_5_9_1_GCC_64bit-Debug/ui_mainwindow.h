/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QLabel *demImg;
    QWidget *panelWind;
    QLabel *label_3;
    QLabel *label_2;
    QCheckBox *windToggleCb;
    QSpinBox *windSpeedTxt;
    QLabel *label_4;
    QComboBox *directionCmb;
    QWidget *panelView;
    QLabel *label_7;
    QCheckBox *ShowTerrainCb;
    QCheckBox *ShowElevationCb;
    QWidget *panelSim;
    QLabel *label_8;
    QPushButton *startSimBtn;
    QPushButton *decSpeedBtn;
    QLabel *speedLbl;
    QLabel *label;
    QPushButton *incSpeedBtn;
    QPushButton *stopSimBtn;
    QWidget *panelDEM;
    QLabel *label_10;
    QPushButton *loadDemBtn;
    QSpinBox *scaleTxt;
    QLabel *label_5;
    QWidget *panelStatus;
    QLabel *label_11;
    QLabel *statsLbl;
    QWidget *panelModel;
    QLabel *label_12;
    QComboBox *spreadCmb;
    QLabel *label_6;
    QLabel *label_13;
    QLabel *label_14;
    QComboBox *windCmb;
    QComboBox *slopeCmb;
    QWidget *panelSpare;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1024, 768);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        demImg = new QLabel(centralWidget);
        demImg->setObjectName(QStringLiteral("demImg"));
        demImg->setGeometry(QRect(170, 0, 600, 600));
        demImg->setStyleSheet(QStringLiteral(""));
        panelWind = new QWidget(centralWidget);
        panelWind->setObjectName(QStringLiteral("panelWind"));
        panelWind->setGeometry(QRect(0, 529, 171, 121));
        panelWind->setStyleSheet(QLatin1String(".QWidget { \n"
"	border: 1px solid rgb(136, 138, 133);\n"
"}"));
        label_3 = new QLabel(panelWind);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 80, 81, 23));
        label_3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_2 = new QLabel(panelWind);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 50, 81, 23));
        label_2->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        windToggleCb = new QCheckBox(panelWind);
        windToggleCb->setObjectName(QStringLiteral("windToggleCb"));
        windToggleCb->setGeometry(QRect(10, 30, 121, 20));
        windToggleCb->setChecked(true);
        windSpeedTxt = new QSpinBox(panelWind);
        windSpeedTxt->setObjectName(QStringLiteral("windSpeedTxt"));
        windSpeedTxt->setGeometry(QRect(95, 50, 71, 23));
        windSpeedTxt->setMinimum(5);
        windSpeedTxt->setMaximum(100);
        windSpeedTxt->setSingleStep(5);
        windSpeedTxt->setValue(10);
        label_4 = new QLabel(panelWind);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 0, 151, 23));
        QFont font;
        font.setPointSize(12);
        label_4->setFont(font);
        label_4->setTextFormat(Qt::AutoText);
        label_4->setAlignment(Qt::AlignCenter);
        directionCmb = new QComboBox(panelWind);
        directionCmb->setObjectName(QStringLiteral("directionCmb"));
        directionCmb->setGeometry(QRect(95, 80, 71, 23));
        panelView = new QWidget(centralWidget);
        panelView->setObjectName(QStringLiteral("panelView"));
        panelView->setGeometry(QRect(0, 449, 171, 81));
        panelView->setStyleSheet(QLatin1String(".QWidget { \n"
"	border: 1px solid rgb(136, 138, 133);\n"
"}"));
        label_7 = new QLabel(panelView);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(10, 0, 151, 23));
        label_7->setFont(font);
        label_7->setTextFormat(Qt::AutoText);
        label_7->setAlignment(Qt::AlignCenter);
        ShowTerrainCb = new QCheckBox(panelView);
        ShowTerrainCb->setObjectName(QStringLiteral("ShowTerrainCb"));
        ShowTerrainCb->setGeometry(QRect(10, 50, 121, 20));
        ShowTerrainCb->setChecked(true);
        ShowElevationCb = new QCheckBox(panelView);
        ShowElevationCb->setObjectName(QStringLiteral("ShowElevationCb"));
        ShowElevationCb->setGeometry(QRect(10, 30, 121, 20));
        ShowElevationCb->setChecked(true);
        label_7->raise();
        ShowTerrainCb->raise();
        ShowElevationCb->raise();
        label_4->raise();
        panelSim = new QWidget(centralWidget);
        panelSim->setObjectName(QStringLiteral("panelSim"));
        panelSim->setGeometry(QRect(0, 119, 171, 201));
        panelSim->setStyleSheet(QLatin1String(".QWidget { \n"
"	border: 1px solid rgb(136, 138, 133);\n"
"}"));
        label_8 = new QLabel(panelSim);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 0, 151, 23));
        label_8->setFont(font);
        label_8->setTextFormat(Qt::AutoText);
        label_8->setAlignment(Qt::AlignCenter);
        startSimBtn = new QPushButton(panelSim);
        startSimBtn->setObjectName(QStringLiteral("startSimBtn"));
        startSimBtn->setGeometry(QRect(10, 30, 151, 40));
        decSpeedBtn = new QPushButton(panelSim);
        decSpeedBtn->setObjectName(QStringLiteral("decSpeedBtn"));
        decSpeedBtn->setGeometry(QRect(10, 150, 51, 40));
        speedLbl = new QLabel(panelSim);
        speedLbl->setObjectName(QStringLiteral("speedLbl"));
        speedLbl->setGeometry(QRect(60, 150, 51, 40));
        speedLbl->setAlignment(Qt::AlignCenter);
        label = new QLabel(panelSim);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(7, 130, 161, 20));
        label->setAlignment(Qt::AlignCenter);
        incSpeedBtn = new QPushButton(panelSim);
        incSpeedBtn->setObjectName(QStringLiteral("incSpeedBtn"));
        incSpeedBtn->setGeometry(QRect(110, 150, 51, 40));
        stopSimBtn = new QPushButton(panelSim);
        stopSimBtn->setObjectName(QStringLiteral("stopSimBtn"));
        stopSimBtn->setGeometry(QRect(10, 80, 151, 40));
        panelDEM = new QWidget(centralWidget);
        panelDEM->setObjectName(QStringLiteral("panelDEM"));
        panelDEM->setGeometry(QRect(0, -1, 171, 121));
        panelDEM->setStyleSheet(QLatin1String(".QWidget { \n"
"	border: 1px solid rgb(136, 138, 133);\n"
"}"));
        label_10 = new QLabel(panelDEM);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(10, 0, 151, 23));
        label_10->setFont(font);
        label_10->setTextFormat(Qt::AutoText);
        label_10->setAlignment(Qt::AlignCenter);
        loadDemBtn = new QPushButton(panelDEM);
        loadDemBtn->setObjectName(QStringLiteral("loadDemBtn"));
        loadDemBtn->setGeometry(QRect(10, 30, 151, 40));
        scaleTxt = new QSpinBox(panelDEM);
        scaleTxt->setObjectName(QStringLiteral("scaleTxt"));
        scaleTxt->setGeometry(QRect(90, 80, 71, 23));
        scaleTxt->setMinimum(1);
        scaleTxt->setMaximum(100);
        scaleTxt->setValue(30);
        label_5 = new QLabel(panelDEM);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(5, 80, 81, 23));
        label_5->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_10->raise();
        loadDemBtn->raise();
        scaleTxt->raise();
        label_5->raise();
        panelStatus = new QWidget(centralWidget);
        panelStatus->setObjectName(QStringLiteral("panelStatus"));
        panelStatus->setGeometry(QRect(770, -1, 254, 731));
        label_11 = new QLabel(panelStatus);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(0, 0, 254, 23));
        label_11->setFont(font);
        label_11->setTextFormat(Qt::AutoText);
        label_11->setAlignment(Qt::AlignCenter);
        statsLbl = new QLabel(panelStatus);
        statsLbl->setObjectName(QStringLiteral("statsLbl"));
        statsLbl->setGeometry(QRect(0, 30, 254, 681));
        statsLbl->setContextMenuPolicy(Qt::DefaultContextMenu);
        statsLbl->setTextFormat(Qt::RichText);
        statsLbl->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        statsLbl->setIndent(5);
        panelModel = new QWidget(centralWidget);
        panelModel->setObjectName(QStringLiteral("panelModel"));
        panelModel->setGeometry(QRect(0, 319, 171, 131));
        panelModel->setStyleSheet(QLatin1String(".QWidget { \n"
"	border: 1px solid rgb(136, 138, 133);\n"
"}"));
        label_12 = new QLabel(panelModel);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(10, 0, 151, 23));
        label_12->setFont(font);
        label_12->setTextFormat(Qt::AutoText);
        label_12->setAlignment(Qt::AlignCenter);
        spreadCmb = new QComboBox(panelModel);
        spreadCmb->setObjectName(QStringLiteral("spreadCmb"));
        spreadCmb->setGeometry(QRect(60, 30, 100, 23));
        label_6 = new QLabel(panelModel);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 30, 50, 23));
        label_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_13 = new QLabel(panelModel);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(10, 60, 50, 23));
        label_13->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_14 = new QLabel(panelModel);
        label_14->setObjectName(QStringLiteral("label_14"));
        label_14->setGeometry(QRect(10, 90, 50, 23));
        label_14->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        windCmb = new QComboBox(panelModel);
        windCmb->setObjectName(QStringLiteral("windCmb"));
        windCmb->setGeometry(QRect(60, 60, 100, 23));
        slopeCmb = new QComboBox(panelModel);
        slopeCmb->setObjectName(QStringLiteral("slopeCmb"));
        slopeCmb->setGeometry(QRect(60, 90, 100, 23));
        panelSpare = new QWidget(centralWidget);
        panelSpare->setObjectName(QStringLiteral("panelSpare"));
        panelSpare->setGeometry(QRect(0, 649, 171, 81));
        panelSpare->setStyleSheet(QLatin1String(".QWidget { \n"
"	border: 1px solid rgb(136, 138, 133);\n"
"}"));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1024, 19));
        menuBar->setStyleSheet(QStringLiteral("visibility: none;"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        statusBar->setStyleSheet(QStringLiteral("border: 1px solid rgb(136, 138, 133)"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        retranslateUi(MainWindow);
        QObject::connect(loadDemBtn, SIGNAL(released()), MainWindow, SLOT(LoadDemPressed()));
        QObject::connect(startSimBtn, SIGNAL(released()), MainWindow, SLOT(StartSimulationPressed()));
        QObject::connect(decSpeedBtn, SIGNAL(released()), MainWindow, SLOT(DecSimSpeedPressed()));
        QObject::connect(incSpeedBtn, SIGNAL(released()), MainWindow, SLOT(IncSimSpeedPressed()));
        QObject::connect(ShowElevationCb, SIGNAL(toggled(bool)), MainWindow, SLOT(ShowElevationChecked(bool)));
        QObject::connect(ShowTerrainCb, SIGNAL(toggled(bool)), MainWindow, SLOT(ShowTerrainChecked(bool)));
        QObject::connect(scaleTxt, SIGNAL(valueChanged(int)), MainWindow, SLOT(ScaleChanged(int)));
        QObject::connect(stopSimBtn, SIGNAL(released()), MainWindow, SLOT(StopSimulation()));
        QObject::connect(windToggleCb, SIGNAL(toggled(bool)), MainWindow, SLOT(WindChecked(bool)));
        QObject::connect(windSpeedTxt, SIGNAL(valueChanged(int)), MainWindow, SLOT(WindSpeedChanged(int)));
        QObject::connect(directionCmb, SIGNAL(highlighted(int)), MainWindow, SLOT(DirectionChanged(int)));
        QObject::connect(spreadCmb, SIGNAL(currentIndexChanged(int)), MainWindow, SLOT(SpreadModelChanged(int)));
        QObject::connect(windCmb, SIGNAL(currentIndexChanged(int)), MainWindow, SLOT(WindModelChanged(int)));
        QObject::connect(slopeCmb, SIGNAL(currentIndexChanged(int)), MainWindow, SLOT(SlopeModelChanged(int)));

        loadDemBtn->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        demImg->setText(QString());
        label_3->setText(QApplication::translate("MainWindow", "Direction:", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindow", "Speed (m/s):", Q_NULLPTR));
        windToggleCb->setText(QApplication::translate("MainWindow", "Enable Wind", Q_NULLPTR));
        label_4->setText(QApplication::translate("MainWindow", "Wind Controls", Q_NULLPTR));
        directionCmb->clear();
        directionCmb->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "N", Q_NULLPTR)
         << QApplication::translate("MainWindow", "NE", Q_NULLPTR)
         << QApplication::translate("MainWindow", "E", Q_NULLPTR)
         << QApplication::translate("MainWindow", "SE", Q_NULLPTR)
         << QApplication::translate("MainWindow", "S", Q_NULLPTR)
         << QApplication::translate("MainWindow", "SW", Q_NULLPTR)
         << QApplication::translate("MainWindow", "W", Q_NULLPTR)
         << QApplication::translate("MainWindow", "NW", Q_NULLPTR)
        );
        label_7->setText(QApplication::translate("MainWindow", "View Controls", Q_NULLPTR));
        ShowTerrainCb->setText(QApplication::translate("MainWindow", "Show Terrain", Q_NULLPTR));
        ShowElevationCb->setText(QApplication::translate("MainWindow", "Show Elevation", Q_NULLPTR));
        label_8->setText(QApplication::translate("MainWindow", "Simulation Controls", Q_NULLPTR));
        startSimBtn->setText(QApplication::translate("MainWindow", "Pause/Play Simulation", Q_NULLPTR));
        decSpeedBtn->setText(QApplication::translate("MainWindow", "-", Q_NULLPTR));
        speedLbl->setText(QApplication::translate("MainWindow", "0x", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Simulation Speed:", Q_NULLPTR));
        incSpeedBtn->setText(QApplication::translate("MainWindow", "+", Q_NULLPTR));
        stopSimBtn->setText(QApplication::translate("MainWindow", "Stop/Reset Simulation", Q_NULLPTR));
        label_10->setText(QApplication::translate("MainWindow", "DEM Controls", Q_NULLPTR));
        loadDemBtn->setText(QApplication::translate("MainWindow", "Load DEM", Q_NULLPTR));
        label_5->setText(QApplication::translate("MainWindow", "Scale (ft.)", Q_NULLPTR));
        panelStatus->setStyleSheet(QApplication::translate("MainWindow", ".QWidget { \n"
"	border: 1px solid rgb(136, 138, 133);\n"
"}", Q_NULLPTR));
        label_11->setText(QApplication::translate("MainWindow", "Statistics:", Q_NULLPTR));
        statsLbl->setText(QString());
        label_12->setText(QApplication::translate("MainWindow", "Model Selection", Q_NULLPTR));
        spreadCmb->clear();
        spreadCmb->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Model A", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Model B", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Model C", Q_NULLPTR)
         << QApplication::translate("MainWindow", "Model D", Q_NULLPTR)
        );
        label_6->setText(QApplication::translate("MainWindow", "Spread:", Q_NULLPTR));
        label_13->setText(QApplication::translate("MainWindow", "Wind:", Q_NULLPTR));
        label_14->setText(QApplication::translate("MainWindow", "Slope:", Q_NULLPTR));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

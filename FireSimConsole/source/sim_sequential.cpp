// ================================================================
// |                       SIM_SEQUENTIAL                         |
// |--------------------------------------------------------------|
// | Author: Damon Hook                                           |
// | Date: 04 October 2017                                        |
// | Desc: Sequential CPU version of 2DCA Fire Simulation         |
// ================================================================
#include <math.h>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

static const int NORTH = 0, NORTH_EAST = 45, EAST = 90, SOUTH_EAST = 135,
                 SOUTH = 180, SOUTH_WEST = 225, WEST = 270, NORTH_WEST = 315;

// clang-format off
// ================================================================
//                             CONFIG
//                  ONLY CHANGE THINGS IN THIS BOX
// ================================================================
static const bool WIND_ACTIVE   = false; 
static const bool SLOPE_ACTIVE  = false; 
static const bool DEBUG_OUTPUT  = false;
static const bool DOUBLE_UPDATE = false;

static const bool FIXED_DELTA_TIME = true;
static const int CONST_TIME        = 1000;

static const int WIDTH        = 100;
static const int HEIGHT       = WIDTH;
static const int ITERATIONS   = 100;
static const int SCALE        = 10;
static const int SIM_SPEED    = 10;
static const float ROS        = 1.0;
static const int WIND_DIR     = NORTH;
static const float WIND_SPEED = 1;

// ================================================================

// ============================================
//                  GLOBALS
// ============================================
// === CONSTANTS ===
static const float SLOPE_ALPHA = 0.0693;
static const float WIND_BETA = 0.0501;
static const float EST_FLAME_LENGTH = 2.8;
// clang-format on

const int UNBURNED = 0, BURNING = 1, EXTINGUISHED = 2;

float elevations[WIDTH * HEIGHT];
int discrete_states[WIDTH * HEIGHT];
float states[WIDTH * HEIGHT];
float new_states[WIDTH * HEIGHT];
float rate_of_spreads[WIDTH * HEIGHT];
float heat_transferred[WIDTH * HEIGHT];
float ignition_heat[WIDTH * HEIGHT];
unsigned long time_started[WIDTH * HEIGHT];
unsigned long elapsed_sim_time;
float timings[ITERATIONS];

#define TRACK_OVERSHOOTS 0;
std::vector<float> overshoots;

// ============================================
//                  METHODS
// ============================================
inline int Coord(int i, int j) { return (j * WIDTH) + i; }

inline float GetSlope(int i, int j, int k, int l) {
  return (elevations[Coord(i, j)] - elevations[Coord(k, l)]) / SCALE;
}

inline int GetRelativeAngle(int wind_direction, int neighbour_direction) {
  float angle = (wind_direction - neighbour_direction) % 360;
  return (angle < 0) ? (angle + 360) : angle;
}

float RateOfSpreadAt(int i, int j, int k, int l, int neighbour_direction) {
  if (k < 0 || l < 0 || k >= WIDTH || l >= HEIGHT ||
      discrete_states[Coord(k, l)] != BURNING ||
      heat_transferred[Coord(k, l)] < ignition_heat[Coord(i, j)])
    return 0;
  float ros = rate_of_spreads[Coord(k, l - 1)];
  if (!SLOPE_ACTIVE && !WIND_ACTIVE) return ros;
  float modifier = 0;
  if (SLOPE_ACTIVE) {
    float slope = GetSlope(i, j, k, l);
    modifier += exp(SLOPE_ALPHA * slope);
  }
  if (WIND_ACTIVE) {
    float flame_angle =
        atan(2.35 * pow(pow(WIND_SPEED, 2) / (9.8 * EST_FLAME_LENGTH), 0.57));
    flame_angle = flame_angle * (180.0 / M_PI);
    float relative_angle = GetRelativeAngle(WIND_DIR, neighbour_direction);
    if (WIND_DIR % 90 == 0)
      flame_angle = (relative_angle == 0)
                        ? flame_angle
                        : (relative_angle == 180) ? -flame_angle : 0;
    else if (WIND_DIR % 45 == 0) {
      float speed_component = cos(45.0 * M_PI / 180.0) * WIND_SPEED;
      float flame_angle_component = atan(
          2.35 *
          pow(pow(flame_angle_component, 2) / (9.8 * EST_FLAME_LENGTH), 0.57));
      flame_angle_component = flame_angle_component * (180.0 / M_PI);
      flame_angle =
          (relative_angle == 0)
              ? flame_angle
              : (relative_angle == 180)
                    ? -flame_angle
                    : (relative_angle == 45 || relative_angle == 315)
                          ? flame_angle_component
                          : (relative_angle == 135 || relative_angle == 225)
                                ? -flame_angle_component
                                : 0;
    }
    modifier += exp(WIND_BETA * flame_angle);
  }
  return ros * modifier;
}

void DoubleUpdate(float delta_time, int i, int j) {
  float new_state = new_states[Coord(i, j)];
  if (new_state > 1.0) {
    float amount_changed = new_state - states[Coord(i, j)];
    float overshot_amount = new_state - 1;
    float portion_overshot = overshot_amount / amount_changed;

    float portion_adj_coeff =
        (1.0f / float(SCALE)) * (portion_overshot * delta_time);
    float portion_diag_coeff = 0.17 * ((1.0 / (sqrt(2) * float(SCALE))) *
                                       (portion_overshot * delta_time));

    float ros = RateOfSpreadAt(i - 1, j, i, j, WEST) * portion_adj_coeff;
    new_states[Coord(i - 1, j)] += ros;
    states[Coord(i - 1, j)] += ros;

    ros = RateOfSpreadAt(i, j - 1, i, j, NORTH) * portion_adj_coeff;
    new_states[Coord(i, j - 1)] += ros;
    states[Coord(i, j - 1)] += ros;

    ros = RateOfSpreadAt(i + 1, j, i, j, EAST) * portion_adj_coeff;
    new_states[Coord(i + 1, j)] += ros;
    states[Coord(i + 1, j)] += ros;

    ros = RateOfSpreadAt(i, j + 1, i, j, SOUTH) * portion_adj_coeff;
    new_states[Coord(i, j + 1)] += ros;
    states[Coord(i, j + 1)] += ros;

    ros = RateOfSpreadAt(i - 1, j - 1, i, j, NORTH_WEST) * portion_diag_coeff;
    new_states[Coord(i - 1, j - 1)] += ros;
    states[Coord(i - 1, j - 1)] += ros;

    ros = RateOfSpreadAt(i - 1, j + 1, i, j, SOUTH_WEST) * portion_diag_coeff;
    new_states[Coord(i - 1, j + 1)] += ros;
    states[Coord(i - 1, j + 1)] += ros;

    ros = RateOfSpreadAt(i + 1, j - 1, i, j, NORTH_EAST) * portion_diag_coeff;
    new_states[Coord(i + 1, j - 1)] += ros;
    states[Coord(i + 1, j - 1)] += ros;

    ros = RateOfSpreadAt(i + 1, j + 1, i, j, SOUTH_EAST) * portion_diag_coeff;
    new_states[Coord(i + 1, j + 1)] += ros;
    states[Coord(i + 1, j + 1)] += ros;
  }
}

void Update(float delta_time_ms) {
  float delta_time = delta_time_ms / 1000.0;
  elapsed_sim_time += delta_time;
  float adj_coeff = (1.0f / float(SCALE)) * delta_time;
  float diag_coeff = 0.17 * ((1.0 / (sqrt(2) * float(SCALE))) * delta_time);

  for (int j = 0; j < HEIGHT; j++) {
    for (int i = 0; i < WIDTH; i++) {
      if (discrete_states[Coord(i, j)] == BURNING) {
        float td = sqrt(2) * (float(SCALE) / rate_of_spreads[Coord(i, j)]);
        if (elapsed_sim_time >= (time_started[Coord(i, j)] + (td)))
          discrete_states[Coord(i, j)] = EXTINGUISHED;
      } else if (discrete_states[Coord(i, j)] == UNBURNED) {
        float sum_adj = 0;
        sum_adj += RateOfSpreadAt(i, j, i - 1, j, WEST);
        sum_adj += RateOfSpreadAt(i, j, i, j - 1, NORTH);
        sum_adj += RateOfSpreadAt(i, j, i + 1, j, EAST);
        sum_adj += RateOfSpreadAt(i, j, i, j + 1, SOUTH);

        float sum_diag = 0;
        sum_diag += RateOfSpreadAt(i, j, i - 1, j - 1, NORTH_WEST);
        sum_diag += RateOfSpreadAt(i, j, i - 1, j + 1, SOUTH_WEST);
        sum_diag += RateOfSpreadAt(i, j, i + 1, j - 1, NORTH_EAST);
        sum_diag += RateOfSpreadAt(i, j, i + 1, j + 1, SOUTH_EAST);

        new_states[Coord(i, j)] =
            states[Coord(i, j)] + adj_coeff * sum_adj + diag_coeff * sum_diag;

#if TRACK_OVERSHOOTS == 1
        if (new_states[Coord(i, j)] >= 1.1)
          overshoots.push_back(new_states[Coord(i, j)] - 1);
#endif

        if (DOUBLE_UPDATE) {
          DoubleUpdate(delta_time, i, j);
        }
      }
    }
  }
  for (int i = 0; i < WIDTH * HEIGHT; i++) {
    if (discrete_states[i] == UNBURNED) {
      if (new_states[i] >= 1.0) {
        states[i] = 1.0;
        discrete_states[i] = BURNING;
        time_started[i] = elapsed_sim_time;
      } else
        states[i] = new_states[i];
    }
  }
}

void PrintSim(int iter, float delta_time) {
  if (iter != -1) {
    std::cout << std::endl;
    std::cout << "Iteration " << iter << "/" << ITERATIONS << std::endl;
    std::cout << delta_time << "ms" << std::endl;
    std::cout << "Elapsed Sim Time " << elapsed_sim_time << "s" << std::endl;
  }
  if (DEBUG_OUTPUT == 0) return;
  for (int j = 0; j < HEIGHT; j++) {
    for (int i = 0; i < WIDTH; i++) {
      if (discrete_states[Coord(i, j)] == BURNING)
        std::cout << "0";
      else
        std::cout << " ";
    }
    std::cout << std::endl;
  }
}
void Setup() {
  for (int i = 0; i < WIDTH * HEIGHT; i++) {
    discrete_states[i] = UNBURNED;
    states[i] = 0;
    rate_of_spreads[i] = ROS;
    heat_transferred[i] = 10.0;
    ignition_heat[i] = 5.0;
    time_started[i] = 0;
    elevations[i] = 0;
  }
  elapsed_sim_time = 0;
  discrete_states[Coord(WIDTH / 2, HEIGHT / 2)] = BURNING;
  states[Coord(WIDTH / 2, HEIGHT / 2)] = 1.0;
}

void WriteOutput(std::string filename) {
  std::ofstream file(filename);
  float cumulative_time = 0;
  file << "Iteration,Execution Time,Cumulative Execution Time" << std::endl;
  for (int i = 0; i < ITERATIONS; i++) {
    cumulative_time += timings[i];
    file << i + 1 << "," << timings[i] << "," << cumulative_time << std::endl;
  }
  file.close();
}

void WriteOvershoots() {
  std::ofstream file("overshoots.txt");
  file << "Total: " << overshoots.size() << std::endl;
  float sum = 0;
  for (int i = 0; i < overshoots.size(); i++) sum += overshoots[i];
  file << "Avg Per Step: " << overshoots.size() / ITERATIONS << std::endl;
  file << "Avg Per Overshoot: " << sum / overshoots.size();
  file.close();
}

int main(int argc, char *argv[]) {
  std::string filename = "output.csv";
  if (argc == 2) filename = argv[1];
  overshoots = std::vector<float>();

  auto start = std::chrono::system_clock::now();
  Setup();
  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  float delta_time = elapsed_seconds.count() * 1000.0;
  std::cout << "Starting Simulation" << std::endl;
  PrintSim(-1, delta_time);
  float total_time = 0;

  for (int i = 0; i < ITERATIONS; i++) {
    start = std::chrono::system_clock::now();
    Update((FIXED_DELTA_TIME == 1) ? CONST_TIME : (delta_time * SIM_SPEED));
    end = std::chrono::system_clock::now();
    elapsed_seconds = end - start;
    delta_time = elapsed_seconds.count() * 1000.0;
    if (DEBUG_OUTPUT)
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    PrintSim(i + 1, delta_time);
    total_time += delta_time;
    timings[i] = delta_time;
  }
  std::cout << std::endl;
  std::cout << "Finished Simulation" << std::endl;
  std::cout << "Total Time: " << total_time << "ms" << std::endl;
  WriteOutput(filename);
  std::cout << "Saved Output Data To: " << filename << std::endl;
#if TRACK_OVERSHOOTS == 1
  WriteOvershoots();
#endif
}
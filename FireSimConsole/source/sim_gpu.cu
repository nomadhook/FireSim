// ================================================================
// |                       SIM_SEQUENTIAL                         |
// |--------------------------------------------------------------|
// | Author: Damon Hook                                           |
// | Date: 04 October 2017                                        |
// | Desc: Sequential CPU version of 2DCA Fire Simulation         |
// ================================================================
#include <cuda_runtime.h>
#include <math.h>
#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

static const int NORTH = 0, NORTH_EAST = 45, EAST = 90, SOUTH_EAST = 135,
                 SOUTH = 180, SOUTH_WEST = 225, WEST = 270, NORTH_WEST = 315;

// clang-format off
// ================================================================
//                             CONFIG
//                  ONLY CHANGE THINGS IN THIS BOX
// ================================================================
__device__ static const bool WIND_ACTIVE   = true; 
__device__ static const bool SLOPE_ACTIVE  = true; 
static const bool DEBUG_OUTPUT             = false;

static const bool FIXED_DELTA_TIME = true;
static const int CONST_TIME        = 1000;

#define WIDTH 100
#define HEIGHT 100
#define SCALE 10

static const int ITERATIONS   = 100;
static const int SIM_SPEED    = 10;
static const float ROS        = 1.0;
static const int WIND_DIR     = NORTH;
static const float WIND_SPEED = 0.5;

#define NUM_PER_THREAD 16

// ================================================================

// ============================================
//                  GLOBALS
// ============================================
// === CONSTANTS ===
__device__ static const float SLOPE_ALPHA = 0.0693;
__device__ static const float WIND_BETA = 0.0501;
__device__ static const float EST_FLAME_LENGTH = 2.8;
// clang-format on

__device__ static const int D_UNBURNED = 0, D_BURNING = 1;
static const int UNBURNED = 0, BURNING = 1, EXTINGUISHED = 2;

float elevations[WIDTH * HEIGHT];
int discrete_states[WIDTH * HEIGHT];
float states[WIDTH * HEIGHT];
float new_states[WIDTH * HEIGHT];
float rate_of_spreads[WIDTH * HEIGHT];
float heat_transferred[WIDTH * HEIGHT];
float ignition_heat[WIDTH * HEIGHT];
unsigned long time_started[WIDTH * HEIGHT];
unsigned long elapsed_sim_time;
float timings[ITERATIONS];

float *d_elevations, *d_states, *d_new_states, *d_rate_of_spreads,
    *d_heat_transferred, *d_ignition_heat;
int *d_discrete_states;

// ============================================
//                GPU METHODS
// ============================================
__device__ inline int DCoord(int i, int j) { return (j * WIDTH) + i; }

__device__ float DRateOfSpreadAt(
    int i, int j, int k, int l, int neighbour_direction, float *d_elevations,
    int *d_discrete_states, float *d_rate_of_spreads, float *d_heat_transferred,
    float *d_ignition_heat, int d_wind_dir, float d_wind_speed) {
  if (k < 0 || l < 0 || k >= WIDTH || l >= HEIGHT ||
      __ldg(&d_discrete_states[DCoord(k, l)]) != D_BURNING ||
      __ldg(&d_heat_transferred[DCoord(k, l)]) <
          __ldg(&d_ignition_heat[DCoord(i, j)]))
    return 0;
  float ros = __ldg(&d_rate_of_spreads[DCoord(k, l - 1)]);
  if (!SLOPE_ACTIVE && !WIND_ACTIVE) return ros;
  float modifier = 0;
  if (SLOPE_ACTIVE) {
    float slope = (__ldg(&d_elevations[DCoord(i, j)]) -
                   __ldg(&d_elevations[DCoord(k, l)])) /
                  SCALE;
    modifier += exp(SLOPE_ALPHA * slope);
  }
  if (WIND_ACTIVE) {
    float flame_angle =
        atan(2.35 * pow(pow(d_wind_speed, 2) / (9.8 * EST_FLAME_LENGTH), 0.57));
    flame_angle = flame_angle * (180.0 / M_PI);
    int relative_angle = (d_wind_dir - neighbour_direction) % 360;
    relative_angle =
        (relative_angle < 0) ? (relative_angle + 360) : relative_angle;
    if (d_wind_dir % 90 == 0)
      flame_angle = (relative_angle == 0)
                        ? flame_angle
                        : (relative_angle == 180) ? -flame_angle : 0;
    else if (WIND_DIR % 45 == 0) {
      float speed_component = cos(45.0 * M_PI / 180.0) * WIND_SPEED;
      float flame_angle_component = atan(
          2.35 *
          pow(pow(flame_angle_component, 2) / (9.8 * EST_FLAME_LENGTH), 0.57));
      flame_angle_component = flame_angle_component * (180.0 / M_PI);
      flame_angle =
          (relative_angle == 0)
              ? flame_angle
              : (relative_angle == 180)
                    ? -flame_angle
                    : (relative_angle == 45 || relative_angle == 315)
                          ? flame_angle_component
                          : (relative_angle == 135 || relative_angle == 225)
                                ? -flame_angle_component
                                : 0;
    }
    modifier += exp(WIND_BETA * flame_angle);
  }
  return ros * modifier;
}

__global__ void DUpdate(float *d_elevations, int *d_discrete_states,
                        float *d_states, float *d_new_states,
                        float *d_rate_of_spreads, float *d_heat_transferred,
                        float *d_ignition_heat, int d_wind_dir,
                        float d_wind_speed, float d_adj_coeff,
                        float d_diag_coeff) {
  int thread_idx = blockIdx.x * blockDim.x * blockDim.y +
                   threadIdx.y * blockDim.x + threadIdx.x;
  int index = thread_idx;
  int step = WIDTH * HEIGHT / NUM_PER_THREAD;
  for (int i = 0; i < NUM_PER_THREAD; i++) {
    if (index >= (WIDTH * HEIGHT)) return;
    int i = index % WIDTH;
    int j = index / WIDTH;
    if (__ldg(&d_discrete_states[index]) == D_UNBURNED) {
      float sum_adj = 0;
      sum_adj +=
          DRateOfSpreadAt(i, j, i - 1, j, WEST, d_elevations, d_discrete_states,
                          d_rate_of_spreads, d_heat_transferred,
                          d_ignition_heat, d_wind_dir, d_wind_speed);
      sum_adj += DRateOfSpreadAt(i, j, i, j - 1, NORTH, d_elevations,
                                 d_discrete_states, d_rate_of_spreads,
                                 d_heat_transferred, d_ignition_heat,
                                 d_wind_dir, d_wind_speed);
      sum_adj +=
          DRateOfSpreadAt(i, j, i + 1, j, EAST, d_elevations, d_discrete_states,
                          d_rate_of_spreads, d_heat_transferred,
                          d_ignition_heat, d_wind_dir, d_wind_speed);
      sum_adj += DRateOfSpreadAt(i, j, i, j + 1, SOUTH, d_elevations,
                                 d_discrete_states, d_rate_of_spreads,
                                 d_heat_transferred, d_ignition_heat,
                                 d_wind_dir, d_wind_speed);

      float sum_diag = 0;
      sum_diag += DRateOfSpreadAt(i, j, i - 1, j - 1, NORTH_WEST, d_elevations,
                                  d_discrete_states, d_rate_of_spreads,
                                  d_heat_transferred, d_ignition_heat,
                                  d_wind_dir, d_wind_speed);
      sum_diag += DRateOfSpreadAt(i, j, i - 1, j + 1, SOUTH_WEST, d_elevations,
                                  d_discrete_states, d_rate_of_spreads,
                                  d_heat_transferred, d_ignition_heat,
                                  d_wind_dir, d_wind_speed);
      sum_diag += DRateOfSpreadAt(i, j, i + 1, j - 1, NORTH_EAST, d_elevations,
                                  d_discrete_states, d_rate_of_spreads,
                                  d_heat_transferred, d_ignition_heat,
                                  d_wind_dir, d_wind_speed);
      sum_diag += DRateOfSpreadAt(i, j, i + 1, j + 1, SOUTH_EAST, d_elevations,
                                  d_discrete_states, d_rate_of_spreads,
                                  d_heat_transferred, d_ignition_heat,
                                  d_wind_dir, d_wind_speed);

      d_new_states[index] = __ldg(&d_states[index]) + d_adj_coeff * sum_adj +
                            d_diag_coeff * sum_diag;
    }
    index += step;
  }
}

// ============================================
//                CPU METHODS
// ============================================

inline int Coord(int i, int j) { return (j * WIDTH) + i; }

void ParallelUpdate(float delta_time_ms) {
  float delta_time = delta_time_ms / 1000.0;
  elapsed_sim_time += delta_time;
  float adj_coeff = (1.0f / float(SCALE)) * delta_time;
  float diag_coeff = 0.17 * ((1.0 / (sqrt(2) * float(SCALE))) * delta_time);

  int dimx = 64;  // default block size if no runtime parameters given
  int dimy = 2;

  dim3 block(dimx, dimy);
  dim3 grid(((WIDTH * HEIGHT / NUM_PER_THREAD + block.x - 1) / block.x) *
            ((WIDTH * HEIGHT / NUM_PER_THREAD + block.y - 1) / block.y));

  cudaMemcpy(d_discrete_states, discrete_states, WIDTH * HEIGHT * sizeof(int),
             cudaMemcpyHostToDevice);
  cudaMemcpy(d_states, states, WIDTH * HEIGHT * sizeof(float),
             cudaMemcpyHostToDevice);

  DUpdate<<<grid, block>>>(d_elevations, d_discrete_states, d_states,
                           d_new_states, d_rate_of_spreads, d_heat_transferred,
                           d_ignition_heat, WIND_DIR, WIND_SPEED, adj_coeff,
                           diag_coeff);

  cudaMemcpy(new_states, d_new_states, WIDTH * HEIGHT * sizeof(float),
             cudaMemcpyDeviceToHost);

  for (int i = 0; i < WIDTH * HEIGHT; i++) {
    if (discrete_states[i] == BURNING) {
      float td = sqrt(2) * (float(SCALE) / rate_of_spreads[i]);
      if (elapsed_sim_time >= (time_started[i] + (td)))
        discrete_states[i] = EXTINGUISHED;
    } else if (discrete_states[i] == UNBURNED) {
      if (new_states[i] >= 1.0) {
        states[i] = 1.0;
        discrete_states[i] = BURNING;
        time_started[i] = elapsed_sim_time;
      } else
        states[i] = new_states[i];
    }
  }
}

void PrintSim(int iter, float delta_time) {
  if (iter != -1) {
    std::cout << std::endl;
    std::cout << "Iteration " << iter << "/" << ITERATIONS << std::endl;
    std::cout << delta_time << "ms" << std::endl;
    std::cout << "Elapsed Sim Time " << elapsed_sim_time << "s" << std::endl;
  }
  if (DEBUG_OUTPUT == 0) return;
  for (int j = 0; j < HEIGHT; j++) {
    for (int i = 0; i < WIDTH; i++) {
      if (discrete_states[Coord(i, j)] == BURNING)
        std::cout << "0";
      else
        std::cout << " ";
    }
    std::cout << std::endl;
  }
}

void Setup() {
  for (int i = 0; i < WIDTH * HEIGHT; i++) {
    discrete_states[i] = UNBURNED;
    states[i] = 0;
    rate_of_spreads[i] = ROS;
    heat_transferred[i] = 10.0;
    ignition_heat[i] = 5.0;
    time_started[i] = 0;
    elevations[i] = 0;
  }
  elapsed_sim_time = 0;
  discrete_states[Coord(WIDTH / 2, HEIGHT / 2)] = BURNING;
  states[Coord(WIDTH / 2, HEIGHT / 2)] = 1.0;
}

void WriteOutput(std::string filename) {
  std::ofstream file(filename);
  float cumulative_time = 0;
  file << "Iteration,Execution Time,Cumulative Execution Time" << std::endl;
  for (int i = 0; i < ITERATIONS; i++) {
    cumulative_time += timings[i];
    file << i + 1 << "," << timings[i] << "," << cumulative_time << std::endl;
  }
  file.close();
}

int main(int argc, char *argv[]) {
  std::string filename = "output.csv";
  if (argc == 2) filename = argv[1];

  auto start = std::chrono::system_clock::now();
  Setup();
  auto end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end - start;
  float delta_time = elapsed_seconds.count() * 1000.0;
  std::cout << "Starting Simulation" << std::endl;
  PrintSim(-1, delta_time);
  float total_time = 0;

  cudaMalloc((float **)&d_elevations, WIDTH * HEIGHT * sizeof(float));
  cudaMalloc((int **)&d_discrete_states, WIDTH * HEIGHT * sizeof(int));
  cudaMalloc((float **)&d_states, WIDTH * HEIGHT * sizeof(float));
  cudaMalloc((float **)&d_new_states, WIDTH * HEIGHT * sizeof(float));
  cudaMalloc((float **)&d_rate_of_spreads, WIDTH * HEIGHT * sizeof(float));
  cudaMalloc((float **)&d_heat_transferred, WIDTH * HEIGHT * sizeof(float));
  cudaMalloc((float **)&d_ignition_heat, WIDTH * HEIGHT * sizeof(float));

  cudaMemcpy(d_elevations, elevations, WIDTH * HEIGHT * sizeof(float),
             cudaMemcpyHostToDevice);
  cudaMemcpy(d_new_states, new_states, WIDTH * HEIGHT * sizeof(float),
             cudaMemcpyHostToDevice);
  cudaMemcpy(d_rate_of_spreads, rate_of_spreads, WIDTH * HEIGHT * sizeof(float),
             cudaMemcpyHostToDevice);
  cudaMemcpy(d_heat_transferred, heat_transferred,
             WIDTH * HEIGHT * sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(d_ignition_heat, ignition_heat, WIDTH * HEIGHT * sizeof(float),
             cudaMemcpyHostToDevice);

  for (int i = 0; i < ITERATIONS; i++) {
    start = std::chrono::system_clock::now();
    ParallelUpdate((FIXED_DELTA_TIME == 1) ? CONST_TIME
                                           : (delta_time * SIM_SPEED));
    end = std::chrono::system_clock::now();
    elapsed_seconds = end - start;
    delta_time = elapsed_seconds.count() * 1000.0;
    if (DEBUG_OUTPUT)
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    PrintSim(i + 1, delta_time);
    total_time += delta_time;
    timings[i] = delta_time;
  }

  cudaFree(d_elevations);
  cudaFree(d_discrete_states);
  cudaFree(d_states);
  cudaFree(d_new_states);
  cudaFree(d_rate_of_spreads);
  cudaFree(d_heat_transferred);
  cudaFree(d_ignition_heat);
  cudaDeviceReset();

  std::cout << std::endl;
  std::cout << "Finished Simulation" << std::endl;
  std::cout << "Total Time: " << total_time << "ms" << std::endl;
  WriteOutput(filename);
  std::cout << "Saved Output Data To: " << filename << std::endl;
}